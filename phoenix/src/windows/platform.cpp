#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

static bool OS_keyboardProc(HWND, UINT, WPARAM, LPARAM);
static void OS_processDialogMessage(MSG&);
static LRESULT CALLBACK OS_windowProc(HWND, UINT, WPARAM, LPARAM);
phoenix::Settings* phoenix::pOS::settings = nullptr;

void phoenix::pOS::main(void)
{
	MSG msg;
	while (GetMessage(&msg, 0, 0, 0)) {
		OS_processDialogMessage(msg);
	}
}

bool phoenix::pOS::pendingEvents(void)
{
	MSG msg;
	return PeekMessage(&msg, 0, 0, 0, PM_NOREMOVE);
}

void phoenix::pOS::processEvents(void)
{
	while (pendingEvents()) {
		MSG msg;
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			OS_processDialogMessage(msg);
		}
	}
}

void OS_processDialogMessage(MSG &msg)
{
	if (msg.message == WM_KEYDOWN || msg.message == WM_KEYUP
	    || msg.message == WM_SYSKEYDOWN || msg.message == WM_SYSKEYUP) {
		if (OS_keyboardProc(msg.hwnd, msg.message,
		    msg.wParam, msg.lParam)) {
			DispatchMessage(&msg);
			return;
		}
	}

	if (!IsDialogMessage(GetForegroundWindow(), &msg)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void phoenix::pOS::quit(void)
{
	osQuit = true;
	PostQuitMessage(0);
}

void phoenix::pOS::initialize(void)
{
	CoInitialize(0);
	InitCommonControls();

	WNDCLASS wc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hIcon = LoadIcon(GetModuleHandle(0), MAKEINTRESOURCE(2));
	wc.hInstance = GetModuleHandle(0);
	wc.lpfnWndProc = OS_windowProc;
	wc.lpszClassName = L"phoenix_window";
	wc.lpszMenuName = 0;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&wc);

	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hInstance = GetModuleHandle(0);
	wc.lpfnWndProc = Canvas_windowProc;
	wc.lpszClassName = L"phoenix_canvas";
	wc.lpszMenuName = 0;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&wc);

	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hInstance = GetModuleHandle(0);
	wc.lpfnWndProc = Label_windowProc;
	wc.lpszClassName = L"phoenix_label";
	wc.lpszMenuName = 0;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&wc);

	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hInstance = GetModuleHandle(0);
	wc.lpfnWndProc = Viewport_windowProc;
	wc.lpszClassName = L"phoenix_viewport";
	wc.lpszMenuName = 0;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&wc);

	settings = new Settings;
	phoenix::pKeyboard::initialize();
}

static bool OS_keyboardProc_TextEdit(HWND hwnd, UINT msg, WPARAM wparam,
    LPARAM lparam, phoenix::TextEdit &textEdit)
{
	/*
	* Ctrl+A = select all text.  note: this is 
	* not a standard accelerator on Windows.
	*/
	if (wparam == 'A' && GetKeyState(VK_CONTROL) < 0) {
		Edit_SetSel(textEdit.p.hwnd, 0, ~0);
		return true;
	}

	/*
	* Ctrl+V = paste text.  note: this formats UNIX (LF) and OS9 (CR)
	* line-endings to Windows (CR+LF) line-endings.  This is necessary
	* as the EDIT control only supports Windows line-endings.
	*/
	if (wparam == 'V' && GetKeyState(VK_CONTROL) < 0) {
		OpenClipboard(hwnd);
		HANDLE handle = GetClipboardData(CF_UNICODETEXT);
		if (!handle) {
			/*
			* XXX: If GetClipboardData doesn't return handle,
			* is closing the clipboard necessary?
			*/
			CloseClipboard();
			return false;
		}

		wchar_t *text = (wchar_t*)GlobalLock(handle);
		if (!text) {
			CloseClipboard();
			return false;
		}

		nall::string data = (const char*)nall::utf8_t(text);
		data.replace("\r\n", "\n");
		data.replace("\r", "\n");
		data.replace("\n", "\r\n");
		GlobalUnlock(handle);
		nall::utf16_t output(data);
		HGLOBAL resource = GlobalAlloc(GMEM_MOVEABLE,
		    (wcslen(output) + 1) * sizeof(wchar_t));
		if (!resource) {
			CloseClipboard();
			return false;
		}

		wchar_t *write = (wchar_t*)GlobalLock(resource);
		if (!write) {
			CloseClipboard();
			return false;
		}

		wcscpy(write, output);
		GlobalUnlock(write);
		if (SetClipboardData(CF_UNICODETEXT, resource) == FALSE) {
			GlobalFree(resource);
		}

		CloseClipboard();
		return false;
	}

	return false;
}

static bool OS_keyboardProc(HWND hwnd, UINT msg, WPARAM wparam,
    LPARAM lparam)
{
	if (msg != WM_KEYDOWN && msg != WM_SYSKEYDOWN && msg != WM_KEYUP &&
	    msg != WM_SYSKEYUP) {
		return false;
	}

	GUITHREADINFO info;
	memset(&info, 0, sizeof(GUITHREADINFO));
	info.cbSize = sizeof(GUITHREADINFO);
	GetGUIThreadInfo(GetCurrentThreadId(), &info);
	phoenix::Object *object = (phoenix::Object*)GetWindowLongPtr(
	    info.hwndFocus, GWLP_USERDATA);
	if (object == nullptr) {
		return false;
	}

	if (dynamic_cast<phoenix::Window*>(object)) {
		phoenix::Window &window = (phoenix::Window&)*object;
		phoenix::Keyboard::Keycode keysym =
		    phoenix::Keysym(wparam, lparam);
		if (keysym != phoenix::Keyboard::Keycode::None) {
			if ((msg == WM_KEYDOWN || msg == WM_SYSKEYDOWN) &&
			    window.onKeyPress) {
				window.onKeyPress(keysym);
			}

			if ((msg == WM_KEYUP || msg == WM_SYSKEYUP) &&
			    window.onKeyRelease) {
				window.onKeyRelease(keysym);
			}
		}

		return false;
	}

	if (msg == WM_KEYDOWN) {
		if (dynamic_cast<phoenix::ListView*>(object)) {
			phoenix::ListView &listView =
			    (phoenix::ListView&)*object;
			if (wparam == VK_RETURN) {
				if (listView.onActivate) {
					listView.onActivate();
				}
			}
		} else if (dynamic_cast<phoenix::LineEdit*>(object)) {
			phoenix::LineEdit &lineEdit = (phoenix::LineEdit&)*object;
			if (wparam == VK_RETURN) {
				if (lineEdit.onActivate) {
					lineEdit.onActivate();
				}
			}
		} else if (dynamic_cast<phoenix::TextEdit*>(object)) {
			phoenix::TextEdit &textEdit =
			    (phoenix::TextEdit&)*object;
			return OS_keyboardProc_TextEdit(hwnd, msg, wparam,
			    lparam, textEdit);
		}
	}

	return false;
}


static LRESULT CALLBACK OS_windowProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
  phoenix::Object *object = (phoenix::Object*)GetWindowLongPtr(hwnd, GWLP_USERDATA);
  if(!object || !dynamic_cast<phoenix::Window*>(object)) return DefWindowProc(hwnd, msg, wparam, lparam);
  phoenix::Window &window = (phoenix::Window&)*object;

  if(!phoenix::osQuit) switch(msg) {
    case WM_CLOSE: {
      window.state.ignore = false;
      if(window.onClose) window.onClose();
      if(window.state.ignore == false) window.setVisible(false);
      return TRUE;
    }

    case WM_MOVE: {
      if(window.p.locked) break;

      phoenix::Geometry geometry = window.geometry();
      window.state.geometry.x = geometry.x;
      window.state.geometry.y = geometry.y;

      if(window.onMove) window.onMove();
      break;
    }

    case WM_SIZE: {
      if(window.p.locked) break;
      SetWindowPos(window.p.hstatus, NULL, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED);

      phoenix::Geometry geometry = window.geometry();
      window.state.geometry.width = geometry.width;
      window.state.geometry.height = geometry.height;

      for(auto &layout : window.state.layout) {
        phoenix::Geometry geom = window.geometry();
        geom.x = geom.y = 0;
        layout.setGeometry(geom);
      }

      if(window.onSize) window.onSize();
      break;
    }

    case WM_GETMINMAXINFO: {
      MINMAXINFO *mmi = (MINMAXINFO*)lparam;
    //mmi->ptMinTrackSize.x = 256 + window.p.frameMargin().width;
    //mmi->ptMinTrackSize.y = 256 + window.p.frameMargin().height;
    //return TRUE;
      break;
    }

    case WM_ERASEBKGND: {
      if(window.p.brush == 0) break;
      RECT rc;
      GetClientRect(window.p.hwnd, &rc);
      PAINTSTRUCT ps;
      BeginPaint(window.p.hwnd, &ps);
      FillRect(ps.hdc, &rc, window.p.brush);
      EndPaint(window.p.hwnd, &ps);
      return TRUE;
    }

    case WM_CTLCOLORBTN:
    case WM_CTLCOLORSTATIC: {
      phoenix::Object *object = (phoenix::Object*)GetWindowLongPtr((HWND)lparam, GWLP_USERDATA);
      if(object && window.p.brush) {
        HDC hdc = (HDC)wparam;
        SetBkColor((HDC)wparam, window.p.brushColor);
        return (INT_PTR)window.p.brush;
      }
      break;
    }

    case WM_COMMAND: {
      unsigned id = LOWORD(wparam);
      HWND control = GetDlgItem(window.p.hwnd, id);
      if(control == 0) {
        phoenix::pObject *object = (phoenix::pObject*)phoenix::pObject::find(id);
        if(!object) break;
        if(dynamic_cast<phoenix::pItem*>(object)) {
          phoenix::Item &item = ((phoenix::pItem*)object)->item;
          if(item.onActivate) item.onActivate();
        } else if(dynamic_cast<phoenix::pCheckItem*>(object)) {
          phoenix::CheckItem &checkItem = ((phoenix::pCheckItem*)object)->checkItem;
          checkItem.setChecked(!checkItem.state.checked);
          if(checkItem.onToggle) checkItem.onToggle();
        } else if(dynamic_cast<phoenix::pRadioItem*>(object)) {
          phoenix::RadioItem &radioItem = ((phoenix::pRadioItem*)object)->radioItem;
          if(radioItem.state.checked == false) {
            radioItem.setChecked();
            if(radioItem.onActivate) radioItem.onActivate();
          }
        }
      } else {
        phoenix::Object *object = (phoenix::Object*)GetWindowLongPtr(control, GWLP_USERDATA);
        if(!object) break;
        if(dynamic_cast<phoenix::Button*>(object)) {
          phoenix::Button &button = (phoenix::Button&)*object;
          if(button.onActivate) button.onActivate();
        } else if(dynamic_cast<phoenix::CheckBox*>(object)) {
          phoenix::CheckBox &checkBox = (phoenix::CheckBox&)*object;
          checkBox.setChecked(!checkBox.state.checked);
          if(checkBox.onToggle) checkBox.onToggle();
        } else if(dynamic_cast<phoenix::ComboBox*>(object)) {
          phoenix::ComboBox &comboBox = (phoenix::ComboBox&)*object;
          if(HIWORD(wparam) == CBN_SELCHANGE) {
            if(comboBox.state.selection != comboBox.selection()) {
              comboBox.state.selection = comboBox.selection();
              if(comboBox.onChange) comboBox.onChange();
            }
          }
        } else if(dynamic_cast<phoenix::LineEdit*>(object)) {
          phoenix::LineEdit &lineEdit = (phoenix::LineEdit&)*object;
          if(HIWORD(wparam) == EN_CHANGE) {
            if(lineEdit.p.locked == false && lineEdit.onChange) lineEdit.onChange();
          }
        } else if(dynamic_cast<phoenix::RadioBox*>(object)) {
          phoenix::RadioBox &radioBox = (phoenix::RadioBox&)*object;
          if(radioBox.state.checked == false) {
            radioBox.setChecked();
            if(radioBox.onActivate) radioBox.onActivate();
          }
        } else if(dynamic_cast<phoenix::TextEdit*>(object)) {
          phoenix::TextEdit &textEdit = (phoenix::TextEdit&)*object;
          if(HIWORD(wparam) == EN_CHANGE) {
            if(textEdit.p.locked == false && textEdit.onChange) textEdit.onChange();
          }
        }
      }
      break;
    }

    case WM_NOTIFY: {
      unsigned id = LOWORD(wparam);
      HWND control = GetDlgItem(window.p.hwnd, id);
      if(control == 0) break;
      phoenix::Object *object = (phoenix::Object*)GetWindowLongPtr(control, GWLP_USERDATA);
      if(object == 0) break;
      if(dynamic_cast<phoenix::ListView*>(object)) {
        phoenix::ListView &listView = (phoenix::ListView&)*object;
        LPNMHDR nmhdr = (LPNMHDR)lparam;
        LPNMLISTVIEW nmlistview = (LPNMLISTVIEW)lparam;

        if(nmhdr->code == LVN_ITEMCHANGED && (nmlistview->uChanged & LVIF_STATE)) {
          unsigned imagemask = ((nmlistview->uNewState & LVIS_STATEIMAGEMASK) >> 12) - 1;
          if(imagemask == 0 || imagemask == 1) {
            if(listView.p.locked == false && listView.onToggle) listView.onToggle(nmlistview->iItem);
          } else if((nmlistview->uOldState & LVIS_FOCUSED) && !(nmlistview->uNewState & LVIS_FOCUSED)) {
            listView.p.lostFocus = true;
          } else if(!(nmlistview->uOldState & LVIS_SELECTED) && (nmlistview->uNewState & LVIS_SELECTED)) {
            listView.p.lostFocus = false;
            listView.state.selected = true;
            listView.state.selection = listView.selection();
            if(listView.p.locked == false && listView.onChange) listView.onChange();
          } else if(listView.p.lostFocus == false && listView.selected() == false) {
            listView.p.lostFocus = false;
            listView.state.selected = false;
            listView.state.selection = 0;
            if(listView.p.locked == false && listView.onChange) listView.onChange();
          }
        } else if(nmhdr->code == LVN_ITEMACTIVATE) {
          if(listView.onActivate) listView.onActivate();
        }
      }
      break;
    }

    case WM_HSCROLL:
    case WM_VSCROLL: {
      phoenix::Object *object = 0;
      if(lparam) {
        object = (phoenix::Object*)GetWindowLongPtr((HWND)lparam, GWLP_USERDATA);
      } else {
        unsigned id = LOWORD(wparam);
        HWND control = GetDlgItem(window.p.hwnd, id);
        if(control == 0) break;
        object = (phoenix::Object*)GetWindowLongPtr(control, GWLP_USERDATA);
      }
      if(object == 0) break;

      if(dynamic_cast<phoenix::HorizontalScrollBar*>(object)
      || dynamic_cast<phoenix::VerticalScrollBar*>(object)) {
        SCROLLINFO info;
        memset(&info, 0, sizeof(SCROLLINFO));
        info.cbSize = sizeof(SCROLLINFO);
        info.fMask = SIF_ALL;
        GetScrollInfo((HWND)lparam, SB_CTL, &info);

        switch(LOWORD(wparam)) {
        case SB_LEFT: info.nPos = info.nMin; break;
        case SB_RIGHT: info.nPos = info.nMax; break;
        case SB_LINELEFT: info.nPos--; break;
        case SB_LINERIGHT: info.nPos++; break;
        case SB_PAGELEFT: info.nPos -= info.nMax >> 3; break;
        case SB_PAGERIGHT: info.nPos += info.nMax >> 3; break;
        case SB_THUMBTRACK: info.nPos = info.nTrackPos; break;
        }

        info.fMask = SIF_POS;
        SetScrollInfo((HWND)lparam, SB_CTL, &info, TRUE);

        //Windows may clamp position to scrollbar range
        GetScrollInfo((HWND)lparam, SB_CTL, &info);

        if(dynamic_cast<phoenix::HorizontalScrollBar*>(object)) {
          phoenix::HorizontalScrollBar &horizontalScrollBar = (phoenix::HorizontalScrollBar&)*object;
          if(horizontalScrollBar.state.position != info.nPos) {
            horizontalScrollBar.state.position = info.nPos;
            if(horizontalScrollBar.onChange) horizontalScrollBar.onChange();
          }
        } else {
          phoenix::VerticalScrollBar &verticalScrollBar = (phoenix::VerticalScrollBar&)*object;
          if(verticalScrollBar.state.position != info.nPos) {
            verticalScrollBar.state.position = info.nPos;
            if(verticalScrollBar.onChange) verticalScrollBar.onChange();
          }
        }

        return TRUE;
      }

      if(dynamic_cast<phoenix::HorizontalSlider*>(object)) {
        phoenix::HorizontalSlider &horizontalSlider = (phoenix::HorizontalSlider&)*object;
        if(horizontalSlider.state.position != horizontalSlider.position()) {
          horizontalSlider.state.position = horizontalSlider.position();
          if(horizontalSlider.onChange) horizontalSlider.onChange();
        }
      } else if(dynamic_cast<phoenix::VerticalSlider*>(object)) {
        phoenix::VerticalSlider &verticalSlider = (phoenix::VerticalSlider&)*object;
        if(verticalSlider.state.position != verticalSlider.position()) {
          verticalSlider.state.position = verticalSlider.position();
          if(verticalSlider.onChange) verticalSlider.onChange();
        }
      }

      break;
    }
  }

  return DefWindowProc(hwnd, msg, wparam, lparam);
}
