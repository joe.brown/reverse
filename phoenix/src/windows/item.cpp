#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

void phoenix::pItem::setImage(const nall::image &image)
{
	createBitmap();
	if (parentWindow) {
		parentWindow->p.updateMenu();
	}
}

void phoenix::pItem::setText(const nall::string &text)
{
	if (parentWindow) {
		parentWindow->p.updateMenu();
	}
}

void phoenix::pItem::constructor(void)
{
	createBitmap();
}

void phoenix::pItem::destructor(void)
{
	if (hbitmap) {
		DeleteObject(hbitmap);
		hbitmap = 0;
	}

	if (parentMenu) {
		parentMenu->remove(item);
	}
}

void phoenix::pItem::createBitmap(void)
{
	if (hbitmap) {
		DeleteObject(hbitmap);
		hbitmap = 0;
	}

	if (item.state.image.width && item.state.image.height) {
		nall::image nallImage = item.state.image;
		nallImage.transform(0, 32, 255u << 24, 255u << 16,
		    255u << 8, 255u << 0);
		/* Windows does not alpha blend menu icons properly. */
		nallImage.alphaBlend(GetSysColor(COLOR_MENU));
		nallImage.scale(GetSystemMetrics(SM_CXMENUCHECK),
		    GetSystemMetrics(SM_CYMENUCHECK),
		    nall::Interpolation::Linear);
		hbitmap = phoenix::CreateBitmap(nallImage);
	}
}
