#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

static nall::string FileDialog(bool save, phoenix::Window &parent,
    const nall::string &path, const nall::lstring &filter)
{
	nall::string dir = path;
	dir.replace("/", "\\");

	nall::string filterList;
	for (auto &filterItem : filter) {
		nall::lstring part;
		part.split("(", filterItem);
		if (part.size() != 2) {
			continue;
		}

		part[1].rtrim<1>(")");
		part[1].replace(" ", "");
		part[1].transform(",", ";");
		filterList.append(nall::string(filterItem, "\t",
		    part[1], "\t"));
	}

	nall::utf16_t wfilter(filterList);
	nall::utf16_t wdir(dir);
	wchar_t wfilename[PATH_MAX + 1] = L"";

	wchar_t *p = wfilter;
	while (*p != L'\0') {
		if (*p == L'\t') {
			*p = L'\0';
		}
		p++;
	}

	OPENFILENAME ofn;
	memset(&ofn, 0, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = &parent != &phoenix::Window::None ?
	    parent.p.hwnd : 0;
	ofn.lpstrFilter = wfilter;
	ofn.lpstrInitialDir = wdir;
	ofn.lpstrFile = wfilename;
	ofn.nMaxFile = PATH_MAX;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = L"";

	bool result = (save == false ? GetOpenFileName(&ofn) :
	    GetSaveFileName(&ofn));
	if (result == false) {
		return "";
	}

	nall::string name = (const char*)nall::utf8_t(wfilename);
	name.transform("\\", "/");
	return name;
}

nall::string phoenix::pDialogWindow::fileOpen(phoenix::Window &parent,
    const nall::string &path, const nall::lstring &filter)
{
	return FileDialog(false, parent, path, filter);
}

nall::string phoenix::pDialogWindow::fileSave(phoenix::Window &parent,
    const nall::string &path, const nall::lstring &filter)
{
	return FileDialog(true, parent, path, filter);
}

nall::string phoenix::pDialogWindow::folderSelect(phoenix::Window &parent,
    const nall::string &path)
{
	wchar_t wfilename[PATH_MAX + 1] = L"";
	BROWSEINFO bi;
	bi.hwndOwner = &parent != &Window::None ? parent.p.hwnd : 0;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = wfilename;
	bi.lpszTitle = L"";
	bi.ulFlags = BIF_NEWDIALOGSTYLE | BIF_RETURNONLYFSDIRS;
	bi.lpfn = NULL;
	bi.lParam = 0;
	bi.iImage = 0;
	bool result = false;

	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
	if (pidl) {
		if (SHGetPathFromIDList(pidl, wfilename)) {
			result = true;
			IMalloc *imalloc = 0;
			if (SUCCEEDED(SHGetMalloc(&imalloc))) {
				imalloc->Free(pidl);
				imalloc->Release();
			}
		}
	}

	if (result == false) {
		return "";
	}

	nall::string name = (const char*)nall::utf8_t(wfilename);
	if (name == "") {
		return "";
	}

	name.transform("\\", "/");
	if (name.endswith("/") == false) {
		name.append("/");
	}

	return name;
}
