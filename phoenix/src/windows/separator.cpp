#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

void phoenix::pSeparator::constructor(void) { }

void phoenix::pSeparator::destructor(void)
{
	if (parentMenu) {
		parentMenu->remove(separator);
	}
}
