#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

static nall::linear_vector<phoenix::pTimer*> timers;

static void CALLBACK Timer_timeoutProc(HWND hwnd, UINT msg,
    UINT_PTR timerID, DWORD time)
{
	for (auto &timer : timers) {
		if (timer->htimer == timerID) {
			if (timer->timer.onTimeout) {
				timer->timer.onTimeout();
			}
			return;
		}
	}
}

void phoenix::pTimer::setEnabled(bool enabled)
{
	if (htimer) {
		KillTimer(NULL, htimer);
		htimer = 0;
	}

	if (enabled == true) {
		htimer = SetTimer(NULL, 0U, timer.state.milliseconds,
		    Timer_timeoutProc);
	}
}

void phoenix::pTimer::setInterval(unsigned milliseconds)
{
	/* Destroy and recreate timer if interval changed. */
	setEnabled(timer.state.enabled);
}

void phoenix::pTimer::constructor(void)
{
	timers.append(this);
	htimer = 0;
}
