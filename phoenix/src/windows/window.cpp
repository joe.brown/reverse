#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

static const unsigned FixedStyle = WS_SYSMENU | WS_CAPTION |
    WS_MINIMIZEBOX | WS_BORDER;
static const unsigned ResizableStyle = WS_SYSMENU | WS_CAPTION |
    WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME;

void phoenix::pWindow::append(phoenix::Layout &layout)
{
	Geometry geom = window.state.geometry;
	geom.x = geom.y = 0;
	layout.setGeometry(geom);
}

void phoenix::pWindow::append(phoenix::Menu &menu)
{
	menu.p.parentWindow = &window;
	updateMenu();
}

void phoenix::pWindow::append(phoenix::Widget &widget)
{
	widget.p.parentWindow = &window;
	widget.p.orphan();

	if (widget.state.font != "") {
		widget.p.setFont(widget.state.font);
	} else if (window.state.widgetFont != "") {
		widget.p.setFont(window.state.widgetFont);
	} else {
		widget.p.setFont("Tahoma, 8");
	}
}

phoenix::Color phoenix::pWindow::backgroundColor(void)
{
	if (window.state.backgroundColorOverride) {
		return window.state.backgroundColor;
	}

	DWORD color = GetSysColor(COLOR_3DFACE);
	return { (uint8_t)(color >> 16), (uint8_t)(color >> 8),
	    (uint8_t)(color >> 0), 255 };
}

bool phoenix::pWindow::focused(void)
{
	return (GetForegroundWindow() == hwnd);
}

phoenix::Geometry phoenix::pWindow::frameMargin(void)
{
	unsigned style = window.state.resizable ?
	    ResizableStyle : FixedStyle;

	if (window.state.fullScreen) {
		style = 0;
	}

	RECT rc = { 0, 0, 640, 480 };
	AdjustWindowRect(&rc, style, window.state.menuVisible);
	unsigned statusHeight = 0;
	if (window.state.statusVisible) {
		RECT src;
		GetClientRect(hstatus, &src);
		statusHeight = src.bottom - src.top;
	}

	return { abs(rc.left), abs(rc.top), (rc.right - rc.left) - 640,
	    (rc.bottom - rc.top) + statusHeight - 480 };
}

phoenix::Geometry phoenix::pWindow::geometry(void)
{
	Geometry margin = frameMargin();

	RECT rc;
	if (IsIconic(hwnd)) {
		/*
		* GetWindowsRect returns -32000(x), -32000(y)
		* when window is minimized.
		*/
		WINDOWPLACEMENT wp;
		GetWindowPlacement(hwnd, &wp);
		rc = wp.rcNormalPosition;
	} else {
		GetWindowRect(hwnd, &rc);
	}

	signed x = rc.left + margin.x;
	signed y = rc.top + margin.y;
	unsigned width = (rc.right - rc.left) - margin.width;
	unsigned height = (rc.bottom - rc.top) - margin.height;

	return { x, y, width, height };
}

void phoenix::pWindow::remove(phoenix::Layout &layout) { }

void phoenix::pWindow::remove(phoenix::Menu &menu)
{
	updateMenu();
}

void phoenix::pWindow::remove(phoenix::Widget &widget)
{
	widget.p.orphan();
}

void phoenix::pWindow::setBackgroundColor(const phoenix::Color &color)
{
	if (brush) {
		DeleteObject(brush);
	}

	brushColor = RGB(color.red, color.green, color.blue);
	brush = CreateSolidBrush(brushColor);
}

void phoenix::pWindow::setFocused(void)
{
	if (window.state.visible == false) {
		setVisible(true);
	}

	SetFocus(hwnd);
}

void phoenix::pWindow::setFullScreen(bool fullScreen)
{
	locked = true;
	if (fullScreen == false) {
		SetWindowLongPtr(hwnd, GWL_STYLE, WS_VISIBLE |
		    (window.state.resizable ? ResizableStyle : FixedStyle));
		setGeometry(window.state.geometry);
	} else {
		SetWindowLongPtr(hwnd, GWL_STYLE, WS_VISIBLE | WS_POPUP);
		Geometry margin = frameMargin();
		setGeometry({ margin.x, margin.y,
		    GetSystemMetrics(SM_CXSCREEN) - margin.width,
		    GetSystemMetrics(SM_CYSCREEN) - margin.height });
	}

	locked = false;
}

void phoenix::pWindow::setGeometry(const phoenix::Geometry &geometry)
{
	locked = true;
	Geometry margin = frameMargin();

	SetWindowPos(hwnd, NULL, geometry.x - margin.x,
	    geometry.y - margin.y, geometry.width + margin.width,
	    geometry.height + margin.height,
	    SWP_NOZORDER | SWP_FRAMECHANGED);

	SetWindowPos(hstatus, NULL, 0, 0, 0, 0,
	    SWP_NOZORDER | SWP_FRAMECHANGED);

	for (auto &layout : window.state.layout) {
		Geometry geom = this->geometry();
		geom.x = geom.y = 0;
		layout.setGeometry(geom);
	}

	locked = false;
}

void phoenix::pWindow::setMenuFont(const nall::string &font) { }

void phoenix::pWindow::setMenuVisible(bool visible)
{
	locked = true;
	SetMenu(hwnd, visible ? hmenu : 0);
	setGeometry(window.state.geometry);
	locked = false;
}

void phoenix::pWindow::setResizable(bool resizable)
{
	SetWindowLongPtr(hwnd, GWL_STYLE, window.state.resizable ?
	    ResizableStyle : FixedStyle);
	setGeometry(window.state.geometry);
}

void phoenix::pWindow::setStatusFont(const nall::string &font)
{
	if (hstatusfont) {
		DeleteObject(hstatusfont);
	}

	hstatusfont = pFont::create(font);
	SendMessage(hstatus, WM_SETFONT, (WPARAM)hstatusfont, 0);
}

void phoenix::pWindow::setStatusText(const nall::string &text)
{
	const wchar_t *tmp = nall::utf16_t(text);
	SendMessage(hstatus, SB_SETTEXT, 0, (LPARAM)tmp);
}

void phoenix::pWindow::setStatusVisible(bool visible)
{
	locked = true;
	ShowWindow(hstatus, visible ? SW_SHOWNORMAL : SW_HIDE);
	setGeometry(window.state.geometry);
	locked = false;
}

void phoenix::pWindow::setTitle(const nall::string &text)
{
	SetWindowText(hwnd, nall::utf16_t(text));
}

void phoenix::pWindow::setVisible(bool visible)
{
	ShowWindow(hwnd, visible ? SW_SHOWNORMAL : SW_HIDE);
}

void phoenix::pWindow::setWidgetFont(const nall::string &font)
{
	for (auto &widget : window.state.widget) {
		if (widget.state.font == "") {
			widget.setFont(font);
		}
	}
}

void phoenix::pWindow::constructor(void)
{
	brush = 0;

	hwnd = CreateWindow(L"phoenix_window", L"", ResizableStyle,
	    128, 128, 256, 256, 0, 0, GetModuleHandle(0), 0);
	hmenu = CreateMenu();
	hstatus = CreateWindow(STATUSCLASSNAME, L"", WS_CHILD, 0, 0, 0, 0,
	    hwnd, 0, GetModuleHandle(0), 0);
	hstatusfont = 0;
	setStatusFont("Tahoma, 8");

	/* 
	* Status bar will be capable of receiving tab
	* focus if it is not disabled
	*/
	SetWindowLongPtr(hstatus, GWL_STYLE, GetWindowLong(hstatus,
	    GWL_STYLE) | WS_DISABLED);

	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&window);
	setGeometry({ 128, 128, 256, 256 });
}

void phoenix::pWindow::destructor(void)
{
	DeleteObject(hstatusfont);
	DestroyWindow(hstatus);
	DestroyMenu(hmenu);
	DestroyWindow(hwnd);
}

void phoenix::pWindow::updateMenu(void)
{
	if (hmenu) {
		DestroyMenu(hmenu);
	}
	hmenu = CreateMenu();

	for (auto &menu : window.state.menu) {
		menu.p.update(window);
		if (menu.visible()) {
			AppendMenu(hmenu, MF_STRING | MF_POPUP,
			    (UINT_PTR)menu.p.hmenu,
			    nall::utf16_t(menu.state.text));
		}
	}

	SetMenu(hwnd, window.state.menuVisible ? hmenu : 0);
}
