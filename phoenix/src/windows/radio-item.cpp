#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

bool phoenix::pRadioItem::checked(void)
{
	return radioItem.state.checked;
}

void phoenix::pRadioItem::setChecked(void)
{
	for (auto &item : radioItem.state.group) {
		/*
		* CheckMenuRadioItems takes: lo, hi, id; checking only id
		* when lo <= id <= hi.  phoenix does not force IDs to be
		* linear, so to uncheck id, we use lo == hi == id + 1,
		* which is out of range.  To check id we use: lo == hi == id
		* (only ID, but in range).
		*/
		if (item.p.parentMenu) {
			CheckMenuRadioItem(item.p.parentMenu->p.hmenu,
			    item.p.id, item.p.id, item.p.id +
			    (id != item.p.id), MF_BYCOMMAND);
		}
	}
}

void phoenix::pRadioItem::setGroup(
    const nall::array<phoenix::RadioItem&> &group) { }

void phoenix::pRadioItem::setText(const nall::string &text)
{
	if (parentWindow) {
		parentWindow->p.updateMenu();
	}
}

void phoenix::pRadioItem::constructor(void) { }

void phoenix::pRadioItem::destructor(void)
{
	if (parentMenu) {
		parentMenu->remove(radioItem);
	}
}
