#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

nall::array<phoenix::pObject*> phoenix::pObject::objects;

phoenix::pObject::pObject(phoenix::Object &object) : object(object)
{
	static unsigned uniqueId = 100;
	objects.append(this);
	id = uniqueId++;
	locked = false;
}

phoenix::pObject* phoenix::pObject::find(unsigned id)
{
	for (auto &item : objects) {
		if (item->id == id) {
			return item;
		}
	}

	return 0;
}
