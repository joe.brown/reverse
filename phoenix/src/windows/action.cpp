#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

void phoenix::pAction::setEnabled(bool enabled)
{
	if (parentWindow) {
		parentWindow->p.updateMenu();
	}
}

void phoenix::pAction::setVisible(bool visible)
{
	if (parentWindow) {
		parentWindow->p.updateMenu();
	}
}

void phoenix::pAction::constructor(void)
{
	parentMenu = 0;
	parentWindow = 0;
}
