#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

static phoenix::MessageWindow::Response MessageWindow_response(
    phoenix::MessageWindow::Buttons buttons, UINT response)
{
	if (response == IDOK) {
		return phoenix::MessageWindow::Response::Ok;
	}

	if (response == IDCANCEL) {
		return phoenix::MessageWindow::Response::Cancel;
	}

	if (response == IDYES) {
		return phoenix::MessageWindow::Response::Yes;
	}

	if (response == IDNO) {
		return phoenix::MessageWindow::Response::No;
	}

	if (buttons == phoenix::MessageWindow::Buttons::OkCancel) {
		return phoenix::MessageWindow::Response::Cancel;
	}

	if (buttons == phoenix::MessageWindow::Buttons::YesNo) {
		return phoenix::MessageWindow::Response::No;
	}

	return phoenix::MessageWindow::Response::Ok;
}

phoenix::MessageWindow::Response phoenix::pMessageWindow::information(
    phoenix::Window &parent, const nall::string &text,
    phoenix::MessageWindow::Buttons buttons)
{
	UINT flags = MB_ICONINFORMATION;
	if (buttons == MessageWindow::Buttons::Ok) {
		flags |= MB_OK;
	}

	if (buttons == MessageWindow::Buttons::OkCancel) {
		flags |= MB_OKCANCEL;
	}

	if (buttons == MessageWindow::Buttons::YesNo) {
		flags |= MB_YESNO;
	}

	return MessageWindow_response(buttons,
	    MessageBox(&parent != &Window::None ?
	    parent.p.hwnd : 0, nall::utf16_t(text), L"", flags));
}

phoenix::MessageWindow::Response phoenix::pMessageWindow::question(
    phoenix::Window &parent, const nall::string &text,
    phoenix::MessageWindow::Buttons buttons)
{
	UINT flags = MB_ICONQUESTION;
	if (buttons == MessageWindow::Buttons::Ok) {
		flags |= MB_OK;
	}

	if (buttons == MessageWindow::Buttons::OkCancel) {
		flags |= MB_OKCANCEL;
	}

	if (buttons == MessageWindow::Buttons::YesNo) {
		flags |= MB_YESNO;
	}

	return MessageWindow_response(buttons,
	    MessageBox(&parent != &phoenix::Window::None ?
	    parent.p.hwnd : 0, nall::utf16_t(text), L"", flags));
}

phoenix::MessageWindow::Response phoenix::pMessageWindow::warning(
    phoenix::Window &parent, const nall::string &text,
    phoenix::MessageWindow::Buttons buttons)
{
	UINT flags = MB_ICONWARNING;
	if (buttons == MessageWindow::Buttons::Ok) {
		flags |= MB_OK;
	}

	if (buttons == MessageWindow::Buttons::OkCancel) {
		flags |= MB_OKCANCEL;
	}

	if (buttons == MessageWindow::Buttons::YesNo) {
		flags |= MB_YESNO;
	}

	return MessageWindow_response(buttons, MessageBox(
	    &parent != &phoenix::Window::None ? parent.p.hwnd : 0,
	    nall::utf16_t(text), L"", flags));
}

phoenix::MessageWindow::Response phoenix::pMessageWindow::critical(
    phoenix::Window &parent, const nall::string &text,
    phoenix::MessageWindow::Buttons buttons)
{
	UINT flags = MB_ICONERROR;
	if (buttons == MessageWindow::Buttons::Ok) {
		flags |= MB_OK;
	}

	if (buttons == MessageWindow::Buttons::OkCancel) {
		flags |= MB_OKCANCEL;
	}

	if (buttons == MessageWindow::Buttons::YesNo) {
		flags |= MB_YESNO;
	}

	return MessageWindow_response(buttons, MessageBox(
	    &parent != &phoenix::Window::None ? parent.p.hwnd : 0,
	    nall::utf16_t(text), L"", flags));
}
