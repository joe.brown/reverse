#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

phoenix::Size phoenix::pDesktop::size(void)
{
	return { GetSystemMetrics(SM_CXSCREEN),
	    GetSystemMetrics(SM_CYSCREEN) };
}

phoenix::Geometry phoenix::pDesktop::workspace(void)
{
	RECT rc;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rc, 0);
	return { rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top };
}
