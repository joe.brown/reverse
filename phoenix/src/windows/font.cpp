#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

phoenix::Geometry phoenix::pFont::geometry(const nall::string &description,
    const nall::string &text)
{
	HFONT hfont = pFont::create(description);
	Geometry geometry = pFont::geometry(hfont, text);
	pFont::free(hfont);
	return geometry;
}

HFONT phoenix::pFont::create(const nall::string &description)
{
	nall::lstring part;
	part.split(",", description);
	for (auto &item : part) {
		item.trim(" ");
	}

	nall::string family = "Sans";
	unsigned size = 8u;
	bool bold = false;
	bool italic = false;

	if (part[0] != "") {
		family = part[0];
	}

	if (part.size() >= 2) {
		size = decimal(part[1]);
	}

	if (part.size() >= 3) {
		bold = part[2].position("Bold");
	}

	if (part.size() >= 3) {
		italic = part[2].position("Italic");
	}

	return CreateFont(
	    -(size * 96.0 / 72.0 + 0.5),
	    0, 0, 0, bold == false ? FW_NORMAL : FW_BOLD,
	    italic, 0, 0, 0, 0, 0, 0, 0, nall::utf16_t(family));
}

void phoenix::pFont::free(HFONT hfont)
{
	DeleteObject(hfont);
}

phoenix::Geometry phoenix::pFont::geometry(HFONT hfont,
    const nall::string &text_)
{
	/*
	* XXX: temporary fix: empty text string returns height
	* of zero; bad for eg Button height
	*/
	nall::string text = (text_ == "" ? " " : text_);

	HDC hdc = GetDC(0);
	SelectObject(hdc, hfont);
	RECT rc = { 0, 0, 0, 0 };
	DrawText(hdc, nall::utf16_t(text), -1, &rc, DT_CALCRECT);
	ReleaseDC(0, hdc);

	return { 0, 0, rc.right, rc.bottom };
}
