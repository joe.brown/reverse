#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

bool phoenix::pCheckItem::checked(void)
{
	return checkItem.state.checked;
}

void phoenix::pCheckItem::setChecked(bool checked)
{
	if (parentMenu) {
		CheckMenuItem(parentMenu->p.hmenu, id, checked ?
		    MF_CHECKED : MF_UNCHECKED);
	}
}

void phoenix::pCheckItem::setText(const nall::string &text)
{
	if (parentWindow) {
		parentWindow->p.updateMenu();
	}
}

void phoenix::pCheckItem::constructor(void) { }

void phoenix::pCheckItem::destructor(void)
{
	if (parentMenu) {
		parentMenu->remove(checkItem);
	}
}
