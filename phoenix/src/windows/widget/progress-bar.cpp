#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

phoenix::Geometry phoenix::pProgressBar::minimumGeometry(void)
{
	return { 0, 0, 0, 23 };
}

void phoenix::pProgressBar::setPosition(unsigned position)
{
	SendMessage(hwnd, PBM_SETPOS, (WPARAM)position, 0);
}

void phoenix::pProgressBar::constructor(void)
{
	hwnd = CreateWindow(PROGRESS_CLASS, L"", WS_CHILD | PBS_SMOOTH,
	    0, 0, 0, 0, parentWindow->p.hwnd, (HMENU)id,
	    GetModuleHandle(0), 0);

	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&progressBar);
	SendMessage(hwnd, PBM_SETRANGE, 0, MAKELPARAM(0, 100));
	SendMessage(hwnd, PBM_SETSTEP, MAKEWPARAM(1, 0), 0);
	setPosition(progressBar.state.position);
	synchronize();
}

void phoenix::pProgressBar::destructor(void)
{
	DestroyWindow(hwnd);
}

void phoenix::pProgressBar::orphan(void)
{
	destructor();
	constructor();
}
