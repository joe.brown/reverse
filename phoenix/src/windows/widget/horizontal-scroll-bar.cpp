#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

phoenix::Geometry phoenix::pHorizontalScrollBar::minimumGeometry(void)
{
	return { 0, 0, 0, 18 };
}

unsigned phoenix::pHorizontalScrollBar::position(void)
{
	return GetScrollPos(hwnd, SB_CTL);
}

void phoenix::pHorizontalScrollBar::setLength(unsigned length)
{
	length += (length == 0);
	SetScrollRange(hwnd, SB_CTL, 0, length - 1, TRUE);
	horizontalScrollBar.setPosition(0);
}

void phoenix::pHorizontalScrollBar::setPosition(unsigned position)
{
	SetScrollPos(hwnd, SB_CTL, position, TRUE);
}

void phoenix::pHorizontalScrollBar::constructor(void)
{
	hwnd = CreateWindow(L"SCROLLBAR", L"",
	    WS_CHILD | WS_TABSTOP | SBS_HORZ, 0, 0, 0, 0,
	    parentWindow->p.hwnd, (HMENU)id, GetModuleHandle(0), 0);

	SetWindowLongPtr(hwnd, GWLP_USERDATA,
	    (LONG_PTR)&horizontalScrollBar);
	unsigned position = horizontalScrollBar.state.position;
	setLength(horizontalScrollBar.state.length);
	horizontalScrollBar.setPosition(position);
	synchronize();
}

void phoenix::pHorizontalScrollBar::destructor(void)
{
	DestroyWindow(hwnd);
}

void phoenix::pHorizontalScrollBar::orphan(void)
{
	destructor();
	constructor();
}
