#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

phoenix::Geometry phoenix::pLineEdit::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(hfont, lineEdit.state.text);
	return { 0, 0, geometry.width + 12, geometry.height + 10 };
}

void phoenix::pLineEdit::setEditable(bool editable)
{
	SendMessage(hwnd, EM_SETREADONLY, editable == false, 0);
}

void phoenix::pLineEdit::setText(const nall::string &text)
{
	locked = true;
	SetWindowText(hwnd, nall::utf16_t(text));
	locked = false;
}

nall::string phoenix::pLineEdit::text(void)
{
	unsigned length = GetWindowTextLength(hwnd);
	wchar_t text[length + 1];
	GetWindowText(hwnd, text, length + 1);
	text[length] = 0;

	return (const char*)nall::utf8_t(text);
}

void phoenix::pLineEdit::constructor(void)
{
	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"",
	    WS_CHILD | WS_TABSTOP | ES_AUTOHSCROLL | ES_AUTOVSCROLL, 0, 0,
	    0, 0, parentWindow->p.hwnd, (HMENU)id, GetModuleHandle(0), 0);

	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&lineEdit);
	setDefaultFont();
	setEditable(lineEdit.state.editable);
	setText(lineEdit.state.text);
	synchronize();
}

void phoenix::pLineEdit::destructor(void)
{
	lineEdit.state.text = text();
	DestroyWindow(hwnd);
}

void phoenix::pLineEdit::orphan(void)
{
	destructor();
	constructor();
}
