#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

phoenix::Geometry phoenix::pVerticalScrollBar::minimumGeometry(void)
{
	return { 0, 0, 18, 0 };
}

unsigned phoenix::pVerticalScrollBar::position(void)
{
	return GetScrollPos(hwnd, SB_CTL);
}

void phoenix::pVerticalScrollBar::setLength(unsigned length)
{
	length += (length == 0);
	SetScrollRange(hwnd, SB_CTL, 0, length - 1, TRUE);
	verticalScrollBar.setPosition(0);
}

void phoenix::pVerticalScrollBar::setPosition(unsigned position)
{
	SetScrollPos(hwnd, SB_CTL, position, TRUE);
}

void phoenix::pVerticalScrollBar::constructor(void)
{
	hwnd = CreateWindow(L"SCROLLBAR", L"", WS_CHILD | SBS_VERT, 0, 0, 0,
    	    0, parentWindow->p.hwnd, (HMENU)id, GetModuleHandle(0), 0);

	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&verticalScrollBar);
	unsigned position = verticalScrollBar.state.position;
	setLength(verticalScrollBar.state.length);
	verticalScrollBar.setPosition(position);
	synchronize();
}

void phoenix::pVerticalScrollBar::destructor(void)
{
	DestroyWindow(hwnd);
}

void phoenix::pVerticalScrollBar::orphan(void)
{
	destructor();
	constructor();
}
