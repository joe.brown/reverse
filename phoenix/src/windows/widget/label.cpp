#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

LRESULT CALLBACK Label_windowProc(HWND hwnd, UINT msg, WPARAM wparam,
    LPARAM lparam)
{
	phoenix::Window *window = (phoenix::Window*)
	    GetWindowLongPtr(GetParent(hwnd), GWLP_USERDATA);
	phoenix::Label *label = (phoenix::Label*)GetWindowLongPtr(hwnd,
	    GWLP_USERDATA);

	if (!window || !label) {
		return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	if (msg == WM_GETDLGCODE) {
		return DLGC_STATIC | DLGC_WANTCHARS;
	}

	if (msg == WM_ERASEBKGND) {
		/*
		* background is erased during
		* WM_PAINT to prevent flickering
		*/
		return TRUE;
	}

	if (msg == WM_PAINT) {
		PAINTSTRUCT ps;
		RECT rc;
		BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rc);
		FillRect(ps.hdc, &rc, window->p.brush ?
		    window->p.brush : GetSysColorBrush(COLOR_3DFACE));
		SetBkColor(ps.hdc, window->p.brush ?
		    window->p.brushColor : GetSysColor(COLOR_3DFACE));
		SelectObject(ps.hdc, ((phoenix::Widget*)label)->p.hfont);

		unsigned length = GetWindowTextLength(hwnd);
		wchar_t text[length + 1];
		GetWindowText(hwnd, text, length + 1);
		text[length] = 0;
		DrawText(ps.hdc, text, -1, &rc,
		    DT_CALCRECT | DT_END_ELLIPSIS);
		unsigned height = rc.bottom;
		GetClientRect(hwnd, &rc);
		rc.top = (rc.bottom - height) / 2;
		rc.bottom = rc.top + height;
		DrawText(ps.hdc, text, -1, &rc, DT_LEFT | DT_END_ELLIPSIS);
		EndPaint(hwnd, &ps);
	}

	return DefWindowProc(hwnd, msg, wparam, lparam);
}
phoenix::Geometry phoenix::pLabel::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(hfont, label.state.text);
	return { 0, 0, geometry.width, geometry.height };
}

void phoenix::pLabel::setText(const nall::string &text)
{
	SetWindowText(hwnd, nall::utf16_t(text));
	InvalidateRect(hwnd, 0, false);
}

void phoenix::pLabel::constructor(void)
{
	hwnd = CreateWindow(L"phoenix_label", L"", WS_CHILD, 0, 0, 0, 0,
	    parentWindow->p.hwnd, (HMENU)id, GetModuleHandle(0), 0);

	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&label);
	setDefaultFont();
	setText(label.state.text);
	synchronize();
}

void phoenix::pLabel::destructor(void)
{
	DestroyWindow(hwnd);
}

void phoenix::pLabel::orphan(void)
{
	destructor();
	constructor();
}
