#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

bool phoenix::pWidget::enabled(void)
{
	return IsWindowEnabled(hwnd);
}

phoenix::Geometry phoenix::pWidget::minimumGeometry(void)
{
	return { 0, 0, 0, 0 };
}

void phoenix::pWidget::setEnabled(bool enabled)
{
	if (widget.state.abstract) {
		enabled = false;
	}

	if (sizable.state.layout &&
	    sizable.state.layout->enabled() == false) {
		enabled = false;
	}

	EnableWindow(hwnd, enabled);
}

void phoenix::pWidget::setFocused(void)
{
	SetFocus(hwnd);
}

void phoenix::pWidget::setFont(const nall::string &font)
{
	if (hfont) {
		DeleteObject(hfont);
	}

	hfont = pFont::create(font);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hfont, 0);
}

void phoenix::pWidget::setGeometry(const phoenix::Geometry &geometry)
{
	SetWindowPos(hwnd, NULL, geometry.x, geometry.y, geometry.width,
	    geometry.height, SWP_NOZORDER);
}

void phoenix::pWidget::setVisible(bool visible)
{
	if (widget.state.abstract) {
		visible = false;
	}

	if (sizable.state.layout &&
	    sizable.state.layout->visible() == false) {
		visible = false;
	}

	ShowWindow(hwnd, visible ? SW_SHOWNORMAL : SW_HIDE);
}

void phoenix::pWidget::constructor(void)
{
	hfont = pFont::create("Tahoma, 8");
	if (widget.state.abstract) {
		hwnd = CreateWindow(L"phoenix_label", L"", WS_CHILD, 0, 0,
		    0, 0, parentWindow->p.hwnd, (HMENU)id,
		GetModuleHandle(0), 0);
		SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&widget);
	}
}

void phoenix::pWidget::destructor(void)
{
	if (widget.state.abstract) {
		DestroyWindow(hwnd);
	}
}

void phoenix::pWidget::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pWidget::setDefaultFont(void)
{
	nall::string description = widget.state.font;
	if (description == "") {
		description = "Tahoma, 8";
	}

	hfont = pFont::create(description);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hfont, 0);
}

/*
* Calling Widget::setParent destroys widget and re-creates it: need to
* re-apply visibility and enabled status; called by each subclassed
* setParent() function.
*/
void phoenix::pWidget::synchronize(void)
{
	widget.setEnabled(widget.enabled());
	widget.setVisible(widget.visible());
}
