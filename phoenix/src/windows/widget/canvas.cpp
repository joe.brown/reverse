#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

LRESULT CALLBACK Canvas_windowProc(HWND hwnd, UINT msg, WPARAM wparam,
    LPARAM lparam)
{
	phoenix::Object *object =
	    (phoenix::Object*)GetWindowLongPtr(hwnd, GWLP_USERDATA);

	if (object == nullptr) {
		return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	if (!dynamic_cast<phoenix::Canvas*>(object)) {
		return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	phoenix::Canvas &canvas = (phoenix::Canvas&)*object;

	if (msg == WM_GETDLGCODE) {
		return DLGC_STATIC | DLGC_WANTCHARS;
	}

	if(msg == WM_PAINT) {
		canvas.p.paint();
		return TRUE;
	}

	if (msg == WM_MOUSEMOVE) {
		TRACKMOUSEEVENT tracker = 
		    { sizeof(TRACKMOUSEEVENT), TME_LEAVE, hwnd };
		TrackMouseEvent(&tracker);
		if (canvas.onMouseMove) {
			canvas.onMouseMove({ (int16_t)LOWORD(lparam),
			    (int16_t)HIWORD(lparam) });
		}
	}

	if (msg == WM_MOUSELEAVE) {
		if (canvas.onMouseLeave) {
			canvas.onMouseLeave();
		}
	}

	if (msg == WM_LBUTTONDOWN || msg == WM_MBUTTONDOWN ||
	    msg == WM_RBUTTONDOWN) {
		if (canvas.onMousePress) {
			switch(msg) {
			case WM_LBUTTONDOWN:
				canvas.onMousePress(
				    phoenix::Mouse::Button::Left);
				break;
			case WM_MBUTTONDOWN:
				canvas.onMousePress(
				    phoenix::Mouse::Button::Middle);
				break;
			case WM_RBUTTONDOWN:
				canvas.onMousePress(
				    phoenix::Mouse::Button::Right);
				break;
			}
		}
	}

	if (msg == WM_LBUTTONUP || msg == WM_MBUTTONUP ||
	    msg == WM_RBUTTONUP) {
		if (canvas.onMouseRelease) {
			switch(msg) {
			case WM_LBUTTONUP:
				canvas.onMouseRelease(
				    phoenix::Mouse::Button::Left);
				break;
			case WM_MBUTTONUP:
				canvas.onMouseRelease(
				    phoenix::Mouse::Button::Middle);
				break;
			case WM_RBUTTONUP:
				canvas.onMouseRelease(
				    phoenix::Mouse::Button::Right);
				break;
			}
		}
	}

	return DefWindowProc(hwnd, msg, wparam, lparam);
}

void phoenix::pCanvas::setSize(const phoenix::Size &size)
{
	delete[] data;
	data = new uint32_t[size.width * size.height];
}

void phoenix::pCanvas::update(void)
{
	size_t len = canvas.state.width * canvas.state.height;
	memcpy(data, canvas.state.data, len * sizeof(uint32_t));
	InvalidateRect(hwnd, 0, false);
}

void phoenix::pCanvas::constructor(void)
{
	size_t len = canvas.state.width * canvas.state.height;
	data = new uint32_t[len];
	memcpy(data, canvas.state.data, len * sizeof(uint32_t));
	hwnd = CreateWindow(L"phoenix_canvas", L"", WS_CHILD, 0, 0, 0, 0,
	    parentWindow->p.hwnd, (HMENU)id, GetModuleHandle(0), 0);
	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&canvas);
	synchronize();
}

void phoenix::pCanvas::destructor(void)
{
	DestroyWindow(hwnd);
	delete[] data;
}

void phoenix::pCanvas::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pCanvas::paint(void)
{
	RECT rc;
	GetClientRect(hwnd, &rc);
	unsigned width = canvas.state.width;
	unsigned height = canvas.state.height;

	BITMAPINFO bmi;
	memset(&bmi, 0, sizeof(BITMAPINFO));
	bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biWidth = width;
	/* GDI stores bitmaps upside down now; negative height flips it */
	bmi.bmiHeader.biHeight = -height;
	bmi.bmiHeader.biSizeImage = sizeof(uint32_t) * width * height;

	PAINTSTRUCT ps;
	BeginPaint(hwnd, &ps);
	SetDIBitsToDevice(ps.hdc, 0, 0, width, height, 0, 0, 0,
	    height, (void*)data, &bmi, DIB_RGB_COLORS);
	EndPaint(hwnd, &ps);
}
