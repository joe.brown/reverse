#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

bool phoenix::pCheckBox::checked(void)
{
	return SendMessage(hwnd, BM_GETCHECK, 0, 0);
}

phoenix::Geometry phoenix::pCheckBox::minimumGeometry(void)
{
	phoenix::Geometry geometry = pFont::geometry(hfont,
	    checkBox.state.text);
	return { 0, 0, geometry.width + 20, geometry.height + 4 };
}

void phoenix::pCheckBox::setChecked(bool checked)
{
	SendMessage(hwnd, BM_SETCHECK, (WPARAM)checked, 0);
}

void phoenix::pCheckBox::setText(const nall::string &text)
{
	SetWindowText(hwnd, nall::utf16_t(text));
}

void phoenix::pCheckBox::constructor(void)
{
	hwnd = CreateWindow(L"BUTTON", L"", WS_CHILD | WS_TABSTOP |
	    BS_CHECKBOX, 0, 0, 0, 0, parentWindow->p.hwnd,
	    (HMENU)id, GetModuleHandle(0), 0);
	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&checkBox);
	setDefaultFont();

	if (checkBox.state.checked) {
		setChecked(true);
	}

	setText(checkBox.state.text);
	synchronize();
}

void phoenix::pCheckBox::destructor(void)
{
	DestroyWindow(hwnd);
}

void phoenix::pCheckBox::orphan(void)
{
	destructor();
	constructor();
}
