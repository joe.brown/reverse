#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

void phoenix::pTextEdit::setCursorPosition(unsigned position)
{
	/* Edit_SetSel takes signed type. */
	if (position == ~0) {
		position >>= 1;
	}

	Edit_SetSel(hwnd, position, position);
	Edit_ScrollCaret(hwnd);
}

void phoenix::pTextEdit::setEditable(bool editable)
{
	SendMessage(hwnd, EM_SETREADONLY, editable == false, (LPARAM)0);
}

void phoenix::pTextEdit::setText(const nall::string &text)
{
	locked = true;
	nall::string output = text;
	output.replace("\r", "");
	output.replace("\n", "\r\n");
	SetWindowText(hwnd, nall::utf16_t(output));
	locked = false;
}

void phoenix::pTextEdit::setWordWrap(bool wordWrap)
{
	/*
	* ES_AUTOHSCROLL cannot be changed after widget creation.  As a
	* result, we must destroy and re-create the widget to change
	* this setting.
	*/
	orphan();
}

nall::string phoenix::pTextEdit::text(void)
{
	unsigned length = GetWindowTextLength(hwnd);
	wchar_t buffer[length + 1];

	GetWindowText(hwnd, buffer, length + 1);
	buffer[length] = 0;
	nall::string text = (const char*)nall::utf8_t(buffer);
	text.replace("\r", "");
	return text;
}

void phoenix::pTextEdit::constructor(void)
{
	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"", WS_CHILD |
	    WS_TABSTOP | WS_VSCROLL | ES_AUTOVSCROLL | ES_MULTILINE |
	    ES_WANTRETURN | (textEdit.state.wordWrap == false ?
	    WS_HSCROLL | ES_AUTOHSCROLL : 0), 0, 0, 0, 0,
	    parentWindow->p.hwnd, (HMENU)id, GetModuleHandle(0), 0);

	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&textEdit);
	setDefaultFont();
	setCursorPosition(textEdit.state.cursorPosition);
	setEditable(textEdit.state.editable);
	setText(textEdit.state.text);
	synchronize();
}

void phoenix::pTextEdit::destructor(void)
{
	textEdit.state.text = text();
	DestroyWindow(hwnd);
}

void phoenix::pTextEdit::orphan(void)
{
	destructor();
	constructor();
}
