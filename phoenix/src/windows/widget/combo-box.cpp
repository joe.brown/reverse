#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

void phoenix::pComboBox::append(const nall::string &text)
{
	SendMessage(hwnd, CB_ADDSTRING, 0,
	    (LPARAM)(wchar_t*)nall::utf16_t(text));
	if (SendMessage(hwnd, CB_GETCOUNT, 0, 0) == 1) {
		setSelection(0);
	}
}

phoenix::Geometry phoenix::pComboBox::minimumGeometry(void)
{
	unsigned maximumWidth = 0;
	for (auto &text : comboBox.state.text) {
		maximumWidth = nall::max(maximumWidth,
		    pFont::geometry(hfont, text).width);
	}

	return { 0, 0, maximumWidth + 24,
	    pFont::geometry(hfont, " ").height + 10 };
}

void phoenix::pComboBox::reset(void)
{
	SendMessage(hwnd, CB_RESETCONTENT, 0, 0);
}

unsigned phoenix::pComboBox::selection(void)
{
	return SendMessage(hwnd, CB_GETCURSEL, 0, 0);
}

void phoenix::pComboBox::setSelection(unsigned row)
{
	SendMessage(hwnd, CB_SETCURSEL, row, 0);
}

void phoenix::pComboBox::constructor(void)
{
	hwnd = CreateWindow(L"COMBOBOX", L"",
	    WS_CHILD | WS_TABSTOP | CBS_DROPDOWNLIST | CBS_HASSTRINGS,
	    0, 0, 0, 0, parentWindow->p.hwnd, (HMENU)id,
	    GetModuleHandle(0), 0);
	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&comboBox);
	setDefaultFont();

	for (auto &text : comboBox.state.text) {
		append(text);
	}

	setSelection(comboBox.state.selection);
	synchronize();
}

void phoenix::pComboBox::destructor(void)
{
	DestroyWindow(hwnd);
}

void phoenix::pComboBox::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pComboBox::setGeometry(const phoenix::Geometry &geometry)
{
	SetWindowPos(hwnd, NULL, geometry.x, geometry.y,
	    geometry.width, 1, SWP_NOZORDER);
	RECT rc;
	GetWindowRect(hwnd, &rc);

	unsigned adjustedHeight = geometry.height - ((rc.bottom - rc.top) -
	    SendMessage(hwnd, CB_GETITEMHEIGHT, (WPARAM)-1, 0));
	SendMessage(hwnd, CB_SETITEMHEIGHT, (WPARAM)-1, adjustedHeight);
}
