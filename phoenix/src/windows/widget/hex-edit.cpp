#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

LRESULT CALLBACK HexEdit_windowProc(HWND hwnd, UINT msg, WPARAM wparam,
    LPARAM lparam)
{
	phoenix::HexEdit &hexEdit = *(phoenix::HexEdit*)
	    GetWindowLongPtr(hwnd, GWLP_USERDATA);
	if (msg == WM_CHAR) {
		if (hexEdit.p.keyPress(wparam)) {
			return 0;
		}
	}

	return hexEdit.p.windowProc(hwnd, msg, wparam, lparam);
}

void phoenix::pHexEdit::setColumns(unsigned columns)
{
	update();
}

void phoenix::pHexEdit::setLength(unsigned length)
{
	update();
}

void phoenix::pHexEdit::setOffset(unsigned offset)
{
	update();
}

void phoenix::pHexEdit::setRows(unsigned rows)
{
	update();
}

void phoenix::pHexEdit::update(void)
{
	if (!hexEdit.onRead) {
		SetWindowText(hwnd, L"");
		return;
	}

	unsigned cursorPosition = Edit_GetSel(hwnd);

	nall::string output;
	unsigned offset = hexEdit.state.offset;

	for (unsigned row = 0; row < hexEdit.state.rows; row++) {
		output.append(nall::hex<8>(offset));
		output.append("  ");

		nall::string hexdata;
		nall::string ansidata = " ";
		for (unsigned column = 0; column < hexEdit.state.columns;
		    column++) {
			if (offset < hexEdit.state.length) {
				uint8_t data = hexEdit.onRead(offset++);
				hexdata.append(nall::hex<2>(data));
				hexdata.append(" ");
				char buffer[2] = { data >= 0x20 &&
				    data <= 0x7e ? (char)data : '.', 0 };
				ansidata.append(buffer);
			} else {
				hexdata.append("   ");
				ansidata.append(" ");
			}
		}

		output.append(hexdata);
		output.append(ansidata);
		if (offset >= hexEdit.state.length) {
			break;
		}

		if (row != hexEdit.state.rows - 1) {
			output.append("\r\n");
		}
	}

	SetWindowText(hwnd, nall::utf16_t(output));
	Edit_SetSel(hwnd, LOWORD(cursorPosition), HIWORD(cursorPosition));
}

void phoenix::pHexEdit::constructor(void)
{
	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"",
	    WS_CHILD | WS_TABSTOP | ES_READONLY |
	    ES_MULTILINE | ES_WANTRETURN, 0, 0, 0, 0,
	    parentWindow->p.hwnd, (HMENU)id, GetModuleHandle(0), 0);

	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&hexEdit);
	setDefaultFont();
	update();

	windowProc = (LRESULT CALLBACK (*)(HWND, UINT, LPARAM, WPARAM))
	    GetWindowLongPtr(hwnd, GWLP_WNDPROC);
	SetWindowLongPtr(hwnd, GWLP_WNDPROC, (LONG_PTR)HexEdit_windowProc);
	synchronize();
}

void phoenix::pHexEdit::destructor(void)
{
	DestroyWindow(hwnd);
}

void phoenix::pHexEdit::orphan(void)
{
	destructor();
	constructor();
}

bool phoenix::pHexEdit::keyPress(unsigned scancode)
{
	if (!hexEdit.onRead) {
		return false;
	}

	unsigned position = LOWORD(Edit_GetSel(hwnd));
	unsigned lineWidth = 10 + (hexEdit.state.columns * 3) +
	    1 + hexEdit.state.columns + 2;
	unsigned cursorY = position / lineWidth;
	unsigned cursorX = position % lineWidth;

	/* Convert scancode to hex nibble. */
	if (scancode >= '0' && scancode <= '9') {
		scancode = scancode - '0';
	} else if (scancode >= 'A' && scancode <= 'F') {
		scancode = scancode - 'A' + 10;
	} else if (scancode >= 'a' && scancode <= 'f') {
		scancode = scancode - 'a' + 10;
	} else {
		return false;
	}

	/* XXX: There has to be a better way... */
	if (cursorX >= 10) {
		/* not on an offset */
		cursorX -= 10;
		if ((cursorX % 3) != 2) {
			/* not on a space; 0 = height, 1 = low */
			bool cursorNibble = (cursorX % 3) == 1;
			cursorX /= 3;
			if (cursorX < hexEdit.state.columns) {
				/* not in ANSI region */
				unsigned offset = hexEdit.state.offset +
				    (cursorY * hexEdit.state.columns +
				    cursorX);

				/* do not edit past end of data */
				if (offset >= hexEdit.state.length) {
					return false;
				}

				uint8_t data = hexEdit.onRead(offset);

				/* write modified value */
				if (cursorNibble == 1) {
					data = (data & 0xf0) |
					    (scancode << 0);
				} else {
					data = (data & 0x0f) |
					    (scancode << 4);
				}

				if (hexEdit.onWrite) {
					hexEdit.onWrite(offset, data);
				}

				/* auto-advance cursor to
				* next nibble or byte */
				position++;
				if (cursorNibble && cursorX !=
				    hexEdit.state.columns - 1) {
					position++;
				}

				Edit_SetSel(hwnd, position, position);

				/* refresh output to reflect modified data */
				update();
			}
		}
	}

	return true;
}
