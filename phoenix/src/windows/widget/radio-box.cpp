#include "phoenix/phoenix.h"
#include "phoenix/platform/win32.h"

bool phoenix::pRadioBox::checked(void)
{
	return SendMessage(hwnd, BM_GETCHECK, 0, 0);
}

phoenix::Geometry phoenix::pRadioBox::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(hfont, radioBox.state.text);
	return { 0, 0, geometry.width + 20, geometry.height + 4 };
}

void phoenix::pRadioBox::setChecked(void)
{
	for (auto &item : radioBox.state.group) {
		SendMessage(item.p.hwnd, BM_SETCHECK,
		    (WPARAM)(&item == &radioBox), 0);
	}
}

void phoenix::pRadioBox::setGroup(
    const nall::array<phoenix::RadioBox&> &group) { }

void phoenix::pRadioBox::setText(const nall::string &text)
{
	SetWindowText(hwnd, nall::utf16_t(text));
}

void phoenix::pRadioBox::constructor(void)
{
	hwnd = CreateWindow(L"BUTTON", L"", WS_CHILD | WS_TABSTOP |
	    BS_RADIOBUTTON, 0, 0, 0, 0, parentWindow->p.hwnd,
	    (HMENU)id, GetModuleHandle(0), 0);

	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&radioBox);
	setDefaultFont();
	if (radioBox.state.checked) {
		setChecked();
	}

	setText(radioBox.state.text);
	synchronize();
}

void phoenix::pRadioBox::destructor(void)
{
	DestroyWindow(hwnd);
}

void phoenix::pRadioBox::orphan(void)
{
	destructor();
	constructor();
}
