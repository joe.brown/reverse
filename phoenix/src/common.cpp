#ifndef PHOENIX_COMMON_CPP
#define PHOENIX_COMMON_CPP

#include <phoenix/phoenix.h>

#if defined(PHOENIX_QT)
  #include <phoenix/platform/qt4.h>
#elif defined(PHOENIX_GTK)
  #include <phoenix/platform/gtk2.h>
#elif defined(PHOENIX_WIN32)
  #include <phoenix/platform/win32.h>
#else
  #error "Unknown GUI toolkit."
#endif

namespace phoenix {

void FixedLayout::append(Sizable &sizable, const Geometry &geometry)
{
	children.append({ &sizable, geometry });
	synchronizeLayout();
	if (window()) {
		window()->synchronizeLayout();
	}
}

void FixedLayout::append(Sizable &sizable)
{
	for (auto &child : children) {
		if (child.sizable == &sizable) {
			return;
		}
	}

	Layout::append(sizable);
	if (window()) {
		window()->synchronizeLayout();
	}
}

bool FixedLayout::enabled(void)
{
	if (layout()) {
		return state.enabled && layout()->enabled();
	}

	return state.enabled;
}

Geometry FixedLayout::minimumGeometry(void)
{
	unsigned width = MinimumSize, height = MinimumSize;
	for (auto &child : children) {
		width  = nall::max(width,
		    child.sizable->minimumGeometry().width);
		height = nall::max(height,
		    child.sizable->minimumGeometry().height);
	}

	return { 0, 0, width, height };
}

void FixedLayout::remove(Sizable &sizable)
{
	for (unsigned n = 0; n < children.size(); n++) {
		if (children[n].sizable == &sizable) {
			children.remove(n);
			Layout::remove(sizable);

			if (window()) {
				window()->synchronizeLayout();
			}

			break;
		}
	}
}

void FixedLayout::reset(void)
{
	for (auto &child : children) {
		if (window() && dynamic_cast<Widget*>(child.sizable)) {
			window()->remove((Widget&)*child.sizable);
		}
	}
}

void FixedLayout::setEnabled(bool enabled)
{
	state.enabled = enabled;
	for (auto &child : children) {
		child.sizable->setVisible(
		    dynamic_cast<Widget*>(child.sizable) ?
		    child.sizable->enabled() : enabled);
	}
}

void FixedLayout::setGeometry(const Geometry &geometry) { }

void FixedLayout::setVisible(bool visible)
{
	state.visible = visible;
	for (auto &child : children) {
		child.sizable->setVisible(
		    dynamic_cast<Widget*>(child.sizable) ?
		    child.sizable->visible() : visible);
	}
}

void FixedLayout::synchronizeLayout(void)
{
	for (auto &child : children) {
		Layout::append(*child.sizable);
		child.sizable->setGeometry(child.geometry);
	}
}

bool FixedLayout::visible(void)
{
	if (layout()) {
		return state.visible && layout()->visible();
	}

	return state.visible;
}

FixedLayout::FixedLayout(void)
{
	state.enabled = true;
	state.visible = true;
}

FixedLayout::~FixedLayout(void)
{
	while (children.size()) {
		remove(*children[0].sizable);
	}
}


void HorizontalLayout::append(Sizable &sizable, const Size &size,
    unsigned spacing)
{
	for (auto &child : children) {
		if (child.sizable == &sizable) {
			return;
		}
	}

	children.append({ &sizable, size.width, size.height, spacing });
	synchronizeLayout();
	if (window()) {
		window()->synchronizeLayout();
	}
}

void HorizontalLayout::append(Sizable &sizable)
{
	for (auto &child : children) {
		if (child.sizable == &sizable) {
			return;
		}
	}

	Layout::append(sizable);
	if (window()) {
		window()->synchronizeLayout();
	}
}

bool HorizontalLayout::enabled(void)
{
	if (layout()) {
		return state.enabled && layout()->enabled();
	}

	return state.enabled;
}

Geometry HorizontalLayout::minimumGeometry(void)
{
	unsigned width = 0, height = 0;

	for (auto &child : children) {
		width += child.spacing;
		if (child.width == MinimumSize ||
		    child.width == MaximumSize) {
			width += child.sizable->minimumGeometry().width;
			continue;
		}
		width += child.width;
	}

	for (auto &child : children) {
		if (child.height == MinimumSize ||
		    child.height == MaximumSize) {
			height = nall::max(height,
			    child.sizable->minimumGeometry().height);
			continue;
		}
		height = nall::max(height, child.height);
	}

	return { 0, 0, state.margin * 2 + width,
	    state.margin * 2 + height };
}

void HorizontalLayout::remove(Sizable &sizable)
{
	for (unsigned n = 0; n < children.size(); n++) {
		if (children[n].sizable == &sizable) {
			if (dynamic_cast<Layout*>(children[n].sizable)) {
				Layout *layout =
				    (Layout*)children[n].sizable;
				layout->reset();
			}

			children.remove(n);
			Layout::remove(sizable);

			if (window()) {
				window()->synchronizeLayout();
			}
			break;
		}
	}
}

void HorizontalLayout::reset(void)
{
	for (auto &child : children) {
		if (window() && dynamic_cast<Layout*>(child.sizable)) {
			((Layout*)child.sizable)->reset();
		}

		if (window() && dynamic_cast<Widget*>(child.sizable)) {
			window()->remove((Widget&)*child.sizable);
		}
	}
}

void HorizontalLayout::setAlignment(double alignment)
{
	state.alignment = nall::max(0.0, nall::min(1.0, alignment));
}

void HorizontalLayout::setEnabled(bool enabled)
{
	state.enabled = enabled;
	for (auto &child : children) {
		child.sizable->setEnabled(
		    dynamic_cast<Widget*>(child.sizable) ?
		    child.sizable->enabled() : enabled);
	}
}

void HorizontalLayout::setGeometry(const Geometry &containerGeometry)
{
	auto children = this->children;
	for (auto &child : children) {
		if (child.width  == MinimumSize) {
			child.width =
			    child.sizable->minimumGeometry().width;
		}

		if (child.height == MinimumSize) {
			child.height =
			    child.sizable->minimumGeometry().height;
		}
	}

	Geometry geometry = containerGeometry;
	geometry.x += state.margin;
	geometry.y += state.margin;
	geometry.width -= state.margin * 2;
	geometry.height -= state.margin * 2;

	unsigned minimumWidth = 0, maximumWidthCounter = 0;
	for (auto &child : children) {
		if (child.width == MaximumSize) {
			maximumWidthCounter++;
		}

		if (child.width != MaximumSize) {
			minimumWidth += child.width;
		}

		minimumWidth += child.spacing;
	}

	for (auto &child : children) {
		if (child.width == MaximumSize) {
			child.width = (geometry.width - minimumWidth) /
			    maximumWidthCounter;
		}

		if (child.height == MaximumSize) {
			child.height = geometry.height;
		}
	}

	unsigned maximumHeight = 0;
	for (auto &child : children) {
		maximumHeight = nall::max(maximumHeight, child.height);
	}

	for (auto &child : children) {
		unsigned pivot = (maximumHeight - child.height) *
		    state.alignment;
		Geometry childGeometry = { geometry.x, geometry.y + pivot,
		    child.width, child.height };
		child.sizable->setGeometry(childGeometry);

		geometry.x += child.width + child.spacing;
		geometry.width -= child.width + child.spacing;
	}
}

void HorizontalLayout::setMargin(unsigned margin)
{
	state.margin = margin;
}

void HorizontalLayout::setVisible(bool visible)
{
	state.visible = visible;
	for (auto &child : children) {
		child.sizable->setVisible(
		    dynamic_cast<Widget*>(child.sizable) ?
		    child.sizable->visible() : visible);
	}
}

void HorizontalLayout::synchronizeLayout(void)
{
	for (auto &child : children) {
		Layout::append(*child.sizable);
	}
}

bool HorizontalLayout::visible(void)
{
	if (layout()) {
		return state.visible && layout()->visible();
	}

	return state.visible;
}

HorizontalLayout::HorizontalLayout(void)
{
	state.alignment = 0.5;
	state.enabled = true;
	state.margin = 0;
	state.visible = true;
}

HorizontalLayout::~HorizontalLayout(void)
{
	while (children.size()) {
		remove(*children[0].sizable);
	}
}


void VerticalLayout::append(Sizable &sizable, const Size &size,
    unsigned spacing)
{
	for (auto &child : children) {
		if (child.sizable == &sizable) {
			return;
		}
	}

	children.append({ &sizable, size.width, size.height, spacing });
	synchronizeLayout();
	if (window()) {
		window()->synchronizeLayout();
	}
}

void VerticalLayout::append(Sizable &sizable)
{
	for (auto &child : children) {
		if (child.sizable == &sizable) {
			return;
		}
	}

	Layout::append(sizable);
	if (window()) {
		window()->synchronizeLayout();
	}
}

bool VerticalLayout::enabled(void)
{
	if (layout()) {
		return state.enabled && layout()->enabled();
	}

	return state.enabled;
}

Geometry VerticalLayout::minimumGeometry(void)
{
	unsigned width = 0, height = 0;

	for (auto &child : children) {
		if (child.width == MinimumSize ||
		    child.width == MaximumSize) {
			width = nall::max(width,
			    child.sizable->minimumGeometry().width);
			continue;
		}

		width = nall::max(width, child.width);
	}

	for (auto &child : children) {
		height += child.spacing;
		if (child.height == MinimumSize ||
		    child.height == MaximumSize) {
			height += child.sizable->minimumGeometry().height;
			continue;
		}

		height += child.height;
	}

	return { 0, 0, state.margin * 2 + width,
	    state.margin * 2 + height };
}

void VerticalLayout::remove(Sizable &sizable)
{
	for (unsigned n = 0; n < children.size(); n++) {
		if (children[n].sizable == &sizable) {
			if (dynamic_cast<Layout*>(children[n].sizable)) {
				Layout *layout =
				    (Layout*)children[n].sizable;
				layout->reset();
			}

			children.remove(n);
			Layout::remove(sizable);
			if (window()) {
				window()->synchronizeLayout();
			}
			break;
		}
	}
}

void VerticalLayout::reset(void)
{
	for (auto &child : children) {
		if (window() && dynamic_cast<Layout*>(child.sizable)) {
			((Layout*)child.sizable)->reset();
		}

		if (window() && dynamic_cast<Widget*>(child.sizable)) {
			window()->remove((Widget&)*child.sizable);
		}
	}
}

void VerticalLayout::setAlignment(double alignment)
{
	state.alignment = nall::max(0.0, nall::min(1.0, alignment));
}

void VerticalLayout::setEnabled(bool enabled)
{
	state.enabled = enabled;
	for (auto &child : children) {
		child.sizable->setEnabled(
		    dynamic_cast<Widget*>(child.sizable) ?
		    child.sizable->enabled() : enabled);
	}
}

void VerticalLayout::setGeometry(const Geometry &containerGeometry)
{
	auto children = this->children;
	for (auto &child : children) {
		if (child.width  == MinimumSize) {
			child.width =
			    child.sizable->minimumGeometry().width;
		}

		if (child.height == MinimumSize) {
			child.height =
			    child.sizable->minimumGeometry().height;
		}
	}

	Geometry geometry = containerGeometry;
	geometry.x += state.margin;
	geometry.y += state.margin;
	geometry.width -= state.margin * 2;
	geometry.height -= state.margin * 2;

	unsigned minimumHeight = 0, maximumHeightCounter = 0;
	for (auto &child : children) {
		if (child.height == MaximumSize) {
			maximumHeightCounter++;
		}

		if (child.height != MaximumSize) {
			minimumHeight += child.height;
		}

		minimumHeight += child.spacing;
	}

	for (auto &child : children) {
		if (child.width  == MaximumSize) {
			child.width = geometry.width;
		}

		if (child.height == MaximumSize) {
			child.height = (geometry.height - minimumHeight) /
			    maximumHeightCounter;
		}
	}

	unsigned maximumWidth = 0;
	for (auto &child : children) {
		maximumWidth = nall::max(maximumWidth, child.width);
	}

	for (auto &child : children) {
		unsigned pivot = (maximumWidth - child.width) *
		    state.alignment;
		Geometry childGeometry = { geometry.x + pivot, geometry.y,
		    child.width, child.height };
		child.sizable->setGeometry(childGeometry);

		geometry.y += child.height + child.spacing;
		geometry.height -= child.height + child.spacing;
	}
}

void VerticalLayout::setMargin(unsigned margin)
{
	state.margin = margin;
}

void VerticalLayout::setVisible(bool visible)
{
	state.visible = visible;
	for (auto &child : children) {
		child.sizable->setVisible(
		    dynamic_cast<Widget*>(child.sizable) ?
		    child.sizable->visible() : visible);
	}
}

void VerticalLayout::synchronizeLayout(void)
{
	for (auto &child : children) {
		Layout::append(*child.sizable);
	}
}

bool VerticalLayout::visible(void)
{
	if (layout()) {
		return state.visible && layout()->visible();
	}

	return state.visible;
}

VerticalLayout::VerticalLayout(void)
{
	state.alignment = 0.0;
	state.enabled = true;
	state.margin = 0;
	state.visible = true;
}

VerticalLayout::~VerticalLayout(void)
{
	while (children.size()) {
		remove(*children[0].sizable);
	}
}

static bool OS_quit = false;
Window Window::None;

uint32_t Color::rgb(void) const
{
	return (255 << 24) + (red << 16) + (green << 8) + (blue << 0);
}

uint32_t Color::rgba(void) const
{
	return (alpha << 24) + (red << 16) + (green << 8) + (blue << 0);
}

Position Geometry::position(void) const
{
	return { x, y };
}

Size Geometry::size(void) const
{
	return { width, height };
}

nall::string Geometry::text(void) const
{
	return { x, ",", y, ",", width, ",", height };
}

/*
* XXX: integer() and decimal() are in the nall namespace.  Why don't
* I have to specify that?  I need to look into this eventually.
*/
Geometry::Geometry(const nall::string &text)
{
	nall::lstring part = text.split(",");
	x = integer(part(0, "256"));
	y = integer(part(1, "256"));
	width = decimal(part(2, "256"));
	height = decimal(part(3, "256"));
}

Geometry Font::geometry(const nall::string &text)
{
	return pFont::geometry(description, text);
}

Font::Font(const nall::string &description): description(description) { }

Size Desktop::size(void)
{
	return pDesktop::size();
}

Geometry Desktop::workspace(void)
{
	return pDesktop::workspace();
}

bool Keyboard::pressed(Keyboard::Scancode scancode)
{
	return pKeyboard::pressed(scancode);
}

bool Keyboard::released(Keyboard::Scancode scancode)
{
	return !pressed(scancode);
}

nall::array<bool> Keyboard::state(void)
{
	return pKeyboard::state();
}

Position Mouse::position(void)
{
	return pMouse::position();
}

bool Mouse::pressed(Mouse::Button button)
{
	return pMouse::pressed(button);
}

bool Mouse::released(Mouse::Button button)
{
	return !pressed(button);
}

nall::string DialogWindow::fileOpen_(Window &parent,
    const nall::string &path, const nall::lstring &filter_)
{
	auto filter = filter_;
	if (filter.size() == 0) {
		filter.append("All files (*)");
	}

	return pDialogWindow::fileOpen(parent, path, filter);
}

nall::string DialogWindow::fileSave_(Window &parent,
    const nall::string &path, const nall::lstring &filter_)
{
	auto filter = filter_;
	if (filter.size() == 0) {
		filter.append("All files (*)");
	}

	return pDialogWindow::fileSave(parent, path, filter);
}

nall::string DialogWindow::folderSelect(Window &parent,
    const nall::string &path)
{
	return pDialogWindow::folderSelect(parent, path);
}

MessageWindow::Response MessageWindow::information(Window &parent,
    const nall::string &text, MessageWindow::Buttons buttons)
{
	return pMessageWindow::information(parent, text, buttons);
}

MessageWindow::Response MessageWindow::question(Window &parent,
    const nall::string &text, MessageWindow::Buttons buttons)
{
	return pMessageWindow::question(parent, text, buttons);
}

MessageWindow::Response MessageWindow::warning(Window &parent,
    const nall::string &text, MessageWindow::Buttons buttons)
{
	return pMessageWindow::warning(parent, text, buttons);
}

MessageWindow::Response MessageWindow::critical(Window &parent,
    const nall::string &text, MessageWindow::Buttons buttons)
{
	return pMessageWindow::critical(parent, text, buttons);
}

Object::Object(pObject &p): p(p)
{
	OS::initialize();
	p.constructor();
}

Object::~Object(void)
{
	p.destructor();
	delete &p;
}

void OS::main(void)
{
	return pOS::main();
}

bool OS::pendingEvents(void)
{
	return pOS::pendingEvents();
}

void OS::processEvents(void)
{
	return pOS::processEvents();
}

void OS::quit(void)
{
	OS_quit = true;
	return pOS::quit();
}

void OS::initialize(void)
{
	static bool initialized = false;
	if (initialized == false) {
		initialized = true;
		return pOS::initialize();
	}
}

void Timer::setEnabled(bool enabled)
{
	state.enabled = enabled;
	return p.setEnabled(enabled);
}

void Timer::setInterval(unsigned milliseconds)
{
	state.milliseconds = milliseconds;
	return p.setInterval(milliseconds);
}

Timer::Timer(void): state(*new State),
    base_from_member<pTimer&>(*new pTimer(*this)),
    Object(base_from_member<pTimer&>::value),
    p(base_from_member<pTimer&>::value)
{
	p.constructor();
}

Timer::~Timer(void)
{
	p.destructor();
	delete &state;
}

void Window::append_(Layout &layout)
{
	if (state.layout.append(layout)) {
		((Sizable&)layout).state.window = this;
		((Sizable&)layout).state.layout = 0;
		p.append(layout);
		layout.synchronizeLayout();
	}
}

void Window::append_(Menu &menu)
{
	if (state.menu.append(menu)) {
		((Action&)menu).state.window = this;
		p.append(menu);
	}
}

void Window::append_(Widget &widget)
{
	if (state.widget.append(widget)) {
		((Sizable&)widget).state.window = this;
		p.append(widget);
	}
}

Color Window::backgroundColor(void)
{
	return p.backgroundColor();
}

Geometry Window::frameGeometry(void)
{
	Geometry geometry = p.geometry();
	Geometry margin = p.frameMargin();
	return {
	    geometry.x - margin.x, geometry.y - margin.y,
	    geometry.width + margin.width, geometry.height + margin.height
	};
}

Geometry Window::frameMargin(void)
{
	return p.frameMargin();
}

bool Window::focused(void)
{
	return p.focused();
}

bool Window::fullScreen(void)
{
	return state.fullScreen;
}

Geometry Window::geometry(void)
{
	return p.geometry();
}

void Window::ignore(void)
{
	state.ignore = true;
}

void Window::remove_(Layout &layout)
{
	if (state.layout.remove(layout)) {
		p.remove(layout);
		((Sizable&)layout).state.window = 0;
	}
}

void Window::remove_(Menu &menu)
{
	if (state.menu.remove(menu)) {
		p.remove(menu);
		((Action&)menu).state.window = 0;
	}
}

void Window::remove_(Widget &widget)
{
	if (state.widget.remove(widget)) {
		p.remove(widget);
		((Sizable&)widget).state.window = 0;
	}
}

void Window::setBackgroundColor(const Color &color)
{
	state.backgroundColorOverride = true;
	state.backgroundColor = color;
	return p.setBackgroundColor(color);
}

void Window::setFrameGeometry(const Geometry &geometry)
{
	Geometry margin = p.frameMargin();
	return setGeometry({
	    geometry.x + margin.x, geometry.y + margin.y,
	    geometry.width - margin.width, geometry.height - margin.height
	});
}

void Window::setFocused(void)
{
	return p.setFocused();
}

void Window::setFullScreen(bool fullScreen)
{
	state.fullScreen = fullScreen;
	return p.setFullScreen(fullScreen);
}

void Window::setGeometry(const Geometry &geometry)
{
	state.geometry = geometry;
	return p.setGeometry(geometry);
}

void Window::setMenuFont(const nall::string &font)
{
	state.menuFont = font;
	return p.setMenuFont(font);
}

void Window::setMenuVisible(bool visible)
{
	state.menuVisible = visible;
	return p.setMenuVisible(visible);
}

void Window::setResizable(bool resizable)
{
	state.resizable = resizable;
	return p.setResizable(resizable);
}

void Window::setStatusFont(const nall::string &font)
{
	state.statusFont = font;
	return p.setStatusFont(font);
}

void Window::setStatusText(const nall::string &text)
{
	state.statusText = text;
	return p.setStatusText(text);
}

void Window::setStatusVisible(bool visible)
{
	state.statusVisible = visible;
	return p.setStatusVisible(visible);
}

void Window::setTitle(const nall::string &text)
{
	state.title = text;
	return p.setTitle(text);
}

void Window::setVisible(bool visible)
{
	state.visible = visible;
	synchronizeLayout();
	return p.setVisible(visible);
}

void Window::setWidgetFont(const nall::string &font)
{
	state.widgetFont = font;
	return p.setWidgetFont(font);
}

nall::string Window::statusText(void)
{
	return state.statusText;
}

void Window::synchronizeLayout(void)
{
	if (visible() && OS_quit == false) {
		setGeometry(geometry());
	}
}

bool Window::visible(void)
{
	return state.visible;
}

Window::Window(void): state(*new State),
    base_from_member<pWindow&>(*new pWindow(*this)),
    Object(base_from_member<pWindow&>::value),
    p(base_from_member<pWindow&>::value)
{
	p.constructor();
}

Window::~Window(void)
{
	p.destructor();
	delete &state;
}

bool Action::enabled(void)
{
	return state.enabled;
}

void Action::setEnabled(bool enabled)
{
	state.enabled = enabled;
	return p.setEnabled(enabled);
}

void Action::setVisible(bool visible)
{
	state.visible = visible;
	return p.setVisible(visible);
}

bool Action::visible(void)
{
	return state.visible;
}

Action::Action(pAction &p): state(*new State), Object(p), p(p)
{
	p.constructor();
}

Action::~Action(void)
{
	p.destructor();
	delete &state;
}

void Menu::append(const nall::array<Action&> &list)
{
	for (auto &action : list) {
		if (state.action.append(action)) {
			action.state.menu = this;
			p.append(action);
		}
	}
}

void Menu::remove(const nall::array<Action&> &list)
{
	for (auto &action : list) {
		if (state.action.remove(action)) {
			action.state.menu = 0;
			return p.remove(action);
		}
	}
}

void Menu::setImage(const nall::image &image)
{
	state.image = image;
	return p.setImage(image);
}

void Menu::setText(const nall::string &text)
{
	state.text = text;
	return p.setText(text);
}

Menu::Menu(void): state(*new State),
    base_from_member<pMenu&>(*new pMenu(*this)),
    Action(base_from_member<pMenu&>::value),
    p(base_from_member<pMenu&>::value)
{
	p.constructor();
}

Menu::~Menu(void)
{
	p.destructor();
	delete &state;
}

Separator::Separator(void):
    base_from_member<pSeparator&>(*new pSeparator(*this)),
    Action(base_from_member<pSeparator&>::value),
    p(base_from_member<pSeparator&>::value)
{
	p.constructor();
}

Separator::~Separator(void)
{
	p.destructor();
}

void Item::setImage(const nall::image &image)
{
	state.image = image;
	return p.setImage(image);
}

void Item::setText(const nall::string &text)
{
	state.text = text;
	return p.setText(text);
}

Item::Item(void): state(*new State),
    base_from_member<pItem&>(*new pItem(*this)),
    Action(base_from_member<pItem&>::value),
    p(base_from_member<pItem&>::value)
{
	p.constructor();
}

Item::~Item(void)
{
	p.destructor();
	delete &state;
}

bool CheckItem::checked(void)
{
	return p.checked();
}

void CheckItem::setChecked(bool checked)
{
	state.checked = checked;
	return p.setChecked(checked);
}

void CheckItem::setText(const nall::string &text)
{
	state.text = text;
	return p.setText(text);
}

CheckItem::CheckItem(void): state(*new State),
    base_from_member<pCheckItem&>(*new pCheckItem(*this)),
    Action(base_from_member<pCheckItem&>::value),
    p(base_from_member<pCheckItem&>::value)
{
	p.constructor();
}

CheckItem::~CheckItem(void)
{
	p.destructor();
	delete &state;
}

void RadioItem::group(const nall::array<RadioItem&> &list)
{
	for (auto &item : list) {
		item.p.setGroup(item.state.group = list);
	}

	if (list.size()) {
		list[0].setChecked();
	}
}

bool RadioItem::checked(void)
{
	return p.checked();
}

void RadioItem::setChecked(void)
{
	for (auto &item : state.group) {
		item.state.checked = false;
	}

	state.checked = true;
	return p.setChecked();
}

void RadioItem::setText(const nall::string &text)
{
	state.text = text;
	return p.setText(text);
}

RadioItem::RadioItem(void): state(*new State),
    base_from_member<pRadioItem&>(*new pRadioItem(*this)),
    Action(base_from_member<pRadioItem&>::value),
    p(base_from_member<pRadioItem&>::value)
{
	p.constructor();
}

RadioItem::~RadioItem(void)
{
	for (auto &item : state.group) {
		if (&item != this) item.state.group.remove(*this);
	}

	p.destructor();
	delete &state;
}

Layout* Sizable::layout(void)
{
	return state.layout;
}

Window* Sizable::window(void)
{
	if (state.layout) {
		return state.layout->window();
	}

	return state.window;
}

Sizable::Sizable(pSizable &p): state(*new State), Object(p), p(p)
{
	p.constructor();
}

Sizable::~Sizable(void)
{
	if (layout()) {
		layout()->remove(*this);
	}

	p.destructor();
	delete &state;
}

void Layout::append(Sizable &sizable)
{
	sizable.state.layout = this;
	sizable.state.window = 0;

	if (dynamic_cast<Layout*>(&sizable)) {
		Layout &layout = (Layout&)sizable;
		layout.synchronizeLayout();
	}

	if (dynamic_cast<Widget*>(&sizable)) {
		Widget &widget = (Widget&)sizable;
		if (sizable.window()) {
			sizable.window()->append(widget);
		}
	}
}

void Layout::remove(Sizable &sizable)
{
	if (dynamic_cast<Widget*>(&sizable)) {
		Widget &widget = (Widget&)sizable;
		if (sizable.window()) {
			sizable.window()->remove(widget);
		}
	}

	sizable.state.layout = 0;
	sizable.state.window = 0;
}

Layout::Layout(void): state(*new State),
    base_from_member<pLayout&>(*new pLayout(*this)),
    Sizable(base_from_member<pLayout&>::value),
    p(base_from_member<pLayout&>::value) { }

Layout::Layout(pLayout &p): state(*new State),
    base_from_member<pLayout&>(p), Sizable(p), p(p) { }

Layout::~Layout(void)
{
	if (layout()) {
		layout()->remove(*this);
	} else if (window()) {
		window()->remove(*this);
	}

	p.destructor();
	delete &state;
}

bool Widget::enabled(void)
{
	return state.enabled;
}

nall::string Widget::font(void)
{
	return state.font;
}

Geometry Widget::geometry(void)
{
	return state.geometry;
}

Geometry Widget::minimumGeometry(void)
{
	return p.minimumGeometry();
}

void Widget::setEnabled(bool enabled)
{
	state.enabled = enabled;
	return p.setEnabled(enabled);
}

void Widget::setFocused(void)
{
	return p.setFocused();
}

void Widget::setFont(const nall::string &font)
{
	state.font = font;
	return p.setFont(font);
}

void Widget::setGeometry(const Geometry &geometry)
{
	state.geometry = geometry;
	return p.setGeometry(geometry);
}

void Widget::setVisible(bool visible)
{
	state.visible = visible;
	return p.setVisible(visible);
}

bool Widget::visible(void)
{
	return state.visible;
}

Widget::Widget(void): state(*new State),
    base_from_member<pWidget&>(*new pWidget(*this)),
    Sizable(base_from_member<pWidget&>::value),
    p(base_from_member<pWidget&>::value)
{
	state.abstract = true;
	p.constructor();
}

Widget::Widget(pWidget &p): state(*new State),
    base_from_member<pWidget&>(p),
    Sizable(base_from_member<pWidget&>::value),
    p(base_from_member<pWidget&>::value)
{
	p.constructor();
}

Widget::~Widget(void)
{
	p.destructor();
	delete &state;
}

void Button::setImage(const nall::image &image, Orientation orientation)
{
	state.image = image;
	state.orientation = orientation;
	return p.setImage(image, orientation);
}

void Button::setText(const nall::string &text)
{
	state.text = text;
	return p.setText(text);
}

Button::Button(void): state(*new State),
    base_from_member<pButton&>(*new pButton(*this)),
    Widget(base_from_member<pButton&>::value),
    p(base_from_member<pButton&>::value)
{
	p.constructor();
}

Button::~Button(void)
{
	p.destructor();
	delete &state;
}

uint32_t* Canvas::data(void)
{
	return state.data;
}

bool Canvas::setImage(const nall::image &image)
{
	if (image.data == nullptr || image.width == 0 ||
	    image.height == 0) {
		return false;
	}

	state.width = image.width;
	state.height = image.height;
	setSize({ state.width, state.height });

	memcpy(state.data, image.data,
	    state.width * state.height * sizeof(uint32_t));

	return true;
}

void Canvas::setSize(const Size &size)
{
	state.width = size.width;
	state.height = size.height;
	delete[] state.data;

	state.data = new uint32_t[size.width * size.height];
	return p.setSize(size);
}

Size Canvas::size(void)
{
	return { state.width, state.height };
}

void Canvas::update(void)
{
	return p.update();
}

Canvas::Canvas(void): state(*new State),
    base_from_member<pCanvas&>(*new pCanvas(*this)),
    Widget(base_from_member<pCanvas&>::value),
    p(base_from_member<pCanvas&>::value)
{
	state.data = new uint32_t[state.width * state.height];
	p.constructor();
}

Canvas::~Canvas(void)
{
	p.destructor();
	delete[] state.data;
	delete &state;
}

bool CheckBox::checked(void)
{
	return p.checked();
}

void CheckBox::setChecked(bool checked)
{
	state.checked = checked;
	return p.setChecked(checked);
}

void CheckBox::setText(const nall::string &text)
{
	state.text = text;
	return p.setText(text);
}

CheckBox::CheckBox(void): state(*new State),
    base_from_member<pCheckBox&>(*new pCheckBox(*this)),
    Widget(base_from_member<pCheckBox&>::value),
    p(base_from_member<pCheckBox&>::value)
{
	p.constructor();
}

CheckBox::~CheckBox(void)
{
	p.destructor();
	delete &state;
}

void ComboBox::append_(const nall::lstring &list)
{
	for (auto &text : list) {
		state.text.append(text);
		p.append(text);
	}
}

void ComboBox::reset(void)
{
	state.selection = 0;
	state.text.reset();
	return p.reset();
}

unsigned ComboBox::selection(void)
{
	return p.selection();
}

void ComboBox::setSelection(unsigned row)
{
	state.selection = row;
	return p.setSelection(row);
}

ComboBox::ComboBox(void): state(*new State),
    base_from_member<pComboBox&>(*new pComboBox(*this)),
    Widget(base_from_member<pComboBox&>::value),
    p(base_from_member<pComboBox&>::value)
{
	p.constructor();
}

ComboBox::~ComboBox(void)
{
	p.destructor();
	delete &state;
}

void HexEdit::setColumns(unsigned columns)
{
	state.columns = columns;
	return p.setColumns(columns);
}

void HexEdit::setLength(unsigned length)
{
	state.length = length;
	return p.setLength(length);
}

void HexEdit::setOffset(unsigned offset)
{
	state.offset = offset;
	return p.setOffset(offset);
}

void HexEdit::setRows(unsigned rows)
{
	state.rows = rows;
	return p.setRows(rows);
}

void HexEdit::update(void)
{
	return p.update();
}

HexEdit::HexEdit(void): state(*new State),
    base_from_member<pHexEdit&>(*new pHexEdit(*this)),
    Widget(base_from_member<pHexEdit&>::value),
    p(base_from_member<pHexEdit&>::value)
{
	p.constructor();
}

HexEdit::~HexEdit(void)
{
	p.destructor();
	delete &state;
}

unsigned HorizontalScrollBar::length(void)
{
	return state.length;
}

unsigned HorizontalScrollBar::position(void)
{
	return p.position();
}

void HorizontalScrollBar::setLength(unsigned length)
{
	state.length = length;
	return p.setLength(length);
}

void HorizontalScrollBar::setPosition(unsigned position)
{
	state.position = position;
	return p.setPosition(position);
}

HorizontalScrollBar::HorizontalScrollBar(void): state(*new State),
    base_from_member<pHorizontalScrollBar&>(*new pHorizontalScrollBar(*this)),
    Widget(base_from_member<pHorizontalScrollBar&>::value),
    p(base_from_member<pHorizontalScrollBar&>::value)
{
	p.constructor();
}

HorizontalScrollBar::~HorizontalScrollBar(void)
{
	p.destructor();
	delete &state;
}

unsigned HorizontalSlider::length(void)
{
	return state.length;
}

unsigned HorizontalSlider::position(void)
{
	return p.position();
}

void HorizontalSlider::setLength(unsigned length)
{
	state.length = length;
	return p.setLength(length);
}

void HorizontalSlider::setPosition(unsigned position)
{
	state.position = position;
	return p.setPosition(position);
}

HorizontalSlider::HorizontalSlider(void): state(*new State),
    base_from_member<pHorizontalSlider&>(*new pHorizontalSlider(*this)),
    Widget(base_from_member<pHorizontalSlider&>::value),
    p(base_from_member<pHorizontalSlider&>::value)
{
	p.constructor();
}

HorizontalSlider::~HorizontalSlider(void)
{
	p.destructor();
	delete &state;
}

void Label::setText(const nall::string &text)
{
	state.text = text;
	return p.setText(text);
}

Label::Label(void): state(*new State),
    base_from_member<pLabel&>(*new pLabel(*this)),
    Widget(base_from_member<pLabel&>::value),
    p(base_from_member<pLabel&>::value)
{
	p.constructor();
}

Label::~Label(void)
{
	p.destructor();
	delete &state;
}

void LineEdit::setEditable(bool editable)
{
	state.editable = editable;
	return p.setEditable(editable);
}

void LineEdit::setText(const nall::string &text)
{
	state.text = text;
	return p.setText(text);
}

nall::string LineEdit::text(void)
{
	return p.text();
}

LineEdit::LineEdit(void): state(*new State),
    base_from_member<pLineEdit&>(*new pLineEdit(*this)),
    Widget(base_from_member<pLineEdit&>::value),
    p(base_from_member<pLineEdit&>::value)
{
	p.constructor();
}

LineEdit::~LineEdit(void)
{
	p.destructor();
	delete &state;
}

void ListView::append_(const nall::lstring &text)
{
	state.checked.append(false);
	state.text.append(text);
	return p.append(text);
}

void ListView::autoSizeColumns(void)
{
	return p.autoSizeColumns();
}

bool ListView::checked(unsigned row)
{
	return p.checked(row);
}

void ListView::modify_(unsigned row, const nall::lstring &text)
{
	state.text[row] = text;
	return p.modify(row, text);
}

void ListView::reset(void)
{
	state.checked.reset();
	state.text.reset();
	return p.reset();
}

bool ListView::selected(void)
{
	return p.selected();
}

unsigned ListView::selection(void)
{
	return p.selection();
}

void ListView::setCheckable(bool checkable)
{
	state.checkable = checkable;
	return p.setCheckable(checkable);
}

void ListView::setChecked(unsigned row, bool checked)
{
	state.checked[row] = checked;
	return p.setChecked(row, checked);
}

void ListView::setHeaderText_(const nall::lstring &text)
{
	state.headerText = text;
	return p.setHeaderText(text);
}

void ListView::setHeaderVisible(bool visible)
{
	state.headerVisible = visible;
	return p.setHeaderVisible(visible);
}

void ListView::setSelected(bool selected)
{
	state.selected = selected;
	return p.setSelected(selected);
}

void ListView::setSelection(unsigned row)
{
	state.selected = true;
	state.selection = row;
	return p.setSelection(row);
}

ListView::ListView(void): state(*new State),
    base_from_member<pListView&>(*new pListView(*this)),
    Widget(base_from_member<pListView&>::value),
    p(base_from_member<pListView&>::value)
{
	p.constructor();
}

ListView::~ListView(void)
{
	p.destructor();
	delete &state;
}

void ProgressBar::setPosition(unsigned position)
{
	state.position = position;
	return p.setPosition(position);
}

ProgressBar::ProgressBar(void): state(*new State),
    base_from_member<pProgressBar&>(*new pProgressBar(*this)),
    Widget(base_from_member<pProgressBar&>::value),
    p(base_from_member<pProgressBar&>::value)
{
	p.constructor();
}

ProgressBar::~ProgressBar(void)
{
	p.destructor();
	delete &state;
}

void RadioBox::group(const nall::array<RadioBox&> &list)
{
	for (auto &item : list) {
		item.p.setGroup(item.state.group = list);
	}

	if (list.size()) {
		list[0].setChecked();
	}
}

bool RadioBox::checked(void)
{
	return p.checked();
}

void RadioBox::setChecked(void)
{
	for (auto &item : state.group) {
		item.state.checked = false;
	}

	state.checked = true;
	return p.setChecked();
}

void RadioBox::setText(const nall::string &text)
{
	state.text = text;
	return p.setText(text);
}

RadioBox::RadioBox(void): state(*new State),
    base_from_member<pRadioBox&>(*new pRadioBox(*this)),
    Widget(base_from_member<pRadioBox&>::value),
    p(base_from_member<pRadioBox&>::value)
{
	p.constructor();
}

RadioBox::~RadioBox(void)
{
	for (auto &item : state.group) {
		if (&item != this) {
			item.state.group.remove(*this);
		}
	}

	p.destructor();
	delete &state;
}

void TextEdit::setCursorPosition(unsigned position)
{
	state.cursorPosition = position;
	return p.setCursorPosition(position);
}

void TextEdit::setEditable(bool editable)
{
	state.editable = editable;
	return p.setEditable(editable);
}

void TextEdit::setText(const nall::string &text)
{
	state.text = text;
	return p.setText(text);
}

void TextEdit::setWordWrap(bool wordWrap)
{
	state.wordWrap = wordWrap;
	return p.setWordWrap(wordWrap);
}

nall::string TextEdit::text(void)
{
	return p.text();
}

TextEdit::TextEdit(void): state(*new State),
    base_from_member<pTextEdit&>(*new pTextEdit(*this)),
    Widget(base_from_member<pTextEdit&>::value),
    p(base_from_member<pTextEdit&>::value)
{
	p.constructor();
}

TextEdit::~TextEdit(void)
{
	p.destructor();
	delete &state;
}

unsigned VerticalScrollBar::length(void)
{
	return state.length;
}

unsigned VerticalScrollBar::position(void)
{
	return p.position();
}

void VerticalScrollBar::setLength(unsigned length)
{
	state.length = length;
	return p.setLength(length);
}

void VerticalScrollBar::setPosition(unsigned position)
{
	state.position = position;
	return p.setPosition(position);
}

VerticalScrollBar::VerticalScrollBar(void): state(*new State),
    base_from_member<pVerticalScrollBar&>(*new pVerticalScrollBar(*this)),
    Widget(base_from_member<pVerticalScrollBar&>::value),
    p(base_from_member<pVerticalScrollBar&>::value)
{
	p.constructor();
}

VerticalScrollBar::~VerticalScrollBar(void)
{
	p.destructor();
	delete &state;
}

unsigned VerticalSlider::length(void)
{
	return state.length;
}

unsigned VerticalSlider::position(void)
{
	return p.position();
}

void VerticalSlider::setLength(unsigned length)
{
	state.length = length;
	return p.setLength(length);
}

void VerticalSlider::setPosition(unsigned position)
{
	state.position = position;
	return p.setPosition(position);
}

VerticalSlider::VerticalSlider(void): state(*new State),
    base_from_member<pVerticalSlider&>(*new pVerticalSlider(*this)),
    Widget(base_from_member<pVerticalSlider&>::value),
    p(base_from_member<pVerticalSlider&>::value)
{
	p.constructor();
}

VerticalSlider::~VerticalSlider(void)
{
	p.destructor();
	delete &state;
}

uintptr_t Viewport::handle(void)
{
	return p.handle();
}

Viewport::Viewport(void):
    base_from_member<pViewport&>(*new pViewport(*this)),
    Widget(base_from_member<pViewport&>::value),
    p(base_from_member<pViewport&>::value)
{
	p.constructor();
}

Viewport::~Viewport(void)
{
	p.destructor();
}

}

#endif
