#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Size phoenix::pDesktop::size(void)
{
	QRect rect = QApplication::desktop()->screenGeometry();
	return { rect.width(), rect.height() };
}

phoenix::Geometry phoenix::pDesktop::workspace(void)
{
	QRect rect = QApplication::desktop()->availableGeometry();
	return { rect.x(), rect.y(), rect.width(), rect.height() };
}
