#include <nall/string.hpp>
#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

/*
* XXX: This is the settings object that pOS initializes and phoenix::Keyboard
* references.  This needs to be stored somewhere else, and not a static object.
*/
//phoenix::Settings *phoenix::pOS::settings = nullptr;

void phoenix::Settings::load(void)
{
	/* XXX: Hard-coded config path.  Bad IMO. */
	nall::string path = { nall::userpath(), ".config/phoenix/qt.cfg" };
	nall::configuration::load(path);
}

void phoenix::Settings::save(void)
{
	/* XXX: Hard-coded config path. */
	nall::string path = { nall::userpath(), ".config/" };
	mkdir(path, 0755);
	path.append("phoenix/");
	mkdir(path, 0755);
	path.append("qt.cfg");
	nall::configuration::save(path);
}

phoenix::Settings::Settings(void)
{
	/* XXX: Hard coded configuration values? */
	append(frameGeometryX = 4, "frameGeometryX");
	append(frameGeometryY = 24, "frameGeometryY");
	append(frameGeometryWidth = 8, "frameGeometryWidth");
	append(frameGeometryHeight = 28, "frameGeometryHeight");
	append(menuGeometryHeight = 20, "menuGeometryHeight");
	append(statusGeometryHeight = 20, "statusGeometryHeight");

	/* Added because GTK needs it. */
	append(windowBackgroundColor = 0xedeceb, "windowBackgroundColor");
}
