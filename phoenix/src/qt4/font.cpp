#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Geometry phoenix::pFont::geometry(const nall::string &description,
    const nall::string &text)
{
	return pFont::geometry(pFont::create(description), text);
}

QFont phoenix::pFont::create(const nall::string &description)
{
	nall::lstring part;
	part.split(",", description);
	for (auto &item : part) {
		item.trim(" ");
	}

	nall::string family = "Sans";
	unsigned size = 8u;
	bool bold = false;
	bool italic = false;

	if (part[0] != "") {
		family = part[0];
	}

	if (part.size() >= 2) {
		size = decimal(part[1]);
	}

	if (part.size() >= 3) {
		bold = part[2].position("Bold");
	}

	if (part.size() >= 3) {
		italic = part[2].position("Italic");
	}

	QFont qtFont;
	qtFont.setFamily(QString(family));
	qtFont.setPointSize(size);

	if (bold) {
		qtFont.setBold(true);
	}

	if (italic) {
		qtFont.setItalic(true);
	}

	return qtFont;
}

phoenix::Geometry phoenix::pFont::geometry(const QFont &qtFont,
    const nall::string &text)
{
	QFontMetrics metrics(qtFont);

	nall::lstring lines;
	lines.split("\n", text);

	unsigned maxWidth = 0;
	for (auto &line : lines) {
		maxWidth = nall::max(maxWidth, metrics.width(QString(line)));
	}

	return { 0, 0, maxWidth, metrics.height() * lines.size() };
}
