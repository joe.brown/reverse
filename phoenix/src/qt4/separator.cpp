#include "phoenix/platform/qt4.h"

void phoenix::pSeparator::constructor(void)
{
	qtAction = new QAction(0);
	qtAction->setSeparator(true);
}

void phoenix::pSeparator::destructor(void)
{
	if (action.state.menu) {
		action.state.menu->remove(separator);
	}

	delete qtAction;
}
