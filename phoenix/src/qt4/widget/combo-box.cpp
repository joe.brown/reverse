#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

void phoenix::pComboBox::append(const nall::string &text)
{
	locked = true;
	qtComboBox->addItem(QString::fromUtf8(text));
	locked = false;
}

phoenix::Geometry phoenix::pComboBox::minimumGeometry(void)
{
	unsigned maximumWidth = 0;
	for (auto &text : comboBox.state.text) {
		maximumWidth = nall::max(maximumWidth,
		    pFont::geometry(qtWidget->font(), text).width);
	}

	Geometry geometry = pFont::geometry(qtWidget->font(), " ");
	return { 0, 0, maximumWidth + 32, geometry.height + 12 };
}

void phoenix::pComboBox::reset(void)
{
	locked = true;
	while (qtComboBox->count()) {
		qtComboBox->removeItem(0);
	}

	locked = false;
}

unsigned phoenix::pComboBox::selection(void)
{
	signed index = qtComboBox->currentIndex();
	return index >= 0 ? index : 0;
}

void phoenix::pComboBox::setSelection(unsigned row)
{
	locked = true;
	qtComboBox->setCurrentIndex(row);
	locked = false;
}

void phoenix::pComboBox::constructor(void)
{
	qtWidget = qtComboBox = new QComboBox;
	connect(qtComboBox, SIGNAL(currentIndexChanged(int)),
	    SLOT(onChange()));

	pWidget::synchronizeState();
	unsigned selection = comboBox.state.selection;
	locked = true;

	for (auto &text : comboBox.state.text) {
		append(text);
	}

	locked = false;
	setSelection(selection);
}

void phoenix::pComboBox::destructor(void)
{
	delete qtComboBox;
	qtWidget = qtComboBox = 0;
}

void phoenix::pComboBox::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pComboBox::onChange(void)
{
	comboBox.state.selection = selection();
	if (locked == false && comboBox.onChange) {
		comboBox.onChange();
	}
}
