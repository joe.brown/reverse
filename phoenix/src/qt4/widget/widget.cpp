#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Geometry phoenix::pWidget::minimumGeometry(void)
{
	return { 0, 0, 0, 0 };
}

void phoenix::pWidget::setEnabled(bool enabled)
{
	if (widget.state.abstract) {
		enabled = false;
	}

	if (sizable.state.layout &&
	    sizable.state.layout->enabled() == false) {
		enabled = false;
	}

	qtWidget->setEnabled(enabled);
}

void phoenix::pWidget::setFocused(void)
{
	qtWidget->setFocus(Qt::OtherFocusReason);
}

void phoenix::pWidget::setFont(const nall::string &font)
{
	qtWidget->setFont(pFont::create(font));
}

void phoenix::pWidget::setGeometry(const phoenix::Geometry &geometry)
{
	qtWidget->setGeometry(geometry.x, geometry.y,
	    geometry.width, geometry.height);
}

void phoenix::pWidget::setVisible(bool visible)
{
	if (widget.state.abstract) {
		visible = false;
	}

	if (sizable.state.layout == 0) {
		visible = false;
	}

	if (sizable.state.layout &&
	    sizable.state.layout->visible() == false) {
		visible = false;
	}

	qtWidget->setVisible(visible);
}

void phoenix::pWidget::constructor(void)
{
	if (widget.state.abstract) {
		qtWidget = new QWidget;
	}
}

/*
* pWidget::constructor() called before p{Derived}::constructor(); ergo
* qtWidget is not yet valid. pWidget::synchronizeState() is called to
* finish construction of p{Derived}::constructor()
*/
void phoenix::pWidget::synchronizeState(void)
{
	setEnabled(widget.state.enabled);
	setFont(widget.state.font);
	//setVisible(widget.state.visible);
}

void phoenix::pWidget::destructor(void)
{
	if (widget.state.abstract) {
		delete qtWidget;
		qtWidget = 0;
	}
}

void phoenix::pWidget::orphan(void)
{
	destructor();
	constructor();
}
