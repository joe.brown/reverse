#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

bool phoenix::pCheckBox::checked(void)
{
	return qtCheckBox->isChecked();
}

phoenix::Geometry phoenix::pCheckBox::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(qtWidget->font(),
	    checkBox.state.text);
	return { 0, 0, geometry.width + 26, geometry.height + 6 };
}

void phoenix::pCheckBox::setChecked(bool checked)
{
	locked = true;
	qtCheckBox->setChecked(checked);
	locked = false;
}

void phoenix::pCheckBox::setText(const nall::string &text)
{
	qtCheckBox->setText(QString::fromUtf8(text));
}

void phoenix::pCheckBox::constructor(void)
{
	qtWidget = qtCheckBox = new QCheckBox;
	connect(qtCheckBox, SIGNAL(stateChanged(int)), SLOT(onToggle()));

	pWidget::synchronizeState();
	setChecked(checkBox.state.checked);
	setText(checkBox.state.text);
}

void phoenix::pCheckBox::destructor(void)
{
	delete qtCheckBox;
	qtWidget = qtCheckBox = 0;
}

void phoenix::pCheckBox::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pCheckBox::onToggle(void)
{
	checkBox.state.checked = checked();
	if (locked == false && checkBox.onToggle) {
		checkBox.onToggle();
	}
}
