#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Geometry phoenix::pVerticalScrollBar::minimumGeometry(void)
{
	return { 0, 0, 15, 0 };
}

unsigned phoenix::pVerticalScrollBar::position(void)
{
	return qtScrollBar->value();
}

void phoenix::pVerticalScrollBar::setLength(unsigned length)
{
	length += length == 0;
	qtScrollBar->setRange(0, length - 1);
	qtScrollBar->setPageStep(length >> 3);
}

void phoenix::pVerticalScrollBar::setPosition(unsigned position)
{
	qtScrollBar->setValue(position);
}

void phoenix::pVerticalScrollBar::constructor(void)
{
	qtWidget = qtScrollBar = new QScrollBar(Qt::Vertical);
	qtScrollBar->setRange(0, 100);
	qtScrollBar->setPageStep(101 >> 3);
	connect(qtScrollBar, SIGNAL(valueChanged(int)), SLOT(onChange()));

	pWidget::synchronizeState();
	setLength(verticalScrollBar.state.length);
	setPosition(verticalScrollBar.state.position);
}

void phoenix::pVerticalScrollBar::destructor(void)
{
	delete qtScrollBar;
	qtWidget = qtScrollBar = 0;
}

void phoenix::pVerticalScrollBar::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pVerticalScrollBar::onChange(void)
{
	verticalScrollBar.state.position = position();
	if (verticalScrollBar.onChange) {
		verticalScrollBar.onChange();
	}
}
