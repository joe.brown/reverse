#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Geometry phoenix::pLineEdit::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(qtWidget->font(),
	    lineEdit.state.text);
	return { 0, 0, geometry.width + 12, geometry.height + 12 };
}

void phoenix::pLineEdit::setEditable(bool editable)
{
	qtLineEdit->setReadOnly(!editable);
}

void phoenix::pLineEdit::setText(const nall::string &text)
{
	qtLineEdit->setText(QString::fromUtf8(text));
}

nall::string phoenix::pLineEdit::text(void)
{
	return qtLineEdit->text().toUtf8().constData();
}

void phoenix::pLineEdit::constructor(void)
{
	qtWidget = qtLineEdit = new QLineEdit;
	connect(qtLineEdit, SIGNAL(returnPressed()), SLOT(onActivate()));
	connect(qtLineEdit, SIGNAL(textEdited(const QString&)),
	    SLOT(onChange()));

	pWidget::synchronizeState();
	setEditable(lineEdit.state.editable);
	setText(lineEdit.state.text);
}

void phoenix::pLineEdit::destructor(void)
{
	delete qtLineEdit;
	qtWidget = qtLineEdit = 0;
}

void phoenix::pLineEdit::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pLineEdit::onActivate(void)
{
	if (lineEdit.onActivate) {
		lineEdit.onActivate();
	}
}

void phoenix::pLineEdit::onChange(void)
{
	lineEdit.state.text = text();
	if (lineEdit.onChange) {
		lineEdit.onChange();
	}
}
