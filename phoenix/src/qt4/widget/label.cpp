#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Geometry phoenix::pLabel::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(qtWidget->font(),
	    label.state.text);
	return { 0, 0, geometry.width, geometry.height };
}

void phoenix::pLabel::setText(const nall::string &text)
{
	qtLabel->setText(QString::fromUtf8(text));
}

void phoenix::pLabel::constructor(void)
{
	qtWidget = qtLabel = new QLabel;

	pWidget::synchronizeState();
	setText(label.state.text);
}

void phoenix::pLabel::destructor(void)
{
	delete qtLabel;
	qtWidget = qtLabel = 0;
}

void phoenix::pLabel::orphan(void)
{
	destructor();
	constructor();
}
