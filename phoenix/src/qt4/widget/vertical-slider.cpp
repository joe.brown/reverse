#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Geometry phoenix::pVerticalSlider::minimumGeometry(void)
{
	return { 0, 0, 20, 0 };
}

unsigned phoenix::pVerticalSlider::position(void)
{
	return qtSlider->value();
}

void phoenix::pVerticalSlider::setLength(unsigned length)
{
	length += length == 0;
	qtSlider->setRange(0, length - 1);
	qtSlider->setPageStep(length >> 3);
}

void phoenix::pVerticalSlider::setPosition(unsigned position)
{
	qtSlider->setValue(position);
}

void phoenix::pVerticalSlider::constructor(void)
{
	qtWidget = qtSlider = new QSlider(Qt::Vertical);
	qtSlider->setRange(0, 100);
	qtSlider->setPageStep(101 >> 3);
	connect(qtSlider, SIGNAL(valueChanged(int)), SLOT(onChange()));

	pWidget::synchronizeState();
	setLength(verticalSlider.state.length);
	setPosition(verticalSlider.state.position);
}

void phoenix::pVerticalSlider::destructor(void)
{
	delete qtSlider;
	qtWidget = qtSlider = 0;
}

void phoenix::pVerticalSlider::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pVerticalSlider::onChange(void)
{
	verticalSlider.state.position = position();
	if (verticalSlider.onChange) {
		verticalSlider.onChange();
	}
}
