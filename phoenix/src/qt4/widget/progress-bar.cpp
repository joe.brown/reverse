#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Geometry phoenix::pProgressBar::minimumGeometry(void)
{
	return { 0, 0, 0, 25 };
}

void phoenix::pProgressBar::setPosition(unsigned position)
{
	qtProgressBar->setValue(position);
}

void phoenix::pProgressBar::constructor(void)
{
	qtWidget = qtProgressBar = new QProgressBar;
	qtProgressBar->setRange(0, 100);
	qtProgressBar->setTextVisible(false);

	pWidget::synchronizeState();
	setPosition(progressBar.state.position);
}

void phoenix::pProgressBar::destructor(void)
{
	delete qtProgressBar;
	qtWidget = qtProgressBar = 0;
}

void phoenix::pProgressBar::orphan(void)
{
	destructor();
	constructor();
}
