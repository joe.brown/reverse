#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

void phoenix::pCanvas::setSize(const phoenix::Size &size)
{
	delete qtImage;
	qtImage = new QImage(size.width, size.height,
	    QImage::Format_ARGB32);
}

void phoenix::pCanvas::update(void)
{
	uint32_t *dp = (uint32_t*)qtImage->bits(),
	    *sp = (uint32_t*)canvas.state.data;
	unsigned size = canvas.state.width * canvas.state.height;

	for (unsigned n = 0; n < size; n++) {
		*dp++ = 0xff000000 | *sp++;
	}

	qtCanvas->update();
}

void phoenix::pCanvas::constructor(void)
{
	qtWidget = qtCanvas = new QtCanvas(*this);
	qtCanvas->setMouseTracking(true);
	qtImage = new QImage(canvas.state.width, canvas.state.height,
	    QImage::Format_ARGB32);
	memcpy(qtImage->bits(), canvas.state.data,
	    canvas.state.width * canvas.state.height * sizeof(uint32_t));

	pWidget::synchronizeState();
	update();
}

void phoenix::pCanvas::destructor(void)
{
	delete qtCanvas;
	delete qtImage;
	qtWidget = qtCanvas = 0;
	qtImage = 0;
}

void phoenix::pCanvas::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pCanvas::QtCanvas::leaveEvent(QEvent *event)
{
	if (self.canvas.onMouseLeave) {
		self.canvas.onMouseLeave();
	}
}

void phoenix::pCanvas::QtCanvas::mouseMoveEvent(QMouseEvent *event)
{
	if (self.canvas.onMouseMove) {
		self.canvas.onMouseMove({
		    event->pos().x(), event->pos().y() });
	}
}

void phoenix::pCanvas::QtCanvas::mousePressEvent(QMouseEvent *event)
{
	if (self.canvas.onMousePress == false) {
		return;
	}

	switch (event->button()) {
	case Qt::LeftButton:
		self.canvas.onMousePress(Mouse::Button::Left);
		break;
	case Qt::MidButton:
		self.canvas.onMousePress(Mouse::Button::Middle);
		break;
	case Qt::RightButton:
		self.canvas.onMousePress(Mouse::Button::Right);
		break;
	}
}

void phoenix::pCanvas::QtCanvas::mouseReleaseEvent(QMouseEvent *event)
{
	if (self.canvas.onMouseRelease == false) {
		return;
	}

	switch (event->button()) {
	case Qt::LeftButton:
		self.canvas.onMouseRelease(Mouse::Button::Left);
		break;
	case Qt::MidButton:
		self.canvas.onMouseRelease(Mouse::Button::Middle);
		break;
	case Qt::RightButton:
		self.canvas.onMouseRelease(Mouse::Button::Right);
		break;
	}
}

void phoenix::pCanvas::QtCanvas::paintEvent(QPaintEvent *event)
{
	QPainter painter(self.qtCanvas);
	painter.drawImage(0, 0, *self.qtImage);

	/* XXX: Seems interesting.  Why was it removed? */
	//this will scale the source image to fit the
	//target widget size (nearest-neighbor):
	//painter.drawImage(
	//	QRect(0, 0, geometry().width(), geometry().height()),
	//	*self.qtImage,
	//	QRect(0, 0, self.canvas.state.width, self.canvas.state.height)
	//);
}

phoenix::pCanvas::QtCanvas::QtCanvas(pCanvas &self) : self(self) { }
