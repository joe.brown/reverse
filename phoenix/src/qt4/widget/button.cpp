#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Geometry phoenix::pButton::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(qtWidget->font(),
	    button.state.text);

	if (button.state.orientation == Orientation::Horizontal) {
		geometry.width += button.state.image.width;
		geometry.height = nall::max(button.state.image.height,
		    geometry.height);
	}

	if (button.state.orientation == Orientation::Vertical) {
		geometry.width = nall::max(button.state.image.width,
		    geometry.width);
		geometry.height += button.state.image.height;
	}

	/* XXX: Why 20 and 12? */
	return { 0, 0, geometry.width + 20, geometry.height + 12 };
}

void phoenix::pButton::setImage(const nall::image &image,
    phoenix::Orientation orientation)
{
	nall::image qtBuffer = image;
	qtBuffer.transform(0, 32, 255u << 24, 255u << 16,
	    255u << 8, 255u << 0);

	QImage qtImage(qtBuffer.data, qtBuffer.width, qtBuffer.height,
	    QImage::Format_ARGB32);
	QIcon qtIcon(QPixmap::fromImage(qtImage));
	qtButton->setIconSize(QSize(qtBuffer.width, qtBuffer.height));
	qtButton->setIcon(qtIcon);
	qtButton->setStyleSheet("text-align: top;");

	switch (orientation) {
	case phoenix::Orientation::Horizontal:
		qtButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
		break;
	case phoenix::Orientation::Vertical:
		qtButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
		break;
	}
}

void phoenix::pButton::setText(const nall::string &text)
{
	qtButton->setText(QString::fromUtf8(text));
}

void phoenix::pButton::constructor(void)
{
	qtWidget = qtButton = new QToolButton;
	qtButton->setToolButtonStyle(Qt::ToolButtonTextOnly);
	connect(qtButton, SIGNAL(released()), SLOT(onActivate()));

	pWidget::synchronizeState();
	setText(button.state.text);
}

void phoenix::pButton::destructor(void)
{
	delete qtButton;
	qtWidget = qtButton = 0;
}

void phoenix::pButton::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pButton::onActivate()
{
	if (button.onActivate) {
		button.onActivate();
	}
}
