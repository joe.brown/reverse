#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Geometry phoenix::pHorizontalScrollBar::minimumGeometry(void)
{
	return { 0, 0, 0, 15 };
}

unsigned phoenix::pHorizontalScrollBar::position(void)
{
	return qtScrollBar->value();
}

void phoenix::pHorizontalScrollBar::setLength(unsigned length)
{
	length += length == 0;
	qtScrollBar->setRange(0, length - 1);
	qtScrollBar->setPageStep(length >> 3);
}

void phoenix::pHorizontalScrollBar::setPosition(unsigned position)
{
	qtScrollBar->setValue(position);
}

void phoenix::pHorizontalScrollBar::constructor(void)
{
	qtWidget = qtScrollBar = new QScrollBar(Qt::Horizontal);
	qtScrollBar->setRange(0, 100);
	qtScrollBar->setPageStep(101 >> 3);
	connect(qtScrollBar, SIGNAL(valueChanged(int)), SLOT(onChange()));

	pWidget::synchronizeState();
	setLength(horizontalScrollBar.state.length);
	setPosition(horizontalScrollBar.state.position);
}

void phoenix::pHorizontalScrollBar::destructor(void)
{
	delete qtScrollBar;
	qtWidget = qtScrollBar = 0;
}

void phoenix::pHorizontalScrollBar::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pHorizontalScrollBar::onChange(void)
{
	horizontalScrollBar.state.position = position();
	if (horizontalScrollBar.onChange) {
		horizontalScrollBar.onChange();
	}
}
