#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

uintptr_t phoenix::pViewport::handle(void)
{
	return (uintptr_t)qtViewport->winId();
}

void phoenix::pViewport::constructor(void)
{
	qtWidget = qtViewport = new QtViewport(*this);
	qtViewport->setMouseTracking(true);
	qtViewport->setAttribute(Qt::WA_PaintOnScreen, true);
	qtViewport->setStyleSheet("background: #000000");

	pWidget::synchronizeState();
}

void phoenix::pViewport::destructor(void)
{
	delete qtViewport;
	qtWidget = qtViewport = nullptr;
}

void phoenix::pViewport::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pViewport::QtViewport::leaveEvent(QEvent *event)
{
	if (self.viewport.onMouseLeave) {
		self.viewport.onMouseLeave();
	}
}

void phoenix::pViewport::QtViewport::mouseMoveEvent(QMouseEvent *event)
{
	if (self.viewport.onMouseMove) {
		self.viewport.onMouseMove({
		    event->pos().x(), event->pos().y() });
	}
}

void phoenix::pViewport::QtViewport::mousePressEvent(QMouseEvent *event)
{
	if (self.viewport.onMousePress == false) {
		return;
	}

	switch (event->button()) {
	case Qt::LeftButton:
		self.viewport.onMousePress(Mouse::Button::Left);
		break;
	case Qt::MidButton:
		self.viewport.onMousePress(Mouse::Button::Middle);
		break;
	case Qt::RightButton:
		self.viewport.onMousePress(Mouse::Button::Right);
		break;
	}
}

void phoenix::pViewport::QtViewport::mouseReleaseEvent(QMouseEvent *event)
{
	if (self.viewport.onMouseRelease == false) {
		return;
	}

	switch (event->button()) {
	case Qt::LeftButton:
		self.viewport.onMouseRelease(Mouse::Button::Left);
		break;
	case Qt::MidButton:
		self.viewport.onMouseRelease(Mouse::Button::Middle);
		break;
	case Qt::RightButton:
		self.viewport.onMouseRelease(Mouse::Button::Right);
		break;
	}
}

phoenix::pViewport::QtViewport::QtViewport(pViewport &self)
    : self(self) { }
