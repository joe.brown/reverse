#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

void phoenix::pTextEdit::setCursorPosition(unsigned position)
{
	QTextCursor cursor = qtTextEdit->textCursor();
	unsigned lastCharacter =
	    strlen(qtTextEdit->toPlainText().toUtf8().constData());
	cursor.setPosition(nall::min(position, lastCharacter));
	qtTextEdit->setTextCursor(cursor);
}

void phoenix::pTextEdit::setEditable(bool editable)
{
	qtTextEdit->setReadOnly(!editable);
}

void phoenix::pTextEdit::setText(const nall::string &text)
{
	qtTextEdit->setPlainText(QString::fromUtf8(text));
}

void phoenix::pTextEdit::setWordWrap(bool wordWrap)
{
	qtTextEdit->setWordWrapMode(wordWrap ?
	    QTextOption::WordWrap : QTextOption::NoWrap);
	qtTextEdit->setHorizontalScrollBarPolicy(wordWrap ?
	    Qt::ScrollBarAlwaysOff : Qt::ScrollBarAlwaysOn);

	/* XXX: We always want our vertical scrollbar on? */
	qtTextEdit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
}

nall::string phoenix::pTextEdit::text(void)
{
	return qtTextEdit->toPlainText().toUtf8().constData();
}

void phoenix::pTextEdit::constructor(void)
{
	qtWidget = qtTextEdit = new QTextEdit;
	connect(qtTextEdit, SIGNAL(textChanged()), SLOT(onChange()));

	pWidget::synchronizeState();
	setEditable(textEdit.state.editable);
	setText(textEdit.state.text);
	setWordWrap(textEdit.state.wordWrap);
}

void phoenix::pTextEdit::destructor(void)
{
	if (sizable.state.layout) {
		sizable.state.layout->remove(textEdit);
	}

	delete qtTextEdit;
	qtWidget = qtTextEdit = 0;
}

void phoenix::pTextEdit::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pTextEdit::onChange(void)
{
	textEdit.state.text = text();
	if (textEdit.onChange) {
		textEdit.onChange();
	}
}
