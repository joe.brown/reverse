#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

bool phoenix::pRadioBox::checked(void)
{
	return qtRadioBox->isChecked();
}

phoenix::Geometry phoenix::pRadioBox::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(qtWidget->font(),
	    radioBox.state.text);
	return { 0, 0, geometry.width + 26, geometry.height + 6 };
}

void phoenix::pRadioBox::setChecked(void)
{
	locked = true;

	for (auto &item : radioBox.state.group) {
		bool checkState = item.p.qtRadioBox == qtRadioBox;
		item.state.checked = checkState;
		item.p.qtRadioBox->setChecked(checkState);
	}

	locked = false;
}

void phoenix::pRadioBox::setGroup(const nall::array<RadioBox&> &group)
{
	locked = true;
	if (qtGroup) {
		delete qtGroup;
		qtGroup = 0;
	}

	if (group.size() > 0 && qtRadioBox == group[0].p.qtRadioBox) {
		qtGroup = new QButtonGroup;
		for (auto &item : group) {
			qtGroup->addButton(item.p.qtRadioBox);
		}
		setChecked();
	}

	locked = false;
}

void phoenix::pRadioBox::setText(const nall::string &text)
{
	qtRadioBox->setText(QString::fromUtf8(text));
}

void phoenix::pRadioBox::constructor(void)
{
	qtWidget = qtRadioBox = new QRadioButton;
	qtGroup = new QButtonGroup;
	qtGroup->addButton(qtRadioBox);
	qtRadioBox->setChecked(true);
	connect(qtRadioBox, SIGNAL(toggled(bool)), SLOT(onActivate()));

	pWidget::synchronizeState();
	setGroup(radioBox.state.group);
	setText(radioBox.state.text);
}

void phoenix::pRadioBox::destructor(void)
{
	delete qtGroup;
	delete qtRadioBox;
	qtWidget = qtRadioBox = 0;
	qtGroup = 0;
}

void phoenix::pRadioBox::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pRadioBox::onActivate(void)
{
	if (locked == false && checked() && radioBox.onActivate) {
		radioBox.onActivate();
	}
}
