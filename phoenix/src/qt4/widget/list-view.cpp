#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

void phoenix::pListView::append(const nall::lstring &text)
{
	locked = true;

	auto items = qtListView->findItems("", Qt::MatchContains);
	QTreeWidgetItem *item = new QTreeWidgetItem(qtListView);
	item->setData(0, Qt::UserRole, (unsigned)items.size());

	if (listView.state.checkable) {
		item->setCheckState(0, Qt::Unchecked);
	}

	for (unsigned n = 0; n < text.size(); n++) {
		item->setText(n, QString::fromUtf8(text[n]));
	}

	locked = false;
}

void phoenix::pListView::autoSizeColumns(void)
{
	for (unsigned n = 0; n < listView.state.headerText.size(); n++) {
		qtListView->resizeColumnToContents(n);
	}
}

bool phoenix::pListView::checked(unsigned row)
{
	QTreeWidgetItem *item = qtListView->topLevelItem(row);
	return item ? item->checkState(0) == Qt::Checked : false;
}

void phoenix::pListView::modify(unsigned row, const nall::lstring &text)
{
	locked = true;
	QTreeWidgetItem *item = qtListView->topLevelItem(row);
	if (!item) {
		return;
	}

	for (unsigned n = 0; n < text.size(); n++) {
		item->setText(n, QString::fromUtf8(text[n]));
	}

	locked = false;
}

void phoenix::pListView::reset(void)
{
	qtListView->clear();
}

bool phoenix::pListView::selected(void)
{
	QTreeWidgetItem *item = qtListView->currentItem();
	return (item && item->isSelected() == true);
}

unsigned phoenix::pListView::selection(void)
{
	QTreeWidgetItem *item = qtListView->currentItem();
	if (item == 0) {
		return 0;
	}

	return item->data(0, Qt::UserRole).toUInt();
}

void phoenix::pListView::setCheckable(bool checkable)
{
	if (checkable) {
		auto items = qtListView->findItems("", Qt::MatchContains);
		for (unsigned n = 0; n < items.size(); n++) {
			items[n]->setCheckState(0, Qt::Unchecked);
		}
	}
}

void phoenix::pListView::setChecked(unsigned row, bool checked)
{
	locked = true;

	QTreeWidgetItem *item = qtListView->topLevelItem(row);
	if (item) {
		item->setCheckState(0, checked ?
		    Qt::Checked : Qt::Unchecked);
	}

	locked = false;
}

void phoenix::pListView::setHeaderText(const nall::lstring &text)
{
	QStringList labels;
	for (auto &column : text) {
		labels << QString::fromUtf8(column);
	}

	qtListView->setColumnCount(text.size());
	qtListView->setAlternatingRowColors(text.size() >= 2);
	qtListView->setHeaderLabels(labels);
	autoSizeColumns();
}

void phoenix::pListView::setHeaderVisible(bool visible)
{
	qtListView->setHeaderHidden(!visible);
	autoSizeColumns();
}

void phoenix::pListView::setSelected(bool selected)
{
	QTreeWidgetItem *item = qtListView->currentItem();
	if (item) {
		item->setSelected(selected);
	}
}

void phoenix::pListView::setSelection(unsigned row)
{
	locked = true;

	QTreeWidgetItem *item = qtListView->currentItem();
	if (item) {
		item->setSelected(false);
	}

	qtListView->setCurrentItem(0);
	auto items = qtListView->findItems("", Qt::MatchContains);
	for (unsigned n = 0; n < items.size(); n++) {
		if (items[n]->data(0, Qt::UserRole).toUInt() == row) {
			qtListView->setCurrentItem(items[n]);
			break;
		}
	}

	locked = false;
}

void phoenix::pListView::constructor(void)
{
	qtWidget = qtListView = new QTreeWidget;
	qtListView->setAllColumnsShowFocus(true);
	qtListView->setRootIsDecorated(false);

	connect(qtListView, SIGNAL(itemActivated(QTreeWidgetItem*, int)),
	    SLOT(onActivate()));
	connect(qtListView, SIGNAL(currentItemChanged(QTreeWidgetItem*,
	    QTreeWidgetItem*)), SLOT(onChange(QTreeWidgetItem*)));
	connect(qtListView, SIGNAL(itemChanged(QTreeWidgetItem*, int)),
	    SLOT(onToggle(QTreeWidgetItem*)));

	pWidget::synchronizeState();
	setCheckable(listView.state.checkable);
	setHeaderText(listView.state.headerText.size() ?
	    listView.state.headerText : nall::lstring{ " " });
	setHeaderVisible(listView.state.headerVisible);
	for (auto &row : listView.state.text) {
		append(row);
	}

	if (listView.state.checkable) {
		for (unsigned n = 0; n < listView.state.checked.size();
		    n++) {
			setChecked(n, listView.state.checked[n]);
		}
	}

	setSelected(listView.state.selected);
	if (listView.state.selected) {
		setSelection(listView.state.selection);
	}

	autoSizeColumns();
}

void phoenix::pListView::destructor(void)
{
	delete qtListView;
	qtWidget = qtListView = 0;
}

void phoenix::pListView::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pListView::onActivate(void)
{
	if (locked == false && listView.onActivate) {
		listView.onActivate();
	}
}

void phoenix::pListView::onChange(QTreeWidgetItem *item)
{
	// Qt bug workaround: clicking items with mouse does not
	// mark items as selected
	if (item) {
		item->setSelected(true);
	}

	listView.state.selected = selected();
	if (listView.state.selected) {
		listView.state.selection = selection();
	}

	if (locked == false && listView.onChange) {
		listView.onChange();
	}
}

void phoenix::pListView::onToggle(QTreeWidgetItem *item)
{
	unsigned row = item->data(0, Qt::UserRole).toUInt();
	bool checkState = checked(row);
	listView.state.checked[row] = checkState;
	if (locked == false && listView.onToggle) {
		listView.onToggle(row);
	}
}
