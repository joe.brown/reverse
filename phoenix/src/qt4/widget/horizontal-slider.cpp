#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Geometry phoenix::pHorizontalSlider::minimumGeometry(void)
{
	return { 0, 0, 0, 20 };
}

unsigned phoenix::pHorizontalSlider::position(void)
{
	return qtSlider->value();
}

void phoenix::pHorizontalSlider::setLength(unsigned length)
{
	length += length == 0;
	qtSlider->setRange(0, length - 1);
	qtSlider->setPageStep(length >> 3);
}

void phoenix::pHorizontalSlider::setPosition(unsigned position)
{
	qtSlider->setValue(position);
}

void phoenix::pHorizontalSlider::constructor(void)
{
	qtWidget = qtSlider = new QSlider(Qt::Horizontal);
	qtSlider->setRange(0, 100);
	qtSlider->setPageStep(101 >> 3);
	connect(qtSlider, SIGNAL(valueChanged(int)), SLOT(onChange()));

	pWidget::synchronizeState();
	setLength(horizontalSlider.state.length);
	setPosition(horizontalSlider.state.position);
}

void phoenix::pHorizontalSlider::destructor(void)
{
	delete qtSlider;
	qtWidget = qtSlider = 0;
}

void phoenix::pHorizontalSlider::orphan(void)
{
	destructor();
	constructor();
}

void phoenix::pHorizontalSlider::onChange(void)
{
	horizontalSlider.state.position = position();
	if (horizontalSlider.onChange) {
		horizontalSlider.onChange();
	}
}
