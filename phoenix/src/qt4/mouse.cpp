#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Position phoenix::pMouse::position(void)
{
	QPoint point = QCursor::pos();
	return { point.x(), point.y() };
}

bool phoenix::pMouse::pressed(phoenix::Mouse::Button button)
{
	Qt::MouseButtons buttons = QApplication::mouseButtons();

	switch (button) {
	case phoenix::Mouse::Button::Left:
		return buttons & Qt::LeftButton;
	case phoenix::Mouse::Button::Middle:
		return buttons & Qt::MidButton;
	case phoenix::Mouse::Button::Right:
		return buttons & Qt::RightButton;
	}

	return false;
}
