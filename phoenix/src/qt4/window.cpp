#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

extern phoenix::Settings *phoenix::pOS::settings;

void phoenix::pWindow::append(Layout &layout)
{
	Geometry geometry = window.state.geometry;
	geometry.x = geometry.y = 0;
	layout.setGeometry(geometry);
}

void phoenix::pWindow::append(Menu &menu)
{
	if (window.state.menuFont != "") {
		menu.p.setFont(window.state.menuFont);
	} else {
		menu.p.setFont("Sans, 8");
	}

	qtMenu->addMenu(menu.p.qtMenu);
}

void phoenix::pWindow::append(Widget &widget)
{
	if (widget.state.font == "") {
		if (window.state.widgetFont != "") {
			widget.p.setFont(window.state.widgetFont);
		} else {
			widget.p.setFont("Sans, 8");
		}
	}

	widget.p.qtWidget->setParent(qtContainer);
	widget.setVisible(widget.visible());
}

phoenix::Color phoenix::pWindow::backgroundColor(void)
{
	if (window.state.backgroundColorOverride) {
		return window.state.backgroundColor;
	}

	QColor color = qtWindow->palette().color(QPalette::ColorRole::Window);
	return { (uint8_t)color.red(), (uint8_t)color.green(),
	    (uint8_t)color.blue(), (uint8_t)color.alpha() };
}

phoenix::Geometry phoenix::pWindow::frameMargin(void)
{
	unsigned menuHeight = window.state.menuVisible ?
	    pOS::settings->menuGeometryHeight : 0;
	unsigned statusHeight = window.state.statusVisible ?
	    pOS::settings->statusGeometryHeight : 0;
	if (window.state.fullScreen) {
		return { 0, menuHeight, 0, menuHeight + statusHeight };
	}

	return {
		pOS::settings->frameGeometryX,
		pOS::settings->frameGeometryY + menuHeight,
		pOS::settings->frameGeometryWidth,
		pOS::settings->frameGeometryHeight +
		    menuHeight + statusHeight
	};
}

bool phoenix::pWindow::focused(void)
{
	return qtWindow->isActiveWindow() && !qtWindow->isMinimized();
}

phoenix::Geometry phoenix::pWindow::geometry(void)
{
	if (window.state.fullScreen) {
		unsigned menuHeight = window.state.menuVisible ?
		    qtMenu->height() : 0;
		unsigned statusHeight = window.state.statusVisible ?
		    qtStatus->height() : 0;
		return { 0, menuHeight, Desktop::size().width,
		    Desktop::size().height - menuHeight - statusHeight };
	}

	return window.state.geometry;
}

void phoenix::pWindow::remove(phoenix::Layout &layout) { }

void phoenix::pWindow::remove(phoenix::Menu &menu) {
	//QMenuBar::removeMenu() does not exist
	qtMenu->clear();
	for (auto &menu : window.state.menu) {
		append(menu);
	}
}

void phoenix::pWindow::remove(phoenix::Widget &widget)
{
	/*
	* bugfix: orphan() destroys and recreates widgets (to
	* disassociate them from their parent); attempting to
	* create widget again after QApplication::quit()
	* crashes libQtGui.
	*/
	if (pOS::qtApplication) {
		widget.p.orphan();
	}
}

void phoenix::pWindow::setBackgroundColor(const phoenix::Color &color)
{
	QPalette palette;
	palette.setColor(QPalette::Window, QColor(color.red, color.green,
	    color.blue, color.alpha));
	qtContainer->setPalette(palette);
	qtContainer->setAutoFillBackground(true);
	qtWindow->setAttribute(Qt::WA_TranslucentBackground,
	    color.alpha != 255);
}

void phoenix::pWindow::setFocused(void)
{
	qtWindow->raise();
	qtWindow->activateWindow();
}

void phoenix::pWindow::setFullScreen(bool fullScreen)
{
	if (fullScreen == false) {
		setResizable(window.state.resizable);
		qtWindow->showNormal();
		qtWindow->adjustSize();
	} else {
		qtLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
		qtContainer->setFixedSize(Desktop::size().width -
		    frameMargin().width, Desktop::size().height -
		    frameMargin().height);
		qtWindow->showFullScreen();
	}
}

void phoenix::pWindow::setGeometry(const phoenix::Geometry &geometry_)
{
	locked = true;
	phoenix::OS::processEvents();
	QApplication::syncX();
	phoenix::Geometry geometry = geometry_, margin = frameMargin();

	setResizable(window.state.resizable);
	qtWindow->move(geometry.x - frameMargin().x,
	    geometry.y - frameMargin().y);
	//qtWindow->adjustSize() fails if larger than 2/3rds screen size
	qtWindow->resize(qtWindow->sizeHint());
	qtWindow->setMinimumSize(1, 1);
	qtContainer->setMinimumSize(1, 1);

	for (auto &layout : window.state.layout) {
		geometry = geometry_;
		geometry.x = geometry.y = 0;
		layout.setGeometry(geometry);
	}

	locked = false;
}

void phoenix::pWindow::setMenuFont(const nall::string &font)
{
	qtMenu->setFont(pFont::create(font));
	for (auto &item : window.state.menu) {
		item.p.setFont(font);
	}
}

void phoenix::pWindow::setMenuVisible(bool visible)
{
	qtMenu->setVisible(visible);
	setGeometry(window.state.geometry);
}

void phoenix::pWindow::setResizable(bool resizable)
{
	if (resizable) {
		qtLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
		qtContainer->setMinimumSize(window.state.geometry.width,
		    window.state.geometry.height);
	} else {
		qtLayout->setSizeConstraint(QLayout::SetFixedSize);
		qtContainer->setFixedSize(window.state.geometry.width,
		    window.state.geometry.height);
	}

	qtStatus->setSizeGripEnabled(resizable);
}

void phoenix::pWindow::setStatusFont(const nall::string &font)
{
	qtStatus->setFont(pFont::create(font));
}

void phoenix::pWindow::setStatusText(const nall::string &text)
{
	qtStatus->showMessage(QString::fromUtf8(text), 0);
}

void phoenix::pWindow::setStatusVisible(bool visible)
{
	qtStatus->setVisible(visible);
	setGeometry(window.state.geometry);
}

void phoenix::pWindow::setTitle(const nall::string &text)
{
	qtWindow->setWindowTitle(QString::fromUtf8(text));
}

void phoenix::pWindow::setVisible(bool visible)
{
	locked = true;

	qtWindow->setVisible(visible);
	if (visible) {
		updateFrameGeometry();
		setGeometry(window.state.geometry);
	}

	locked = false;
}

void phoenix::pWindow::setWidgetFont(const nall::string &font)
{
	for (auto &item : window.state.widget) {
		if (!item.state.font) {
			item.setFont(font);
		}
	}
}

void phoenix::pWindow::constructor(void)
{
	qtWindow = new QtWindow(*this);
	qtWindow->setWindowTitle(" ");

	qtLayout = new QVBoxLayout(qtWindow);
	qtLayout->setMargin(0);
	qtLayout->setSpacing(0);
	qtWindow->setLayout(qtLayout);

	qtMenu = new QMenuBar(qtWindow);
	qtMenu->setVisible(false);
	qtLayout->addWidget(qtMenu);

	qtContainer = new QWidget(qtWindow);
	qtContainer->setSizePolicy(QSizePolicy::Expanding,
	    QSizePolicy::Expanding);
	qtContainer->setVisible(true);
	qtLayout->addWidget(qtContainer);

	qtStatus = new QStatusBar(qtWindow);
	qtStatus->setSizeGripEnabled(true);
	qtStatus->setVisible(false);
	qtLayout->addWidget(qtStatus);

	setGeometry(window.state.geometry);
	/* XXX: Hardcoded fonts */
	setMenuFont("Sans, 8");
	setStatusFont("Sans, 8");
}

void phoenix::pWindow::destructor(void)
{
	delete qtStatus;
	delete qtContainer;
	delete qtMenu;
	delete qtLayout;
	delete qtWindow;
}

void phoenix::pWindow::updateFrameGeometry(void)
{
	phoenix::pOS::syncX();
	QRect border = qtWindow->frameGeometry();
	QRect client = qtWindow->geometry();

	pOS::settings->frameGeometryX = client.x() - border.x();
	pOS::settings->frameGeometryY = client.y() - border.y();
	pOS::settings->frameGeometryWidth = border.width() - client.width();
	pOS::settings->frameGeometryHeight =
	    border.height() - client.height();

	if (window.state.menuVisible) {
		pOS::syncX();
		pOS::settings->menuGeometryHeight = qtMenu->height();
	}

	if (window.state.statusVisible) {
		pOS::syncX();
		pOS::settings->statusGeometryHeight = qtStatus->height();
	}

	pOS::settings->save();
}

void phoenix::pWindow::QtWindow::closeEvent(QCloseEvent *event)
{
	self.window.state.ignore = false;
	event->ignore();
	if (self.window.onClose) {
		self.window.onClose();
	}

	if (self.window.state.ignore == false) {
		hide();
	}
}

void phoenix::pWindow::QtWindow::moveEvent(QMoveEvent *event)
{
	if (self.locked == false && self.window.state.fullScreen == false &&
	    self.qtWindow->isVisible() == true) {
		self.window.state.geometry.x +=
		    event->pos().x() - event->oldPos().x();
		self.window.state.geometry.y +=
		    event->pos().y() - event->oldPos().y();
	}

	if (self.locked == false) {
		if (self.window.onMove) {
			self.window.onMove();
		}
	}
}

void phoenix::pWindow::QtWindow::keyPressEvent(QKeyEvent *event)
{
	phoenix::Keyboard::Keycode sym =
	    phoenix::Keysym(event->nativeVirtualKey());
	if (sym != Keyboard::Keycode::None && self.window.onKeyPress) {
		self.window.onKeyPress(sym);
	}
}

void phoenix::pWindow::QtWindow::keyReleaseEvent(QKeyEvent *event)
{
	Keyboard::Keycode sym = phoenix::Keysym(event->nativeVirtualKey());
	if (sym != Keyboard::Keycode::None && self.window.onKeyRelease) {
		self.window.onKeyRelease(sym);
	}
}

void phoenix::pWindow::QtWindow::resizeEvent(QResizeEvent*)
{
	if (self.locked == false && self.window.state.fullScreen == false &&
	    self.qtWindow->isVisible() == true) {
		self.window.state.geometry.width =
		    self.qtContainer->geometry().width();
		self.window.state.geometry.height =
		    self.qtContainer->geometry().height();
	}

	for (auto &layout : self.window.state.layout) {
		Geometry geometry = self.geometry();
		geometry.x = geometry.y = 0;
		layout.setGeometry(geometry);
	}

	if (self.locked == false) {
		if (self.window.onSize) {
			self.window.onSize();
		}
	}
}

QSize phoenix::pWindow::QtWindow::sizeHint(void) const
{
	unsigned width = self.window.state.geometry.width;
	unsigned height = self.window.state.geometry.height;
	if (self.window.state.menuVisible) {
		height += pOS::settings->menuGeometryHeight;
	}

	if (self.window.state.statusVisible) {
		height += pOS::settings->statusGeometryHeight;
	}

	return QSize(width, height);
}
