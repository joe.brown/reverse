#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

static QMessageBox::StandardButtons
    MessageWindow_buttons(phoenix::MessageWindow::Buttons buttons)
{
	QMessageBox::StandardButtons standardButtons = QMessageBox::NoButton;
	if (buttons == phoenix::MessageWindow::Buttons::Ok) {
		standardButtons = QMessageBox::Ok;
	}

	if (buttons == phoenix::MessageWindow::Buttons::OkCancel) {
		standardButtons = QMessageBox::Ok | QMessageBox::Cancel;
	}

	if (buttons == phoenix::MessageWindow::Buttons::YesNo) {
		standardButtons = QMessageBox::Yes | QMessageBox::No;
	}

	return standardButtons;
}

static phoenix::MessageWindow::Response
    MessageWindow_response(phoenix::MessageWindow::Buttons buttons,
    QMessageBox::StandardButton response)
{
	if (response == QMessageBox::Ok) {
		return phoenix::MessageWindow::Response::Ok;
	}

	if (response == QMessageBox::Cancel) {
		return phoenix::MessageWindow::Response::Cancel;
	}

	if (response == QMessageBox::Yes) {
		return phoenix::MessageWindow::Response::Yes;
	}

	if (response == QMessageBox::No) {
		return phoenix::MessageWindow::Response::No;
	}

	/*
	* MessageWindow was closed via window manager, rather
	* than by a button; assume a cancel/no response.
	*/
	if (buttons == phoenix::MessageWindow::Buttons::OkCancel) {
		return phoenix::MessageWindow::Response::Cancel;
	}

	if (buttons == phoenix::MessageWindow::Buttons::YesNo) {
		return phoenix::MessageWindow::Response::No;
	}

	return phoenix::MessageWindow::Response::Ok;
}

phoenix::MessageWindow::Response phoenix::pMessageWindow::information(
    Window &parent, const nall::string &text,
    phoenix::MessageWindow::Buttons buttons)
{
	return MessageWindow_response(
	    buttons, QMessageBox::information(&parent != &Window::None ?
	    parent.p.qtWindow : 0, " ", QString::fromUtf8(text),
	    MessageWindow_buttons(buttons)));
}

phoenix::MessageWindow::Response phoenix::pMessageWindow::question(
    phoenix::Window &parent, const nall::string &text,
    phoenix::MessageWindow::Buttons buttons)
{
	return MessageWindow_response(
	    buttons, QMessageBox::question(&parent != &Window::None ?
	    parent.p.qtWindow : 0, " ", QString::fromUtf8(text),
	    MessageWindow_buttons(buttons)));
}

phoenix::MessageWindow::Response phoenix::pMessageWindow::warning(
    phoenix::Window &parent, const nall::string &text,
    phoenix::MessageWindow::Buttons buttons)
{
	return MessageWindow_response(
	    buttons, QMessageBox::warning(&parent != &Window::None ?
	    parent.p.qtWindow : 0, " ", QString::fromUtf8(text),
	    MessageWindow_buttons(buttons)));
}

phoenix::MessageWindow::Response phoenix::pMessageWindow::critical(
    phoenix::Window &parent, const nall::string &text,
    phoenix::MessageWindow::Buttons buttons)
{
	return MessageWindow_response(
	    buttons, QMessageBox::critical(&parent != &Window::None ?
	    parent.p.qtWindow : 0, " ", QString::fromUtf8(text),
	    MessageWindow_buttons(buttons)));
}
