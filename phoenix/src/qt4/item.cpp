#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

void phoenix::pItem::setImage(const nall::image &image)
{
	nall::image qtBuffer = image;
	qtBuffer.transform(0, 32u, 255u << 24, 255u << 16,
	    255u << 8, 255u << 0);

	QImage qtImage(qtBuffer.data, qtBuffer.width, qtBuffer.height,
	    QImage::Format_ARGB32);
	QIcon qtIcon(QPixmap::fromImage(qtImage));
	qtAction->setIcon(qtIcon);
}

void phoenix::pItem::setText(const nall::string &text)
{
	qtAction->setText(QString::fromUtf8(text));
}

void phoenix::pItem::constructor(void)
{
	qtAction = new QAction(0);
	connect(qtAction, SIGNAL(triggered()), SLOT(onActivate()));
}

void phoenix::pItem::destructor(void)
{
	if (action.state.menu) {
		action.state.menu->remove(item);
	}

	delete qtAction;
}

void phoenix::pItem::onActivate(void)
{
	if (item.onActivate) {
		item.onActivate();
	}
}
