#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

bool phoenix::pRadioItem::checked(void)
{
	return qtAction->isChecked();
}

void phoenix::pRadioItem::setChecked()
{
	locked = true;

	for (auto &item : radioItem.state.group) {
		bool checkState = item.p.qtAction == qtAction;
		item.state.checked = checkState;
		item.p.qtAction->setChecked(checkState);
	}

	locked = false;
}

void phoenix::pRadioItem::setGroup(const nall::array<RadioItem&> &group) { }

void phoenix::pRadioItem::setText(const nall::string &text)
{
	qtAction->setText(QString::fromUtf8(text));
}

void phoenix::pRadioItem::constructor(void)
{
	qtAction = new QAction(0);
	qtGroup = new QActionGroup(0);
	qtAction->setCheckable(true);
	qtAction->setActionGroup(qtGroup);
	qtAction->setChecked(true);
	connect(qtAction, SIGNAL(triggered()), SLOT(onActivate()));
}

void phoenix::pRadioItem::destructor(void)
{
	if (action.state.menu) {
		action.state.menu->remove(radioItem);
	}

	delete qtAction;
}

void phoenix::pRadioItem::onActivate(void)
{
	if (radioItem.state.checked == false) {
		setChecked();
		if (locked == false && radioItem.onActivate) {
			radioItem.onActivate();
		}
	}
}
