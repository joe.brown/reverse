#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

phoenix::Keyboard::Keycode phoenix::Keysym(int keysym)
{
	switch (keysym) {
	case XK_Escape:
		return phoenix::Keyboard::Keycode::Escape;
	case XK_F1:
		return phoenix::Keyboard::Keycode::F1;
	case XK_F2:
		return phoenix::Keyboard::Keycode::F2;
	case XK_F3:
		return phoenix::Keyboard::Keycode::F3;
	case XK_F4:
		return phoenix::Keyboard::Keycode::F4;
	case XK_F5:
		return phoenix::Keyboard::Keycode::F5;
	case XK_F6:
		return phoenix::Keyboard::Keycode::F6;
	case XK_F7:
		return phoenix::Keyboard::Keycode::F7;
	case XK_F8:
		return phoenix::Keyboard::Keycode::F8;
	case XK_F9:
		return phoenix::Keyboard::Keycode::F9;
	case XK_F10:
		return phoenix::Keyboard::Keycode::F10;
	case XK_F11:
		return phoenix::Keyboard::Keycode::F11;
	case XK_F12:
		return phoenix::Keyboard::Keycode::F12;

	case XK_Print:
		return phoenix::Keyboard::Keycode::PrintScreen;
	case XK_Scroll_Lock:
		return phoenix::Keyboard::Keycode::ScrollLock;
	case XK_Pause:
		return phoenix::Keyboard::Keycode::Pause;
	case XK_Insert:
		return phoenix::Keyboard::Keycode::Insert;
	case XK_Delete:
		return phoenix::Keyboard::Keycode::Delete;
	case XK_Home:
		return phoenix::Keyboard::Keycode::Home;
	case XK_End:
		return phoenix::Keyboard::Keycode::End;
	case XK_Prior:
		return phoenix::Keyboard::Keycode::PageUp;
	case XK_Next:
		return phoenix::Keyboard::Keycode::PageDown;
	case XK_Up:
		return phoenix::Keyboard::Keycode::Up;
	case XK_Down:
		return phoenix::Keyboard::Keycode::Down;
	case XK_Left:
		return phoenix::Keyboard::Keycode::Left;
	case XK_Right:
		return phoenix::Keyboard::Keycode::Right;
	case XK_grave:
		return phoenix::Keyboard::Keycode::Grave;
	case XK_1:
		return phoenix::Keyboard::Keycode::Number1;
	case XK_2:
		return phoenix::Keyboard::Keycode::Number2;
	case XK_3:
		return phoenix::Keyboard::Keycode::Number3;
	case XK_4:
		return phoenix::Keyboard::Keycode::Number4;
	case XK_5:
		return phoenix::Keyboard::Keycode::Number5;
	case XK_6:
		return phoenix::Keyboard::Keycode::Number6;
	case XK_7:
		return phoenix::Keyboard::Keycode::Number7;
	case XK_8:
		return phoenix::Keyboard::Keycode::Number8;
	case XK_9:
		return phoenix::Keyboard::Keycode::Number9;
	case XK_0:
		return phoenix::Keyboard::Keycode::Number0;
	case XK_minus:
		return phoenix::Keyboard::Keycode::Minus;
	case XK_equal:
		return phoenix::Keyboard::Keycode::Equal;
	case XK_BackSpace:
		return phoenix::Keyboard::Keycode::Backspace;
	case XK_asciitilde:
		return phoenix::Keyboard::Keycode::Tilde;
	case XK_exclam:
		return phoenix::Keyboard::Keycode::Exclamation;
	case XK_at:
		return phoenix::Keyboard::Keycode::At;
	case XK_numbersign:
		return phoenix::Keyboard::Keycode::Pound;
	case XK_dollar:
		return phoenix::Keyboard::Keycode::Dollar;
	case XK_percent:
		return phoenix::Keyboard::Keycode::Percent;
	case XK_asciicircum:
		return phoenix::Keyboard::Keycode::Power;
	case XK_ampersand:
		return phoenix::Keyboard::Keycode::Ampersand;
	case XK_asterisk:
		return phoenix::Keyboard::Keycode::Asterisk;
	case XK_parenleft:
		return phoenix::Keyboard::Keycode::ParenthesisLeft;
	case XK_parenright:
		return phoenix::Keyboard::Keycode::ParenthesisRight;
	case XK_underscore:
		return phoenix::Keyboard::Keycode::Underscore;
	case XK_plus:
		return phoenix::Keyboard::Keycode::Plus;
	case XK_bracketleft:
		return phoenix::Keyboard::Keycode::BracketLeft;
	case XK_bracketright:
		return phoenix::Keyboard::Keycode::BracketRight;
	case XK_backslash:
		return phoenix::Keyboard::Keycode::Backslash;
	case XK_semicolon:
		return phoenix::Keyboard::Keycode::Semicolon;
	case XK_apostrophe:
		return phoenix::Keyboard::Keycode::Apostrophe;
	case XK_comma:
		return phoenix::Keyboard::Keycode::Comma;
	case XK_period:
		return phoenix::Keyboard::Keycode::Period;
	case XK_slash:
		return phoenix::Keyboard::Keycode::Slash;
	case XK_braceleft:
		return phoenix::Keyboard::Keycode::BraceLeft;
	case XK_braceright:
		return phoenix::Keyboard::Keycode::BraceRight;
	case XK_bar:
		return phoenix::Keyboard::Keycode::Pipe;
	case XK_colon:
		return phoenix::Keyboard::Keycode::Colon;
	case XK_quotedbl:
		return phoenix::Keyboard::Keycode::Quote;
	case XK_less:
		return phoenix::Keyboard::Keycode::CaretLeft;
	case XK_greater:
		return phoenix::Keyboard::Keycode::CaretRight;
	case XK_question:
		return phoenix::Keyboard::Keycode::Question;
	case XK_Tab:
		return phoenix::Keyboard::Keycode::Tab;
	case XK_Caps_Lock:
		return phoenix::Keyboard::Keycode::CapsLock;
	case XK_Return:
		return phoenix::Keyboard::Keycode::Return;
	case XK_Shift_L:
		return phoenix::Keyboard::Keycode::ShiftLeft;
	case XK_Shift_R:
		return phoenix::Keyboard::Keycode::ShiftRight;
	case XK_Control_L:
		return phoenix::Keyboard::Keycode::ControlLeft;
	case XK_Control_R:
		return phoenix::Keyboard::Keycode::ControlRight;
	case XK_Super_L:
		return phoenix::Keyboard::Keycode::SuperLeft;
	case XK_Super_R:
		return phoenix::Keyboard::Keycode::SuperRight;
	case XK_Alt_L:
		return phoenix::Keyboard::Keycode::AltLeft;
	case XK_Alt_R:
		return phoenix::Keyboard::Keycode::AltRight;
	case XK_space:
		return phoenix::Keyboard::Keycode::Space;
	case XK_Menu:
		return phoenix::Keyboard::Keycode::Menu;
	case XK_A:
		return phoenix::Keyboard::Keycode::A;
	case XK_B:
		return phoenix::Keyboard::Keycode::B;
	case XK_C:
		return phoenix::Keyboard::Keycode::C;
	case XK_D:
		return phoenix::Keyboard::Keycode::D;
	case XK_E:
		return phoenix::Keyboard::Keycode::E;
	case XK_F:
		return phoenix::Keyboard::Keycode::F;
	case XK_G:
		return phoenix::Keyboard::Keycode::G;
	case XK_H:
		return phoenix::Keyboard::Keycode::H;
	case XK_I:
		return phoenix::Keyboard::Keycode::I;
	case XK_J:
		return phoenix::Keyboard::Keycode::J;
	case XK_K:
		return phoenix::Keyboard::Keycode::K;
	case XK_L:
		return phoenix::Keyboard::Keycode::L;
	case XK_M:
		return phoenix::Keyboard::Keycode::M;
	case XK_N:
		return phoenix::Keyboard::Keycode::N;
	case XK_O:
		return phoenix::Keyboard::Keycode::O;
	case XK_P:
		return phoenix::Keyboard::Keycode::P;
	case XK_Q:
		return phoenix::Keyboard::Keycode::Q;
	case XK_R:
		return phoenix::Keyboard::Keycode::R;
	case XK_S:
		return phoenix::Keyboard::Keycode::S;
	case XK_T:
		return phoenix::Keyboard::Keycode::T;
	case XK_U:
		return phoenix::Keyboard::Keycode::U;
	case XK_V:
		return phoenix::Keyboard::Keycode::V;
	case XK_W:
		return phoenix::Keyboard::Keycode::W;
	case XK_X:
		return phoenix::Keyboard::Keycode::X;
	case XK_Y:
		return phoenix::Keyboard::Keycode::Y;
	case XK_Z:
		return phoenix::Keyboard::Keycode::Z;
	case XK_a:
		return phoenix::Keyboard::Keycode::a;
	case XK_b:
		return phoenix::Keyboard::Keycode::b;
	case XK_c:
		return phoenix::Keyboard::Keycode::c;
	case XK_d:
		return phoenix::Keyboard::Keycode::d;
	case XK_e:
		return phoenix::Keyboard::Keycode::e;
	case XK_f:
		return phoenix::Keyboard::Keycode::f;
	case XK_g:
		return phoenix::Keyboard::Keycode::g;
	case XK_h:
		return phoenix::Keyboard::Keycode::h;
	case XK_i:
		return phoenix::Keyboard::Keycode::i;
	case XK_j:
		return phoenix::Keyboard::Keycode::j;
	case XK_k:
		return phoenix::Keyboard::Keycode::k;
	case XK_l:
		return phoenix::Keyboard::Keycode::l;
	case XK_m:
		return phoenix::Keyboard::Keycode::m;
	case XK_n:
		return phoenix::Keyboard::Keycode::n;
	case XK_o:
		return phoenix::Keyboard::Keycode::o;
	case XK_p:
		return phoenix::Keyboard::Keycode::p;
	case XK_q:
		return phoenix::Keyboard::Keycode::q;
	case XK_r:
		return phoenix::Keyboard::Keycode::r;
	case XK_s:
		return phoenix::Keyboard::Keycode::s;
	case XK_t:
		return phoenix::Keyboard::Keycode::t;
	case XK_u:
		return phoenix::Keyboard::Keycode::u;
	case XK_v:
		return phoenix::Keyboard::Keycode::v;
	case XK_w:
		return phoenix::Keyboard::Keycode::w;
	case XK_x:
		return phoenix::Keyboard::Keycode::x;
	case XK_y:
		return phoenix::Keyboard::Keycode::y;
	case XK_z:
		return phoenix::Keyboard::Keycode::z;
	case XK_Num_Lock:
		return phoenix::Keyboard::Keycode::NumLock;
	case XK_KP_Divide:
		return phoenix::Keyboard::Keycode::Divide;
	case XK_KP_Multiply:
		return phoenix::Keyboard::Keycode::Multiply;
	case XK_KP_Subtract:
		return phoenix::Keyboard::Keycode::Subtract;
	case XK_KP_Add:
		return phoenix::Keyboard::Keycode::Add;
	case XK_KP_Enter:
		return phoenix::Keyboard::Keycode::Enter;
	case XK_KP_Decimal:
		return phoenix::Keyboard::Keycode::Point;
	case XK_KP_1:
		return phoenix::Keyboard::Keycode::Keypad1;
	case XK_KP_2:
		return phoenix::Keyboard::Keycode::Keypad2;
	case XK_KP_3:
		return phoenix::Keyboard::Keycode::Keypad3;
	case XK_KP_4:
		return phoenix::Keyboard::Keycode::Keypad4;
	case XK_KP_5:
		return phoenix::Keyboard::Keycode::Keypad5;
	case XK_KP_6:
		return phoenix::Keyboard::Keycode::Keypad6;
	case XK_KP_7:
		return phoenix::Keyboard::Keycode::Keypad7;
	case XK_KP_8:
		return phoenix::Keyboard::Keycode::Keypad8;
	case XK_KP_9:
		return phoenix::Keyboard::Keycode::Keypad9;
	case XK_KP_0:
		return phoenix::Keyboard::Keycode::Keypad0;
	case XK_KP_Home:
		return phoenix::Keyboard::Keycode::KeypadHome;
	case XK_KP_End:
		return phoenix::Keyboard::Keycode::KeypadEnd;
	case XK_KP_Page_Up:
		return phoenix::Keyboard::Keycode::KeypadPageUp;
	case XK_KP_Page_Down:
		return phoenix::Keyboard::Keycode::KeypadPageDown;
	case XK_KP_Up:
		return phoenix::Keyboard::Keycode::KeypadUp;
	case XK_KP_Down:
		return phoenix::Keyboard::Keycode::KeypadDown;
	case XK_KP_Left:
		return phoenix::Keyboard::Keycode::KeypadLeft;
	case XK_KP_Right:
		return phoenix::Keyboard::Keycode::KeypadRight;
	case XK_KP_Begin:
		return phoenix::Keyboard::Keycode::KeypadCenter;
	case XK_KP_Insert:
		return phoenix::Keyboard::Keycode::KeypadInsert;
	case XK_KP_Delete:
		return phoenix::Keyboard::Keycode::KeypadDelete;
	}

	return phoenix::Keyboard::Keycode::None;
}
