#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

nall::string phoenix::pDialogWindow::fileOpen(phoenix::Window &parent,
    const nall::string &path, const nall::lstring &filter)
{
	nall::string filterList;
	for (auto &item : filter) {
		filterList.append(item);
		filterList.append(";;");
	}
	filterList.rtrim<1>(";;");

	/*
	* Convert filter list from phoenix to Qt format, example:
	* "Text, XML files (*.txt,*.xml)" -> "Text, XML files (*.txt *.xml)"
	*/
	signed parenthesis = 0;
	for (auto &n : filterList) {
		if (n == '(') {
			parenthesis++;
		}

		if (n == ')') {
			parenthesis--;
		}

		if (n == ',' && parenthesis) {
			n = ' ';
		}
	}

	QString filename = QFileDialog::getOpenFileName(
		&parent != &Window::None ? parent.p.qtWindow : 0, "Load File",
		QString::fromUtf8(path), QString::fromUtf8(filterList)
	);

	return filename.toUtf8().constData();
}

nall::string phoenix::pDialogWindow::fileSave(phoenix::Window &parent,
    const nall::string &path, const nall::lstring &filter)
{
	nall::string filterList;
	for (auto &item : filter) {
		filterList.append(item);
		filterList.append(";;");
	}

	filterList.rtrim<1>(";;");

	/*
	* convert filter list from phoenix to Qt format, example:
	* "Text, XML files (*.txt,*.xml)" -> "Text, XML files (*.txt *.xml)"
	*/
	signed parenthesis = 0;
	for (auto &n : filterList) {
		if (n == '(') {
			parenthesis++;
		}

		if (n == ')') {
			parenthesis--;
		}

		if (n == ',' && parenthesis) {
			n = ' ';
		}
	}

	QString filename = QFileDialog::getSaveFileName(
		&parent != &Window::None ? parent.p.qtWindow : 0, "Save File",
		QString::fromUtf8(path), QString::fromUtf8(filterList)
	);

	return filename.toUtf8().constData();
}

nall::string phoenix::pDialogWindow::folderSelect(phoenix::Window &parent,
    const nall::string &path)
{
	QString directory = QFileDialog::getExistingDirectory(
		&parent != &Window::None ? parent.p.qtWindow : 0,
		"Select Directory", QString::fromUtf8(path),
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks
	);

	nall::string name = directory.toUtf8().constData();
	if (name != "" && name.endswith("/") == false) {
		name.append("/");
	}

	return name;
}
