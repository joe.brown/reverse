#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

bool phoenix::pCheckItem::checked(void)
{
	return qtAction->isChecked();
}

void phoenix::pCheckItem::setChecked(bool checked)
{
	qtAction->setChecked(checked);
}

void phoenix::pCheckItem::setText(const nall::string &text)
{
	qtAction->setText(QString::fromUtf8(text));
}

void phoenix::pCheckItem::constructor(void)
{
	qtAction = new QAction(0);
	qtAction->setCheckable(true);
	connect(qtAction, SIGNAL(triggered()), SLOT(onToggle()));
}

void phoenix::pCheckItem::destructor(void)
{
	if (action.state.menu) {
		action.state.menu->remove(checkItem);
	}

	delete qtAction;
}

void phoenix::pCheckItem::onToggle(void)
{
	checkItem.state.checked = checked();
	if (checkItem.onToggle) {
		checkItem.onToggle();
	}
}
