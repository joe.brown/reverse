#include "phoenix/phoenix.h"
#include "phoenix/platform/qt4.h"

void phoenix::pTimer::setEnabled(bool enabled)
{
	if (enabled) {
		qtTimer->start();
	} else {
		qtTimer->stop();
	}
}

void phoenix::pTimer::setInterval(unsigned milliseconds)
{
	qtTimer->setInterval(milliseconds);
}

void phoenix::pTimer::constructor(void)
{
	qtTimer = new QTimer;
	qtTimer->setInterval(0);
	connect(qtTimer, SIGNAL(timeout()), SLOT(onTimeout()));
}

void phoenix::pTimer::destructor(void)
{
	delete qtTimer;
}

void phoenix::pTimer::onTimeout(void)
{
	if (timer.onTimeout) {
		timer.onTimeout();
	}
}
