#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

static void RadioItem_activate(phoenix::RadioItem *self)
{
	for (auto &item : self->state.group) {
		item.state.checked = (&item == self);
	}

	if (self->p.locked == false && self->checked() && self->onActivate) {
		self->onActivate();
	}
}

bool phoenix::pRadioItem::checked(void)
{
	return gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
}

void phoenix::pRadioItem::setChecked(void)
{
	locked = true;
	for (auto &item : radioItem.state.group) {
		gtk_check_menu_item_set_active(
		    GTK_CHECK_MENU_ITEM(item.p.widget), false);
	}

	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(widget), true);
	locked = false;
}

void phoenix::pRadioItem::setGroup(
    const nall::array<phoenix::RadioItem&> &group)
{
	for (unsigned n = 0; n < group.size(); n++) {
		if (n == 0) {
			continue;
		}

		GSList *currentGroup =
		    gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(
		    group[0].p.widget));
		if (currentGroup != gtk_radio_menu_item_get_group(
		    GTK_RADIO_MENU_ITEM(group[n].p.widget))) {
			gtk_radio_menu_item_set_group(
			    GTK_RADIO_MENU_ITEM(group[n].p.widget),
			    currentGroup);
		}
	}
}

void phoenix::pRadioItem::setText(const nall::string &text)
{
	gtk_menu_item_set_label(GTK_MENU_ITEM(widget), mnemonic(text));
}

void phoenix::pRadioItem::constructor(void)
{
	widget = gtk_radio_menu_item_new_with_mnemonic(0, "");
	setGroup(radioItem.state.group);
	setText(radioItem.state.text);
	for (auto &item : radioItem.state.group) {
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(
		    item.p.widget), item.state.checked);
	}

	g_signal_connect_swapped(G_OBJECT(widget), "toggled",
	    G_CALLBACK(RadioItem_activate), (gpointer)&radioItem);
}

void phoenix::pRadioItem::destructor(void)
{
	gtk_widget_destroy(widget);
}

void phoenix::pRadioItem::orphan(void)
{
	destructor();
	constructor();
}
