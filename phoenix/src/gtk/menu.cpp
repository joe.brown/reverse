#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

void phoenix::pMenu::append(phoenix::Action &action)
{
	action.state.window = this->action.state.window;

	gtk_menu_shell_append(GTK_MENU_SHELL(gtkMenu), action.p.widget);
	if (action.state.window &&
	    action.state.window->state.menuFont != "") {
		action.p.setFont(action.state.window->state.menuFont);
	}

	gtk_widget_show(action.p.widget);
}

void phoenix::pMenu::remove(phoenix::Action &action)
{
	action.p.orphan();
	action.state.window = 0;
}

void phoenix::pMenu::setImage(const nall::image &image)
{
	GtkImage *gtkImage = CreateImage(image, /* menuIcon = */ true);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(widget),
	    (GtkWidget*)gtkImage);
}

void phoenix::pMenu::setText(const nall::string &text)
{
	gtk_menu_item_set_label(GTK_MENU_ITEM(widget), mnemonic(text));
}

void phoenix::pMenu::constructor(void)
{
	gtkMenu = gtk_menu_new();
	widget = gtk_image_menu_item_new_with_mnemonic("");
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(widget), gtkMenu);
	setText(menu.state.text);
}

void phoenix::pMenu::destructor(void)
{
	gtk_widget_destroy(gtkMenu);
	gtk_widget_destroy(widget);
}

void phoenix::pMenu::orphan(void)
{
	for (auto &action : menu.state.action) {
		action.p.orphan();
	}

	destructor();
	constructor();

	for (auto &action : menu.state.action) {
		append(action);
	}
}

void phoenix::pMenu::setFont(const nall::string &font)
{
	pAction::setFont(font);
	for (auto &item : menu.state.action) {
		item.p.setFont(font);
	}
}
