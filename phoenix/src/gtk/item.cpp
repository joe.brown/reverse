#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

static void Item_activate(phoenix::Item *self)
{
	if (self->onActivate) {
		self->onActivate();
	}
}

void phoenix::pItem::setImage(const nall::image &image)
{
	GtkImage *gtkImage = phoenix::CreateImage(image, true);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(widget),
	    (GtkWidget*)gtkImage);
}

void phoenix::pItem::setText(const nall::string &text)
{
	gtk_menu_item_set_label(GTK_MENU_ITEM(widget), mnemonic(text));
}

void phoenix::pItem::constructor(void)
{
	widget = gtk_image_menu_item_new_with_mnemonic("");
	g_signal_connect_swapped(G_OBJECT(widget), "activate",
	    G_CALLBACK(Item_activate), (gpointer)&item);
	setText(item.state.text);
}

void phoenix::pItem::destructor(void)
{
	gtk_widget_destroy(widget);
}

void phoenix::pItem::orphan(void)
{
	destructor();
	constructor();
}
