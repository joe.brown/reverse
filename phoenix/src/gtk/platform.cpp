#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

XlibDisplay* phoenix::pOS::display = 0;
phoenix::Settings* phoenix::pOS::settings = 0;
phoenix::Font phoenix::pOS::defaultFont;

void phoenix::pOS::main(void)
{
	gtk_main();
}

bool phoenix::pOS::pendingEvents(void)
{
	return gtk_events_pending();
}

void phoenix::pOS::processEvents(void)
{
	while (pendingEvents()) {
		gtk_main_iteration_do(false);
	}
}

void phoenix::pOS::quit(void)
{
	gtk_main_quit();
}

void phoenix::pOS::initialize(void)
{
	display = XOpenDisplay(0);

	settings = new Settings;
	settings->load();

	int argc = 1;
	char *argv[2];
	argv[0] = new char[8];
	argv[1] = 0;
	strcpy(argv[0], "phoenix");
	char **argvp = argv;
	gtk_init(&argc, &argvp);

	gtk_rc_parse_string(R"(
		style "phoenix-gtk" {
			GtkWindow::resize-grip-width = 0
			GtkWindow::resize-grip-height = 0
			GtkTreeView::vertical-separator = 0
			GtkComboBox::appears-as-list = 1
		}
		class "GtkWindow" style "phoenix-gtk"
		class "GtkTreeView" style "phoenix-gtk"
		# class "GtkComboBox" style "phoenix-gtk")");

	pKeyboard::initialize();
}
