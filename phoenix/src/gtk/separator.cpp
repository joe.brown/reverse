#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

void phoenix::pSeparator::constructor(void)
{
	widget = gtk_separator_menu_item_new();
}

void phoenix::pSeparator::destructor(void)
{
	gtk_widget_destroy(widget);
}

void phoenix::pSeparator::orphan(void)
{
	destructor();
	constructor();
}
