#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

static nall::string FileDialog(bool save, phoenix::Window &parent,
    const nall::string &path, const nall::lstring &filter)
{
	nall::string name;

	GtkWidget *dialog = gtk_file_chooser_dialog_new(
	    save == 0 ? "Load File" : "Save File",
	    &parent != &phoenix::Window::None ?
	    GTK_WINDOW(parent.p.widget) : (GtkWindow*)0,
	    save == 0 ? GTK_FILE_CHOOSER_ACTION_OPEN :
	    GTK_FILE_CHOOSER_ACTION_SAVE, GTK_STOCK_CANCEL,
	    GTK_RESPONSE_CANCEL, GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
	    (const gchar*)0);

	if (path) {
		gtk_file_chooser_set_current_folder(
		    GTK_FILE_CHOOSER(dialog), path);
	}

	for (auto &filterItem : filter) {
		GtkFileFilter *gtkFilter = gtk_file_filter_new();
		gtk_file_filter_set_name(gtkFilter, filterItem);
		nall::lstring part;
		part.split("(", filterItem);
		part[1].rtrim<1>(")");
		nall::lstring list;
		list.split(",", part[1]);
		for (auto &pattern : list) {
			gtk_file_filter_add_pattern(gtkFilter, pattern);
		}

		gtk_file_chooser_add_filter(
		    GTK_FILE_CHOOSER(dialog), gtkFilter);
	}

	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		char *temp = gtk_file_chooser_get_filename(
		    GTK_FILE_CHOOSER(dialog));
		name = temp;
		g_free(temp);
	}

	gtk_widget_destroy(dialog);
	return name;
}

nall::string phoenix::pDialogWindow::fileOpen(Window &parent,
    const nall::string &path, const nall::lstring &filter)
{
	return FileDialog(0, parent, path, filter);
}

nall::string phoenix::pDialogWindow::fileSave(Window &parent,
    const nall::string &path, const nall::lstring &filter)
{
	return FileDialog(1, parent, path, filter);
}

nall::string phoenix::pDialogWindow::folderSelect(Window &parent,
    const nall::string &path)
{
	nall::string name;

	GtkWidget *dialog = gtk_file_chooser_dialog_new("Select Folder",
	    &parent != &Window::None ? GTK_WINDOW(parent.p.widget) :
	    (GtkWindow*)0, GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
	    GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_OPEN,
	    GTK_RESPONSE_ACCEPT, (const gchar*)0);

	if (path) {
		gtk_file_chooser_set_current_folder(
		    GTK_FILE_CHOOSER(dialog), path);
	}

	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		char *temp = gtk_file_chooser_get_filename(
		    GTK_FILE_CHOOSER(dialog));
		name = temp;
		g_free(temp);
	}

	gtk_widget_destroy(dialog);
	if (name == "") {
		return "";
	}

	if (name.endswith("/") == false) {
		name.append("/");
	}

	return name;
}
