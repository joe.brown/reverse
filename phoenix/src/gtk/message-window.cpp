#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

static phoenix::MessageWindow::Response
    MessageWindow_response(phoenix::MessageWindow::Buttons buttons,
    gint response)
{
	if (response == GTK_RESPONSE_OK) {
		return phoenix::MessageWindow::Response::Ok;
	}

	if (response == GTK_RESPONSE_CANCEL) {
		return phoenix::MessageWindow::Response::Cancel;
	}

	if (response == GTK_RESPONSE_YES) {
		return phoenix::MessageWindow::Response::Yes;
	}

	if (response == GTK_RESPONSE_NO) {
		return phoenix::MessageWindow::Response::No;
	}

	if (buttons == phoenix::MessageWindow::Buttons::OkCancel) {
		return phoenix::MessageWindow::Response::Cancel;
	}

	if (buttons == phoenix::MessageWindow::Buttons::YesNo) {
		return phoenix::MessageWindow::Response::No;
	}

	return phoenix::MessageWindow::Response::Ok;
}

phoenix::MessageWindow::Response
    phoenix::pMessageWindow::information(phoenix::Window &parent,
    const nall::string &text, phoenix::MessageWindow::Buttons buttons)
{
	GtkButtonsType buttonsType = GTK_BUTTONS_OK;
	if (buttons == MessageWindow::Buttons::OkCancel) {
		buttonsType = GTK_BUTTONS_OK_CANCEL;
	}

	if (buttons == MessageWindow::Buttons::YesNo) {
		buttonsType = GTK_BUTTONS_YES_NO;
	}

	GtkWidget *dialog = gtk_message_dialog_new(
	    &parent != &Window::None ? GTK_WINDOW(parent.p.widget) :
	    (GtkWindow*)0, GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, buttonsType,
	    "%s", (const char*)text);
	gint response = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	return MessageWindow_response(buttons, response);
}

phoenix::MessageWindow::Response
    phoenix::pMessageWindow::question(phoenix::Window &parent,
    const nall::string &text, phoenix::MessageWindow::Buttons buttons)
{
	GtkButtonsType buttonsType = GTK_BUTTONS_OK;
	if (buttons == MessageWindow::Buttons::OkCancel) {
		buttonsType = GTK_BUTTONS_OK_CANCEL;
	}

	if (buttons == MessageWindow::Buttons::YesNo) {
		buttonsType = GTK_BUTTONS_YES_NO;
	}

	GtkWidget *dialog = gtk_message_dialog_new(
	    &parent != &Window::None ? GTK_WINDOW(parent.p.widget) :
	    (GtkWindow*)0, GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION,
	    buttonsType, "%s", (const char*)text);
	gint response = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	return MessageWindow_response(buttons, response);
}

phoenix::MessageWindow::Response
    phoenix::pMessageWindow::warning(phoenix::Window &parent,
    const nall::string &text, phoenix::MessageWindow::Buttons buttons)
{
	GtkButtonsType buttonsType = GTK_BUTTONS_OK;
	if (buttons == MessageWindow::Buttons::OkCancel) {
		buttonsType = GTK_BUTTONS_OK_CANCEL;
	}

	if (buttons == MessageWindow::Buttons::YesNo) {
		buttonsType = GTK_BUTTONS_YES_NO;
	}

	GtkWidget *dialog = gtk_message_dialog_new(
	    &parent != &Window::None ? GTK_WINDOW(parent.p.widget) :
	    (GtkWindow*)0, GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING,
	    buttonsType, "%s", (const char*)text);
	gint response = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	return MessageWindow_response(buttons, response);
}

phoenix::MessageWindow::Response
    phoenix::pMessageWindow::critical(phoenix::Window &parent,
    const nall::string &text, phoenix::MessageWindow::Buttons buttons)
{
	GtkButtonsType buttonsType = GTK_BUTTONS_OK;
	if (buttons == MessageWindow::Buttons::OkCancel) {
		buttonsType = GTK_BUTTONS_OK_CANCEL;
	}

	if (buttons == MessageWindow::Buttons::YesNo) {
		buttonsType = GTK_BUTTONS_YES_NO;
	}

	GtkWidget *dialog = gtk_message_dialog_new(
	    &parent != &Window::None ? GTK_WINDOW(parent.p.widget) :
	    (GtkWindow*)0, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR,
	    buttonsType, "%s", (const char*)text);
	gint response = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	return MessageWindow_response(buttons, response);
}
