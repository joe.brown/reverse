#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

static void CheckItem_toggle(phoenix::CheckItem *self)
{
	if (self->p.locked == false && self->onToggle) {
		self->onToggle();
	}
}

bool phoenix::pCheckItem::checked(void)
{
	return gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
}

void phoenix::pCheckItem::setChecked(bool checked)
{
	locked = true;
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(widget), checked);
	locked = false;
}

void phoenix::pCheckItem::setText(const nall::string &text)
{
	gtk_menu_item_set_label(GTK_MENU_ITEM(widget), mnemonic(text));
}

void phoenix::pCheckItem::constructor(void)
{
	widget = gtk_check_menu_item_new_with_mnemonic("");
	setChecked(checkItem.state.checked);
	setText(checkItem.state.text);
	g_signal_connect_swapped(G_OBJECT(widget), "toggled",
	    G_CALLBACK(CheckItem_toggle), (gpointer)&checkItem);
}

void phoenix::pCheckItem::destructor(void)
{
	gtk_widget_destroy(widget);
}

void phoenix::pCheckItem::orphan(void)
{
	destructor();
	constructor();
}
