#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

phoenix::Geometry phoenix::pProgressBar::minimumGeometry(void)
{
	return { 0, 0, 0, 25 };
}

void phoenix::pProgressBar::setPosition(unsigned position)
{
	position = position <= 100 ? position : 0;
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(gtkWidget),
	    (double)position / 100.0);
}

void phoenix::pProgressBar::constructor(void)
{
	gtkWidget = gtk_progress_bar_new();

	setPosition(progressBar.state.position);
}

void phoenix::pProgressBar::destructor(void)
{
	gtk_widget_destroy(gtkWidget);
}

void phoenix::pProgressBar::orphan(void)
{
	destructor();
	constructor();
}
