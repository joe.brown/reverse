#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

phoenix::Geometry phoenix::pLabel::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(widget.state.font,
	    label.state.text);
	return { 0, 0, geometry.width, geometry.height };
}

void phoenix::pLabel::setText(const nall::string &text)
{
	gtk_label_set_text(GTK_LABEL(gtkWidget), text);
}

void phoenix::pLabel::constructor(void)
{
	gtkWidget = gtk_label_new("");
	gtk_misc_set_alignment(GTK_MISC(gtkWidget), 0.0, 0.5);

	setText(label.state.text);
}

void phoenix::pLabel::destructor(void)
{
	gtk_widget_destroy(gtkWidget);
}

void phoenix::pLabel::orphan(void)
{
	destructor();
	constructor();
}
