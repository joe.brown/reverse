#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

static void RadioBox_activate(phoenix::RadioBox *self)
{
	if (self->p.locked == false && self->checked() &&
	    self->onActivate) {
		self->onActivate();
	}
}

bool phoenix::pRadioBox::checked(void)
{
	return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtkWidget));
}

phoenix::Geometry phoenix::pRadioBox::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(widget.state.font,
	    radioBox.state.text);
	/* Font &font = pWidget::font(); */
	/* Geometry geometry = font.geometry(radioBox.state.text); */
	return { 0, 0, geometry.width + 28, geometry.height + 4 };
}

void phoenix::pRadioBox::setChecked(void)
{
	locked = true;
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(gtkWidget), true);
	locked = false;
}

void phoenix::pRadioBox::setGroup(const nall::array<RadioBox&> &group)
{
	for (unsigned n = 0; n < group.size(); n++) {
		if (n == 0) {
			continue;
		}

		GSList *currentGroup = gtk_radio_button_get_group(
		    GTK_RADIO_BUTTON(group[0].p.gtkWidget));
		if (currentGroup != gtk_radio_button_get_group(
		    GTK_RADIO_BUTTON(gtkWidget))) {
			gtk_radio_button_set_group(GTK_RADIO_BUTTON(
			    gtkWidget), currentGroup);
		}
	}
}

void phoenix::pRadioBox::setText(const nall::string &text)
{
	gtk_button_set_label(GTK_BUTTON(gtkWidget), text);
}

void phoenix::pRadioBox::constructor(void)
{
	gtkWidget = gtk_radio_button_new_with_label(0, "");
	g_signal_connect_swapped(G_OBJECT(gtkWidget), "toggled",
	    G_CALLBACK(RadioBox_activate), (gpointer)&radioBox);

	setText(radioBox.state.text);
}

void phoenix::pRadioBox::destructor(void)
{
	gtk_widget_destroy(gtkWidget);
}

void phoenix::pRadioBox::orphan(void)
{
	destructor();
	constructor();
}
