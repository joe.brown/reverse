#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

static void HorizontalScrollBar_change(phoenix::HorizontalScrollBar *self)
{
	if (self->state.position == self->position()) {
		return;
	}

	self->state.position = self->position();
	if (self->p.locked == false && self->onChange) {
		self->onChange();
	}
}

phoenix::Geometry phoenix::pHorizontalScrollBar::minimumGeometry(void)
{
	return { 0, 0, 0, 20 };
}

unsigned phoenix::pHorizontalScrollBar::position(void)
{
	return (unsigned)gtk_range_get_value(GTK_RANGE(gtkWidget));
}

void phoenix::pHorizontalScrollBar::setLength(unsigned length)
{
	locked = true;
	length += length == 0;
	gtk_range_set_range(GTK_RANGE(gtkWidget), 0,
	    nall::max(1u, length - 1));
	gtk_range_set_increments(GTK_RANGE(gtkWidget), 1, length >> 3);
	locked = false;
}

void phoenix::pHorizontalScrollBar::setPosition(unsigned position)
{
	gtk_range_set_value(GTK_RANGE(gtkWidget), position);
}

void phoenix::pHorizontalScrollBar::constructor(void)
{
	gtkWidget = gtk_hscrollbar_new(0);
	g_signal_connect_swapped(G_OBJECT(gtkWidget), "value-changed",
	    G_CALLBACK(HorizontalScrollBar_change),
	    (gpointer)&horizontalScrollBar);

	setLength(horizontalScrollBar.state.length);
	setPosition(horizontalScrollBar.state.position);
}

void phoenix::pHorizontalScrollBar::destructor(void)
{
	gtk_widget_destroy(gtkWidget);
}

void phoenix::pHorizontalScrollBar::orphan(void)
{
	destructor();
	constructor();
}
