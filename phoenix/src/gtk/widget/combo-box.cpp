#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

static void ComboBox_change(phoenix::ComboBox *self)
{
	if (self->p.locked == false) {
		self->state.selection = self->selection();
		if (self->onChange) {
			self->onChange();
		}
	}
}

void phoenix::pComboBox::append(const nall::string &text)
{
	gtk_combo_box_append_text(GTK_COMBO_BOX(gtkWidget), text);
	if (itemCounter++ == 0) {
		setSelection(0);
	}
}

phoenix::Geometry phoenix::pComboBox::minimumGeometry(void)
{
	unsigned maximumWidth = 0;
	for (auto &item : comboBox.state.text) {
		maximumWidth = nall::max(maximumWidth,
		    pFont::geometry(widget.state.font, item).width);
	}

	Geometry geometry = pFont::geometry(widget.state.font, " ");
	return { 0, 0, maximumWidth + 44, geometry.height + 12 };
}

void phoenix::pComboBox::reset(void)
{
	locked = true;
	gtk_list_store_clear(GTK_LIST_STORE(gtk_combo_box_get_model(
	    GTK_COMBO_BOX(gtkWidget))));
	itemCounter = 0;
	locked = false;
}

unsigned phoenix::pComboBox::selection(void)
{
	return gtk_combo_box_get_active(GTK_COMBO_BOX(gtkWidget));
}

void phoenix::pComboBox::setSelection(unsigned row)
{
	locked = true;
	gtk_combo_box_set_active(GTK_COMBO_BOX(gtkWidget), row);
	locked = false;
}

void phoenix::pComboBox::constructor(void)
{
	itemCounter = 0;
	gtkWidget = gtk_combo_box_new_text();
	g_signal_connect_swapped(G_OBJECT(gtkWidget), "changed",
	    G_CALLBACK(ComboBox_change), (gpointer)&comboBox);

	locked = true;
	for (auto &text : comboBox.state.text) {
		append(text);
	}

	locked = false;
	setSelection(comboBox.state.selection);
}

void phoenix::pComboBox::destructor(void)
{
	gtk_widget_destroy(gtkWidget);
}

void phoenix::pComboBox::orphan(void)
{
	destructor();
	constructor();
}
