#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

phoenix::Geometry phoenix::pWidget::minimumGeometry(void)
{
	return { 0, 0, 0, 0 };
}

bool phoenix::pWidget::enabled(void)
{
	return gtk_widget_get_sensitive(gtkWidget);
}

void phoenix::pWidget::setEnabled(bool enabled)
{
	if (widget.state.abstract) {
		enabled = false;
	}

	if (sizable.state.layout &&
	    sizable.state.layout->enabled() == false) {
		enabled = false;
	}

	gtk_widget_set_sensitive(gtkWidget, enabled);
}

void phoenix::pWidget::setFocused(void)
{
	gtk_widget_grab_focus(gtkWidget);
}

void phoenix::pWidget::setFont(const nall::string &font)
{
	pFont::setFont(gtkWidget, font);
}

void phoenix::pWidget::setGeometry(const phoenix::Geometry &geometry)
{
	if (sizable.window() && sizable.window()->visible()) {
		gtk_fixed_move(GTK_FIXED(sizable.window()->p.formContainer),
		    gtkWidget, geometry.x, geometry.y);
	}

	unsigned width = (signed)geometry.width <= 0 ?
	    1U : geometry.width;
	unsigned height = (signed)geometry.height <= 0 ?
	    1U : geometry.height;
	gtk_widget_set_size_request(gtkWidget, width, height);
}

void phoenix::pWidget::setVisible(bool visible)
{
	if (widget.state.abstract) {
		visible = false;
	}

	if (sizable.state.layout &&
	    sizable.state.layout->visible() == false) {
		visible = false;
	}

	gtk_widget_set_visible(gtkWidget, visible);
}

void phoenix::pWidget::constructor(void)
{
	if (widget.state.abstract) {
		gtkWidget = gtk_label_new("");
	}
}

void phoenix::pWidget::destructor(void)
{
	if (widget.state.abstract) {
		gtk_widget_destroy(gtkWidget);
	}
}

void phoenix::pWidget::orphan(void)
{
	destructor();
	constructor();
}
