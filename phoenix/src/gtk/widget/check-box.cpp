#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

static void CheckBox_toggle(phoenix::CheckBox *self)
{
	self->state.checked = self->checked();
	if (self->p.locked == false && self->onToggle) {
		self->onToggle();
	}
}

bool phoenix::pCheckBox::checked(void)
{
	return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtkWidget));
}

phoenix::Geometry phoenix::pCheckBox::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(widget.state.font,
	    checkBox.state.text);
	return { 0, 0, geometry.width + 28, geometry.height + 4 };
}

void phoenix::pCheckBox::setChecked(bool checked)
{
	locked = true;
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(gtkWidget), checked);
	locked = false;
}

void phoenix::pCheckBox::setText(const nall::string &text)
{
	gtk_button_set_label(GTK_BUTTON(gtkWidget), text);
}

void phoenix::pCheckBox::constructor(void)
{
	gtkWidget = gtk_check_button_new_with_label("");
	g_signal_connect_swapped(G_OBJECT(gtkWidget), "toggled",
	    G_CALLBACK(CheckBox_toggle), (gpointer)&checkBox);

	setChecked(checkBox.state.checked);
	setText(checkBox.state.text);
}

void phoenix::pCheckBox::destructor(void)
{
	gtk_widget_destroy(gtkWidget);
}

void phoenix::pCheckBox::orphan(void)
{
	destructor();
	constructor();
}
