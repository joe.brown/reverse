#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

static void LineEdit_activate(phoenix::LineEdit *self)
{
	if (self->onActivate) {
		self->onActivate();
	}
}

static void LineEdit_change(phoenix::LineEdit *self)
{
	self->state.text = self->text();
	if (self->p.locked == false && self->onChange) {
		self->onChange();
	}
}

phoenix::Geometry phoenix::pLineEdit::minimumGeometry(void)
{
	Geometry geometry = pFont::geometry(widget.state.font,
	    lineEdit.state.text);
	return { 0, 0, geometry.width + 10, geometry.height + 10 };
}

void phoenix::pLineEdit::setEditable(bool editable)
{
	gtk_editable_set_editable(GTK_EDITABLE(gtkWidget), editable);
}

void phoenix::pLineEdit::setText(const nall::string &text)
{
	locked = true;
	gtk_entry_set_text(GTK_ENTRY(gtkWidget), text);
	locked = false;
}

nall::string phoenix::pLineEdit::text(void)
{
	return gtk_entry_get_text(GTK_ENTRY(gtkWidget));
}

void phoenix::pLineEdit::constructor(void)
{
	gtkWidget = gtk_entry_new();
	g_signal_connect_swapped(G_OBJECT(gtkWidget), "activate",
	    G_CALLBACK(LineEdit_activate), (gpointer)&lineEdit);
	g_signal_connect_swapped(G_OBJECT(gtkWidget), "changed",
	    G_CALLBACK(LineEdit_change), (gpointer)&lineEdit);

	setEditable(lineEdit.state.editable);
	setText(lineEdit.state.text);
}

void phoenix::pLineEdit::destructor(void)
{
	gtk_widget_destroy(gtkWidget);
}

void phoenix::pLineEdit::orphan(void)
{
	destructor();
	constructor();
}
