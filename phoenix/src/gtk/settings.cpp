#include "phoenix/phoenix.h"
#include "phoenix/platform/gtk2.h"

//static Settings *settings = nullptr;

void phoenix::Settings::load(void)
{
	nall::string path = { nall::userpath(), ".config/phoenix/gtk.cfg" };
	configuration::load(path);
}

void phoenix::Settings::save(void)
{
	nall::string path = { nall::userpath(), ".config/" };
	mkdir(path, 0755);
	path.append("phoenix/");
	mkdir(path, 0755);
	path.append("gtk.cfg");
	configuration::save(path);
}

phoenix::Settings::Settings(void)
{
	append(frameGeometryX = 4, "frameGeometryX");
	append(frameGeometryY = 24, "frameGeometryY");
	append(frameGeometryWidth = 8, "frameGeometryWidth");
	append(frameGeometryHeight = 28, "frameGeometryHeight");
	append(menuGeometryHeight = 20, "menuGeometryHeight");
	append(statusGeometryHeight = 20, "statusGeometryHeight");
	append(windowBackgroundColor = 0xedeceb, "windowBackgroundColor");
}
