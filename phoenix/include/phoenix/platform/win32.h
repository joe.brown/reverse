#ifndef PHOENIX_PLATFORM_WIN32_H
#define PHOENIX_PLATFORM_WIN32_H

#define UNICODE
//#define WINVER 0x0501
//#define _WIN32_WINNT 0x0501
//#define _WIN32_IE 0x0600
#define __MSVCRT_VERSION__ 0x0601
#define NOMINMAX 1

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <io.h>
#include <shlobj.h>

#include <nall/windows/utf8.hpp>

static const unsigned Windows2000  = 0x0500;
static const unsigned WindowsXP    = 0x0501;
static const unsigned WindowsVista = 0x0600;
static const unsigned Windows7     = 0x0601;

namespace phoenix {

struct pFont;
struct pWindow;
struct pMenu;
struct pLayout;
struct pWidget;

static bool osQuit = false;

struct pFont {
	static Geometry geometry(const nall::string &description,
	    const nall::string &text);

	static HFONT create(const nall::string &description);
	static void free(HFONT hfont);
	static Geometry geometry(HFONT hfont, const nall::string &text);
};

struct pDesktop {
	static Size size(void);
	static Geometry workspace(void);
};

struct pKeyboard {
	static bool pressed(Keyboard::Scancode scancode);
	static nall::array<bool> state(void);

	static void initialize(void);
};

struct pMouse {
	static Position position(void);
	static bool pressed(Mouse::Button button);
};

struct pDialogWindow {
	static nall::string fileOpen(Window &parent,
	    const nall::string &path, const nall::lstring &filter);
	static nall::string fileSave(Window &parent,
	    const nall::string &path, const nall::lstring &filter);
	static nall::string folderSelect(Window &parent,
	    const nall::string &path);
};

struct pMessageWindow {
	static MessageWindow::Response information(Window &parent,
	    const nall::string &text, MessageWindow::Buttons buttons);
	static MessageWindow::Response question(Window &parent,
	    const nall::string &text, MessageWindow::Buttons buttons);
	static MessageWindow::Response warning(Window &parent,
	    const nall::string &text, MessageWindow::Buttons buttons);
	static MessageWindow::Response critical(Window &parent,
	    const nall::string &text, MessageWindow::Buttons buttons);
};

struct pObject {
	Object &object;
	uintptr_t id;
	bool locked;
	static nall::array<pObject*> objects;

	pObject(Object &object);
	static pObject* find(unsigned id);
	virtual ~pObject(void) { }

	void constructor(void) { }
	void destructor(void) { }
};

struct pOS : public pObject {
	static Settings* settings;
	static void main(void);
	static bool pendingEvents(void);
	static void processEvents(void);
	static void quit(void);

	static void initialize(void);
};

struct pTimer : public pObject {
	Timer &timer;
	UINT_PTR htimer;

	void setEnabled(bool enabled);
	void setInterval(unsigned milliseconds);

	pTimer(Timer &timer) : pObject(timer), timer(timer) { }
	void constructor(void);
};

struct pWindow : public pObject {
	Window &window;
	HWND hwnd;
	HMENU hmenu;
	HWND hstatus;
	HFONT hstatusfont;
	HBRUSH brush;
	COLORREF brushColor;

	void append(Layout &layout);
	void append(Menu &menu);
	void append(Widget &widget);
	Color backgroundColor(void);
	bool focused(void);
	Geometry frameMargin(void);
	Geometry geometry(void);
	void remove(Layout &layout);
	void remove(Menu &menu);
	void remove(Widget &widget);
	void setBackgroundColor(const Color &color);
	void setFocused(void);
	void setFullScreen(bool fullScreen);
	void setGeometry(const Geometry &geometry);
	void setMenuFont(const nall::string &font);
	void setMenuVisible(bool visible);
	void setResizable(bool resizable);
	void setStatusFont(const nall::string &font);
	void setStatusText(const nall::string &text);
	void setStatusVisible(bool visible);
	void setTitle(const nall::string &text);
	void setVisible(bool visible);
	void setWidgetFont(const nall::string &font);

	pWindow(Window &window) : pObject(window), window(window) { }
	void constructor(void);
	void destructor(void);
	void updateMenu(void);
};

struct pAction : public pObject {
	Action &action;
	Menu *parentMenu;
	Window *parentWindow;

	void setEnabled(bool enabled);
	void setVisible(bool visible);

	pAction(Action &action) : pObject(action), action(action) { }
	void constructor(void);
};

struct pMenu : public pAction {
	Menu &menu;
	HMENU hmenu;
	HBITMAP hbitmap;

	void append(Action &action);
	void remove(Action &action);
	void setImage(const nall::image &image);
	void setText(const nall::string &text);

	pMenu(Menu &menu) : pAction(menu), menu(menu), hbitmap(0) { }
	void constructor(void);
	void destructor(void);
	void createBitmap(void);
	void update(Window &parentWindow, Menu *parentMenu = 0);
};

struct pSeparator : public pAction {
	Separator &separator;

	pSeparator(Separator &separator) :
	    pAction(separator), separator(separator) { }
	void constructor(void);
	void destructor(void);
};

struct pItem : public pAction {
	Item &item;
	HBITMAP hbitmap;

	void setImage(const nall::image &image);
	void setText(const nall::string &text);

	pItem(Item &item) : pAction(item), item(item), hbitmap(0) { }
	void constructor(void);
	void destructor(void);
	void createBitmap(void);
};

struct pCheckItem : public pAction {
	CheckItem &checkItem;

	bool checked(void);
	void setChecked(bool checked);
	void setText(const nall::string &text);

	pCheckItem(CheckItem &checkItem) :
	    pAction(checkItem), checkItem(checkItem) { }
	void constructor(void);
	void destructor(void);
};

struct pRadioItem : public pAction {
	RadioItem &radioItem;

	bool checked(void);
	void setChecked(void);
	void setGroup(const nall::array<RadioItem&> &group);
	void setText(const nall::string &text);

	pRadioItem(RadioItem &radioItem) :
	    pAction(radioItem), radioItem(radioItem) { }
	void constructor(void);
	void destructor(void);
};

struct pSizable : public pObject {
	Sizable &sizable;

	pSizable(Sizable &sizable) : pObject(sizable), sizable(sizable) { }
};

struct pLayout : public pSizable {
	Layout &layout;

	pLayout(Layout &layout) : pSizable(layout), layout(layout) { }
};

struct pWidget : public pSizable {
	Widget &widget;
	Window *parentWindow;
	HWND hwnd;
	HFONT hfont;

	bool enabled(void);
	virtual Geometry minimumGeometry(void);
	void setEnabled(bool enabled);
	void setFocused(void);
	void setFont(const nall::string &font);
	virtual void setGeometry(const Geometry &geometry);
	void setVisible(bool visible);

	pWidget(Widget &widget) : pSizable(widget), widget(widget)
	    { parentWindow = &Window::None; }
	void constructor(void);
	void destructor(void);
	virtual void orphan(void);
	void setDefaultFont(void);
	void synchronize(void);
};

struct pButton : public pWidget {
	Button &button;
	HBITMAP hbitmap;
	HIMAGELIST himagelist;

	Geometry minimumGeometry(void);
	void setImage(const nall::image &image, Orientation orientation);
	void setText(const nall::string &text);

	pButton(Button &button) : pWidget(button), button(button),
	    hbitmap(0), himagelist(0) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pCanvas : public pWidget {
	Canvas &canvas;
	uint32_t *data;

	void setSize(const Size &size);
	void update(void);

	pCanvas(Canvas &canvas) : pWidget(canvas), canvas(canvas) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
	void paint(void);
};

struct pCheckBox : public pWidget {
	CheckBox &checkBox;

	bool checked(void);
	Geometry minimumGeometry(void);
	void setChecked(bool checked);
	void setText(const nall::string &text);

	pCheckBox(CheckBox &checkBox) :
	    pWidget(checkBox), checkBox(checkBox) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pComboBox : public pWidget {
	ComboBox &comboBox;

	void append(const nall::string &text);
	Geometry minimumGeometry(void);
	void reset(void);
	unsigned selection(void);
	void setSelection(unsigned row);

	pComboBox(ComboBox &comboBox) :
	    pWidget(comboBox), comboBox(comboBox) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
	void setGeometry(const Geometry &geometry);
};

struct pHexEdit : public pWidget {
	HexEdit &hexEdit;
	LRESULT CALLBACK (*windowProc)(HWND, UINT, LPARAM, WPARAM);

	void setColumns(unsigned columns);
	void setLength(unsigned length);
	void setOffset(unsigned offset);
	void setRows(unsigned rows);
	void update(void);

	pHexEdit(HexEdit &hexEdit) : pWidget(hexEdit), hexEdit(hexEdit) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
	bool keyPress(unsigned key);
};

struct pHorizontalScrollBar : public pWidget {
	HorizontalScrollBar &horizontalScrollBar;

	Geometry minimumGeometry(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	pHorizontalScrollBar(HorizontalScrollBar &horizontalScrollBar) :
	    pWidget(horizontalScrollBar),
	    horizontalScrollBar(horizontalScrollBar) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pHorizontalSlider : public pWidget {
	HorizontalSlider &horizontalSlider;

	Geometry minimumGeometry(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	pHorizontalSlider(HorizontalSlider &horizontalSlider) :
	    pWidget(horizontalSlider), horizontalSlider(horizontalSlider) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pLabel : public pWidget {
	Label &label;

	Geometry minimumGeometry(void);
	void setText(const nall::string &text);

	pLabel(Label &label) : pWidget(label), label(label) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pLineEdit : public pWidget {
	LineEdit &lineEdit;

	Geometry minimumGeometry(void);
	void setEditable(bool editable);
	void setText(const nall::string &text);
	nall::string text(void);

	pLineEdit(LineEdit &lineEdit) :
	    pWidget(lineEdit), lineEdit(lineEdit) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pListView : public pWidget {
	ListView &listView;
	bool lostFocus;

	void append(const nall::lstring &text);
	void autoSizeColumns(void);
	bool checked(unsigned row);
	void modify(unsigned row, const nall::lstring &text);
	void reset(void);
	bool selected(void);
	unsigned selection(void);
	void setCheckable(bool checkable);
	void setChecked(unsigned row, bool checked);
	void setHeaderText(const nall::lstring &text);
	void setHeaderVisible(bool visible);
	void setSelected(bool selected);
	void setSelection(unsigned row);

	pListView(ListView &listView) :
	    pWidget(listView), listView(listView) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
	void setGeometry(const Geometry &geometry);
};

struct pProgressBar : public pWidget {
	ProgressBar &progressBar;

	Geometry minimumGeometry();
	void setPosition(unsigned position);

	pProgressBar(ProgressBar &progressBar) :
	    pWidget(progressBar), progressBar(progressBar) {}
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pRadioBox : public pWidget {
	RadioBox &radioBox;

	bool checked(void);
	Geometry minimumGeometry(void);
	void setChecked(void);
	void setGroup(const nall::array<RadioBox&> &group);
	void setText(const nall::string &text);

	pRadioBox(RadioBox &radioBox) :
	    pWidget(radioBox), radioBox(radioBox) {}
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pTextEdit : public pWidget {
	TextEdit &textEdit;

	void setCursorPosition(unsigned position);
	void setEditable(bool editable);
	void setText(const nall::string &text);
	void setWordWrap(bool wordWrap);
	nall::string text(void);

	pTextEdit(TextEdit &textEdit) : pWidget(textEdit), textEdit(textEdit) {}
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pVerticalScrollBar : public pWidget {
	VerticalScrollBar &verticalScrollBar;

	Geometry minimumGeometry(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	pVerticalScrollBar(VerticalScrollBar &verticalScrollBar) :
	    pWidget(verticalScrollBar),
	    verticalScrollBar(verticalScrollBar) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pVerticalSlider : public pWidget {
	VerticalSlider &verticalSlider;

	Geometry minimumGeometry(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	pVerticalSlider(VerticalSlider &verticalSlider) :
	    pWidget(verticalSlider), verticalSlider(verticalSlider) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pViewport : public pWidget {
	Viewport &viewport;

	uintptr_t handle(void);

	pViewport(Viewport &viewport) :
	    pWidget(viewport), viewport(viewport) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

static unsigned OsVersion(void)
{
	OSVERSIONINFO versionInfo = { 0 };
	versionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&versionInfo);
	return (versionInfo.dwMajorVersion << 8) +
	    (versionInfo.dwMajorVersion << 0);
}

static HBITMAP CreateBitmap(const nall::image &image)
{
	HDC hdc = GetDC(0);
	BITMAPINFO bitmapInfo;

	memset(&bitmapInfo, 0, sizeof(BITMAPINFO));
	bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biWidth = image.width;
	/* bitmaps are stored upside down unless we negate height. */
	bitmapInfo.bmiHeader.biHeight = -image.height;
	bitmapInfo.bmiHeader.biPlanes = 1;
	bitmapInfo.bmiHeader.biBitCount = 32;
	bitmapInfo.bmiHeader.biCompression = BI_RGB;
	bitmapInfo.bmiHeader.biSizeImage = image.width * image.height * 4;

	void *bits = nullptr;
	HBITMAP hbitmap = CreateDIBSection(hdc, &bitmapInfo,
	    DIB_RGB_COLORS, &bits, NULL, 0);
	if (bits) {
		memcpy(bits, image.data, image.width * image.height * 4);
	}

	ReleaseDC(0, hdc);
	return hbitmap;
}

static Keyboard::Keycode Keysym(unsigned keysym, unsigned keyflags)
{
	#define pressed(keysym) (GetAsyncKeyState(keysym) & 0x8000)
	#define enabled(keysym) (GetKeyState(keysym))
	#define shifted() (pressed(VK_LSHIFT) || pressed(VK_RSHIFT))
	#define extended() (keyflags & (1 << 24))

	switch(keysym) {
	case VK_ESCAPE:
		return Keyboard::Keycode::Escape;
	case VK_F1:
		return Keyboard::Keycode::F1;
	case VK_F2:
		return Keyboard::Keycode::F2;
	case VK_F3:
		return Keyboard::Keycode::F3;
	case VK_F4:
		return Keyboard::Keycode::F4;
	case VK_F5:
		return Keyboard::Keycode::F5;
	case VK_F6:
		return Keyboard::Keycode::F6;
	case VK_F7:
		return Keyboard::Keycode::F7;
	case VK_F8:
		return Keyboard::Keycode::F8;
	case VK_F9:
		return Keyboard::Keycode::F9;
	/*
	* Keyboard::Keycode::F10 (should be captured under VK_MENU from
	* WM_SYSKEY(UP,DOWN); but this is not working...)
	*/
	case VK_F11:
		return Keyboard::Keycode::F11;
	case VK_F12:
		return Keyboard::Keycode::F12;

	/* Keyboard::Keycode::PrintScreen */
	/* Keyboard::Keycode::SysRq */
	case VK_SCROLL:
		return Keyboard::Keycode::ScrollLock;
	case VK_PAUSE:
		return Keyboard::Keycode::Pause;
	/* Keyboard::Keycode::Break */

	case VK_INSERT:
		return extended() ? Keyboard::Keycode::Insert :
		    Keyboard::Keycode::KeypadInsert;
	case VK_DELETE:
		return extended() ? Keyboard::Keycode::Delete :
		    Keyboard::Keycode::KeypadDelete;
	case VK_HOME:
		return extended() ? Keyboard::Keycode::Home :
		    Keyboard::Keycode::KeypadHome;
	case VK_END:
		return extended() ? Keyboard::Keycode::End :
		    Keyboard::Keycode::KeypadEnd;
	case VK_PRIOR:
		return extended() ? Keyboard::Keycode::PageUp :
		    Keyboard::Keycode::KeypadPageUp;
	case VK_NEXT:
		return extended() ? Keyboard::Keycode::PageDown :
		    Keyboard::Keycode::KeypadPageDown;

	case VK_UP:
		return extended() ? Keyboard::Keycode::Up :
		    Keyboard::Keycode::KeypadUp;
	case VK_DOWN:
		return extended() ? Keyboard::Keycode::Down :
		    Keyboard::Keycode::KeypadDown;
	case VK_LEFT:
		return extended() ? Keyboard::Keycode::Left :
		    Keyboard::Keycode::KeypadLeft;
	case VK_RIGHT:
		return extended() ? Keyboard::Keycode::Right :
		    Keyboard::Keycode::KeypadRight;

	case VK_OEM_3:
		return !shifted() ? Keyboard::Keycode::Grave :
		    Keyboard::Keycode::Tilde;
	case '1':
		return !shifted() ? Keyboard::Keycode::Number1 :
		    Keyboard::Keycode::Exclamation;
	case '2':
		return !shifted() ? Keyboard::Keycode::Number2 :
		    Keyboard::Keycode::At;
	case '3':
		return !shifted() ? Keyboard::Keycode::Number3 :
		    Keyboard::Keycode::Pound;
	case '4':
		return !shifted() ? Keyboard::Keycode::Number4 :
		    Keyboard::Keycode::Dollar;
	case '5':
		return !shifted() ? Keyboard::Keycode::Number5 :
		    Keyboard::Keycode::Percent;
	case '6':
		return !shifted() ? Keyboard::Keycode::Number6 :
		    Keyboard::Keycode::Power;
	case '7':
		return !shifted() ? Keyboard::Keycode::Number7 :
		    Keyboard::Keycode::Ampersand;
	case '8':
		return !shifted() ? Keyboard::Keycode::Number8 :
		    Keyboard::Keycode::Asterisk;
	case '9':
		return !shifted() ? Keyboard::Keycode::Number9 :
		    Keyboard::Keycode::ParenthesisLeft;
	case '0':
		return !shifted() ? Keyboard::Keycode::Number0 :
		    Keyboard::Keycode::ParenthesisRight;
	case VK_OEM_MINUS:
		return !shifted() ? Keyboard::Keycode::Minus :
		    Keyboard::Keycode::Underscore;
	case VK_OEM_PLUS:
		return !shifted() ? Keyboard::Keycode::Equal :
		    Keyboard::Keycode::Plus;
	case VK_BACK:
		return Keyboard::Keycode::Backspace;

	case VK_OEM_4:
		return !shifted() ? Keyboard::Keycode::BracketLeft :
		    Keyboard::Keycode::BraceLeft;
	case VK_OEM_6:
		return !shifted() ? Keyboard::Keycode::BracketRight :
		    Keyboard::Keycode::BraceRight;
	case VK_OEM_5:
		return !shifted() ? Keyboard::Keycode::Backslash :
		    Keyboard::Keycode::Pipe;
	case VK_OEM_1:
		return !shifted() ? Keyboard::Keycode::Semicolon :
		    Keyboard::Keycode::Colon;
	case VK_OEM_7:
		return !shifted() ? Keyboard::Keycode::Apostrophe :
		    Keyboard::Keycode::Quote;
	case VK_OEM_COMMA:
		return !shifted() ? Keyboard::Keycode::Comma :
		    Keyboard::Keycode::CaretLeft;
	case VK_OEM_PERIOD:
		return !shifted() ? Keyboard::Keycode::Period :
		    Keyboard::Keycode::CaretRight;
	case VK_OEM_2:
		return !shifted() ? Keyboard::Keycode::Slash :
		    Keyboard::Keycode::Question;

	case VK_TAB:
		return Keyboard::Keycode::Tab;
	case VK_CAPITAL:
		return Keyboard::Keycode::CapsLock;
	case VK_RETURN:
		return !extended() ? Keyboard::Keycode::Return :
		    Keyboard::Keycode::Enter;
	case VK_SHIFT:
		return !pressed(VK_RSHIFT) ? Keyboard::Keycode::ShiftLeft :
		    Keyboard::Keycode::ShiftRight;
	case VK_CONTROL:
		return !pressed(VK_RCONTROL) ?
		    Keyboard::Keycode::ControlLeft :
		    Keyboard::Keycode::ControlRight;
	case VK_LWIN:
		return Keyboard::Keycode::SuperLeft;
	case VK_RWIN:
		return Keyboard::Keycode::SuperRight;
	case VK_MENU:
		if (keyflags & (1 << 24)) {
			return Keyboard::Keycode::AltRight;
		}
		return Keyboard::Keycode::AltLeft;
	case VK_SPACE:
		return Keyboard::Keycode::Space;
	case VK_APPS:
		return Keyboard::Keycode::Menu;

	case 'A':
	case 'B':
	case 'C':
	case 'D':
	case 'E':
	case 'F':
	case 'G':
	case 'H':
	case 'I':
	case 'J':
	case 'K':
	case 'L':
	case 'M':
	case 'N':
	case 'O':
	case 'P':
	case 'Q':
	case 'R':
	case 'S':
	case 'T':
	case 'U':
	case 'V':
	case 'W':
	case 'X':
	case 'Y':
	case 'Z':
		if (enabled(VK_CAPITAL)) {
			if (shifted()) {
				return (Keyboard::Keycode)
				    ((unsigned)Keyboard::Keycode::a +
				    keysym - 'A');
			} else {
				return (Keyboard::Keycode)
				    ((unsigned)Keyboard::Keycode::A +
				    keysym - 'A');
			}
		} else {
			if (shifted()) {
				return (Keyboard::Keycode)
				    ((unsigned)Keyboard::Keycode::A +
				    keysym - 'A');
			} else {
				return (Keyboard::Keycode)
				    ((unsigned)Keyboard::Keycode::a +
				    keysym - 'A');
			}
		}
		break;

	case VK_NUMLOCK:
		return Keyboard::Keycode::NumLock;
	case VK_DIVIDE:
		return Keyboard::Keycode::Divide;
	case VK_MULTIPLY:
		return Keyboard::Keycode::Multiply;
	case VK_SUBTRACT:
		return Keyboard::Keycode::Subtract;
	case VK_ADD:
		return Keyboard::Keycode::Add;
	case VK_DECIMAL:
		return Keyboard::Keycode::Point;
	case VK_NUMPAD1:
		return Keyboard::Keycode::Keypad1;
	case VK_NUMPAD2:
		return Keyboard::Keycode::Keypad2;
	case VK_NUMPAD3:
		return Keyboard::Keycode::Keypad3;
	case VK_NUMPAD4:
		return Keyboard::Keycode::Keypad4;
	case VK_NUMPAD5:
		return Keyboard::Keycode::Keypad5;
	case VK_NUMPAD6:
		return Keyboard::Keycode::Keypad6;
	case VK_NUMPAD7:
		return Keyboard::Keycode::Keypad7;
	case VK_NUMPAD8:
		return Keyboard::Keycode::Keypad8;
	case VK_NUMPAD9:
		return Keyboard::Keycode::Keypad9;
	case VK_NUMPAD0:
		return Keyboard::Keycode::Keypad0;

	case VK_CLEAR:
		return Keyboard::Keycode::KeypadCenter;
	}

	return Keyboard::Keycode::None;

	#undef pressed
	#undef enabled
	#undef shifted
	#undef extended
}

} /* namespace phoenix */

LRESULT CALLBACK Canvas_windowProc(HWND hwnd, UINT msg, WPARAM wparam,
    LPARAM lparam);
LRESULT CALLBACK Label_windowProc(HWND hwnd, UINT msg, WPARAM wparam,
    LPARAM lparam);
LRESULT CALLBACK HexEdit_windowProc(HWND hwnd, UINT msg, WPARAM wparam,
    LPARAM lparam);
LRESULT CALLBACK Viewport_windowProc(HWND hwnd, UINT msg, WPARAM wparam,
    LPARAM lparam);

#endif
