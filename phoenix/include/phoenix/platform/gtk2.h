#ifndef PHOENIX_PLATFORM_GTK2_H
#define PHOENIX_PLATFORM_GTK2_H

#include <nall/xorg/guard.hpp>
#include <gtk/gtk.h>                                                      
#include <gdk/gdk.h>                                                      
#include <gdk/gdkx.h>                                                     
#include <gdk/gdkkeysyms.h>                                               
#include <cairo.h>                                                        
#include <X11/Xatom.h>                                                    
#include <nall/xorg/guard.hpp>                                            

#include <nall/image.hpp>
#include <nall/map.hpp>
#include <nall/config.hpp>

#include <phoenix/phoenix.h>

namespace phoenix {

struct pWindow;
struct pMenu;
struct pLayout;
struct pWidget;



struct pFont {
	static Geometry geometry(const nall::string &description,
	    const nall::string &text);

	static PangoFontDescription* create(const nall::string &description);
	static void free(PangoFontDescription *font);
	static Geometry geometry(PangoFontDescription *font,
	    const nall::string &text);
	static void setFont(GtkWidget *widget, const nall::string &font);
	static void setFont(GtkWidget *widget, gpointer font);
};

struct pDesktop {
	static Size size(void);
	static Geometry workspace(void);
};

struct pKeyboard {
	static bool pressed(Keyboard::Scancode scancode);
	static nall::array<bool> state();

	static void initialize(void);
};

struct pMouse {
	static Position position(void);
	static bool pressed(Mouse::Button button);
};

struct pDialogWindow {
	static nall::string fileOpen(Window &parent,
	    const nall::string &path, const nall::lstring &filter);
	static nall::string fileSave(Window &parent,
	    const nall::string &path, const nall::lstring &filter);
	static nall::string folderSelect(Window &parent,
	    const nall::string &path);
};

struct pMessageWindow {
	static MessageWindow::Response information(Window &parent,
	    const nall::string &text, MessageWindow::Buttons buttons);
	static MessageWindow::Response question(Window &parent,
	    const nall::string &text, MessageWindow::Buttons buttons);
	static MessageWindow::Response warning(Window &parent,
	    const nall::string &text, MessageWindow::Buttons buttons);
	static MessageWindow::Response critical(Window &parent,
	    const nall::string &text, MessageWindow::Buttons buttons);
};

struct pObject {
	Object &object;
	bool locked;

	pObject(Object &object) : object(object), locked(false) { }
	virtual ~pObject(void) { }

	void constructor(void) { }
	void destructor(void) { }
};

struct pOS : public pObject {
	static XlibDisplay *display;
	static Settings *settings;
	static Font defaultFont;

	static void main(void);
	static bool pendingEvents(void);
	static void processEvents(void);
	static void quit(void);

	static void initialize(void);
};

struct pTimer : public pObject {
	Timer &timer;

	void setEnabled(bool enabled);
	void setInterval(unsigned milliseconds);

	pTimer(Timer &timer) : pObject(timer), timer(timer) { }
	void constructor(void);
};

struct pWindow : public pObject {
	Window &window;
	GtkWidget *widget;
	GtkWidget *menuContainer;
	GtkWidget *formContainer;
	GtkWidget *statusContainer;
	GtkWidget *menu;
	GtkWidget *status;
	GtkAllocation lastAllocation;
	bool onSizePending;

	void append(Layout &layout);
	void append(Menu &menu);
	void append(Widget &widget);
	Color backgroundColor(void);
	bool focused(void);
	Geometry frameMargin(void);
	Geometry geometry(void);
	void remove(Layout &layout);
	void remove(Menu &menu);
	void remove(Widget &widget);
	void setBackgroundColor(const Color &color);
	void setFocused(void);
	void setFullScreen(bool fullScreen);
	void setGeometry(const Geometry &geometry);
	void setMenuFont(const nall::string &font);
	void setMenuVisible(bool visible);
	void setResizable(bool resizable);
	void setStatusFont(const nall::string &font);
	void setStatusText(const nall::string &text);
	void setStatusVisible(bool visible);
	void setTitle(const nall::string &text);
	void setVisible(bool visible);
	void setWidgetFont(const nall::string &font);

	pWindow(Window &window) : pObject(window), window(window) { }
	void constructor(void);
	unsigned menuHeight(void);
	unsigned statusHeight(void);
};

struct pAction : public pObject {
	Action &action;
	GtkWidget *widget;

	void setEnabled(bool enabled);
	void setVisible(bool visible);

	pAction(Action &action) : pObject(action), action(action) { }
	void constructor(void);
	virtual void orphan(void);
	nall::string mnemonic(nall::string text);
	virtual void setFont(const nall::string &font);
};

struct pMenu : public pAction {
	Menu &menu;
	GtkWidget *gtkMenu;

	void append(Action &action);
	void remove(Action &action);
	void setImage(const nall::image &image);
	void setText(const nall::string &text);

	pMenu(Menu &menu) : pAction(menu), menu(menu) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
	void setFont(const nall::string &font);
};

struct pSeparator : public pAction {
	Separator &separator;

	pSeparator(Separator &separator) :
	    pAction(separator), separator(separator) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pItem : public pAction {
	Item &item;

	void setImage(const nall::image &image);
	void setText(const nall::string &text);

	pItem(Item &item) : pAction(item), item(item) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pCheckItem : public pAction {
	CheckItem &checkItem;

	bool checked(void);
	void setChecked(bool checked);
	void setText(const nall::string &text);

	pCheckItem(CheckItem &checkItem) :
	    pAction(checkItem), checkItem(checkItem) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pRadioItem : public pAction {
	RadioItem &radioItem;

	bool checked(void);
	void setChecked(void);
	void setGroup(const nall::array<RadioItem&> &group);
	void setText(const nall::string &text);

	pRadioItem(RadioItem &radioItem) :
	    pAction(radioItem), radioItem(radioItem) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pSizable : public pObject {
	Sizable &sizable;
	pSizable(Sizable &sizable) : pObject(sizable), sizable(sizable) { }
};

struct pLayout : public pSizable {
	Layout &layout;
	pLayout(Layout &layout) : pSizable(layout), layout(layout) { }
};

struct pWidget : public pSizable {
	Widget &widget;
	GtkWidget *gtkWidget;

	bool enabled(void);
	virtual Geometry minimumGeometry(void);
	void setEnabled(bool enabled);
	virtual void setFocused(void);
	virtual void setFont(const nall::string &font);
	virtual void setGeometry(const Geometry &geometry);
	void setVisible(bool visible);

	pWidget(Widget &widget) : pSizable(widget), widget(widget) { }
	void constructor(void);
	void destructor(void);
	virtual void orphan(void);
};

struct pButton : public pWidget {
	Button &button;

	Geometry minimumGeometry(void);
	void setImage(const nall::image &image, Orientation orientation);
	void setText(const nall::string &text);

	pButton(Button &button) : pWidget(button), button(button) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pCanvas : public pWidget {
	Canvas &canvas;
	cairo_surface_t *surface;

	void setSize(const Size &size);
	void update(void);

	pCanvas(Canvas &canvas) : pWidget(canvas), canvas(canvas) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pCheckBox : public pWidget {
	CheckBox &checkBox;

	bool checked(void);
	Geometry minimumGeometry(void);
	void setChecked(bool checked);
	void setText(const nall::string &text);

	pCheckBox(CheckBox &checkBox) : pWidget(checkBox), checkBox(checkBox) {}
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pComboBox : public pWidget {
	ComboBox &comboBox;
	unsigned itemCounter;

	void append(const nall::string &text);
	Geometry minimumGeometry(void);
	void reset(void);
	unsigned selection(void);
	void setSelection(unsigned row);

	pComboBox(ComboBox &comboBox) :
	    pWidget(comboBox), comboBox(comboBox) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pHexEdit : public pWidget {
	HexEdit &hexEdit;
	GtkWidget *container;
	GtkWidget *subWidget;
	GtkWidget *scrollBar;
	GtkTextBuffer *textBuffer;
	GtkTextMark *textCursor;

	void setColumns(unsigned columns);
	void setLength(unsigned length);
	void setOffset(unsigned offset);
	void setRows(unsigned rows);
	void update(void);

	pHexEdit(HexEdit &hexEdit) :
	    pWidget(hexEdit), hexEdit(hexEdit) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
	unsigned cursorPosition(void);
	bool keyPress(unsigned scancode);
	void scroll(unsigned position);
	void setCursorPosition(unsigned position);
	void setScroll(void);
	void updateScroll(void);
};

struct pHorizontalScrollBar : public pWidget {
	HorizontalScrollBar &horizontalScrollBar;

	Geometry minimumGeometry(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	pHorizontalScrollBar(HorizontalScrollBar &horizontalScrollBar) :
	    pWidget(horizontalScrollBar),
	    horizontalScrollBar(horizontalScrollBar) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pHorizontalSlider : public pWidget {
	HorizontalSlider &horizontalSlider;

	Geometry minimumGeometry();
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	pHorizontalSlider(HorizontalSlider &horizontalSlider) :
	    pWidget(horizontalSlider),
	    horizontalSlider(horizontalSlider) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pLabel : public pWidget {
	Label &label;

	Geometry minimumGeometry(void);
	void setText(const nall::string &text);

	pLabel(Label &label) : pWidget(label), label(label) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pLineEdit : public pWidget {
	LineEdit &lineEdit;

	Geometry minimumGeometry(void);
	void setEditable(bool editable);
	void setText(const nall::string &text);
	nall::string text(void);

	pLineEdit(LineEdit &lineEdit) :
	    pWidget(lineEdit), lineEdit(lineEdit) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pListView : public pWidget {
	ListView &listView;
	GtkWidget *subWidget;
	GtkListStore *store;
	struct GtkColumn {
		GtkCellRenderer *renderer;
		GtkTreeViewColumn *column;
		GtkWidget *label;
	};

	nall::linear_vector<GtkColumn> column;

	void append(const nall::lstring &text);
	void autoSizeColumns(void);
	bool checked(unsigned row);
	void modify(unsigned row, const nall::lstring &text);
	void reset(void);
	bool selected(void);
	unsigned selection(void);
	void setCheckable(bool checkable);
	void setChecked(unsigned row, bool checked);
	void setHeaderText(const nall::lstring &text);
	void setHeaderVisible(bool visible);
	void setSelected(bool selected);
	void setSelection(unsigned row);

	pListView(ListView &listView) :
	    pWidget(listView), listView(listView) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
	void setFocused(void);
	void setFont(const nall::string &font);
};

struct pProgressBar : public pWidget {
	ProgressBar &progressBar;

	Geometry minimumGeometry(void);
	void setPosition(unsigned position);

	pProgressBar(ProgressBar &progressBar) :
	    pWidget(progressBar), progressBar(progressBar) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pRadioBox : public pWidget {
	RadioBox &radioBox;

	bool checked(void);
	Geometry minimumGeometry(void);
	void setChecked(void);
	void setGroup(const nall::array<RadioBox&> &group);
	void setText(const nall::string &text);

	pRadioBox(RadioBox &radioBox) :
	    pWidget(radioBox), radioBox(radioBox) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pTextEdit : public pWidget {
	TextEdit &textEdit;
	GtkWidget *subWidget;
	GtkTextBuffer *textBuffer;

	void setCursorPosition(unsigned position);
	void setEditable(bool editable);
	void setText(const nall::string &text);
	void setWordWrap(bool wordWrap);
	nall::string text(void);

	pTextEdit(TextEdit &textEdit) :
	    pWidget(textEdit), textEdit(textEdit) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pVerticalScrollBar : public pWidget {
	VerticalScrollBar &verticalScrollBar;

	Geometry minimumGeometry(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	pVerticalScrollBar(VerticalScrollBar &verticalScrollBar) :
	    pWidget(verticalScrollBar),
	    verticalScrollBar(verticalScrollBar) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pVerticalSlider : public pWidget {
	VerticalSlider &verticalSlider;

	Geometry minimumGeometry(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	pVerticalSlider(VerticalSlider &verticalSlider) :
	    pWidget(verticalSlider), verticalSlider(verticalSlider) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

struct pViewport : public pWidget {
	Viewport &viewport;

	uintptr_t handle(void);

	pViewport(Viewport &viewport) :
	    pWidget(viewport), viewport(viewport) { }
	void constructor(void);
	void destructor(void);
	void orphan(void);
};

static inline GtkImage* CreateImage(const nall::image &image, bool menuIcon)
{
	nall::image gdkImage = image;
	gdkImage.transform(0, 32, 255u << 24, 255u << 0,
	    255u << 8, 255u << 16);
	if (menuIcon) {
		gdkImage.scale(16, 16, nall::Interpolation::Linear);
	}

	GdkPixbuf *pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, true, 8,
	    gdkImage.width, gdkImage.height);
	memcpy(gdk_pixbuf_get_pixels(pixbuf), gdkImage.data,
	    gdkImage.width * gdkImage.height * 4);
	GtkImage *gtkImage = (GtkImage*)gtk_image_new_from_pixbuf(pixbuf);
	g_object_unref(pixbuf);

	return gtkImage;
}

static inline Keyboard::Keycode Keysym(unsigned keysym)
{
	switch (keysym) {
	case GDK_Escape:
		return Keyboard::Keycode::Escape;
	case GDK_F1:
		return Keyboard::Keycode::F1;
	case GDK_F2:
		return Keyboard::Keycode::F2;
	case GDK_F3:
		return Keyboard::Keycode::F3;
	case GDK_F4:
		return Keyboard::Keycode::F4;
	case GDK_F5:
		return Keyboard::Keycode::F5;
	case GDK_F6:
		return Keyboard::Keycode::F6;
	case GDK_F7:
		return Keyboard::Keycode::F7;
	case GDK_F8:
		return Keyboard::Keycode::F8;
	case GDK_F9:
		return Keyboard::Keycode::F9;
	case GDK_F10:
		return Keyboard::Keycode::F10;
	case GDK_F11:
		return Keyboard::Keycode::F11;
	case GDK_F12:
		return Keyboard::Keycode::F12;

	case GDK_Print:
		return Keyboard::Keycode::PrintScreen;

	//Keyboard::Keycode::SysRq
	case GDK_Scroll_Lock:
		return Keyboard::Keycode::ScrollLock;

	//Keyboard::Keycode::Break
	case GDK_Pause:
		return Keyboard::Keycode::Pause;

	case GDK_Insert:
		return Keyboard::Keycode::Insert;
	case GDK_Delete:
		return Keyboard::Keycode::Delete;
	case GDK_Home:
		return Keyboard::Keycode::Home;
	case GDK_End:
		return Keyboard::Keycode::End;
	case GDK_Prior:
		return Keyboard::Keycode::PageUp;
	case GDK_Next:
		return Keyboard::Keycode::PageDown;

	case GDK_Up:
		return Keyboard::Keycode::Up;
	case GDK_Down:
		return Keyboard::Keycode::Down;
	case GDK_Left:
		return Keyboard::Keycode::Left;
	case GDK_Right:
		return Keyboard::Keycode::Right;

	case GDK_grave:
		return Keyboard::Keycode::Grave;
	case GDK_1:
		return Keyboard::Keycode::Number1;
	case GDK_2:
		return Keyboard::Keycode::Number2;
	case GDK_3:
		return Keyboard::Keycode::Number3;
	case GDK_4:
		return Keyboard::Keycode::Number4;
	case GDK_5:
		return Keyboard::Keycode::Number5;
	case GDK_6:
		return Keyboard::Keycode::Number6;
	case GDK_7:
		return Keyboard::Keycode::Number7;
	case GDK_8:
		return Keyboard::Keycode::Number8;
	case GDK_9:
		return Keyboard::Keycode::Number9;
	case GDK_0:
		return Keyboard::Keycode::Number0;
	case GDK_minus:
		return Keyboard::Keycode::Minus;
	case GDK_equal:
		return Keyboard::Keycode::Equal;
	case GDK_BackSpace:
		return Keyboard::Keycode::Backspace;

	case GDK_asciitilde:
		return Keyboard::Keycode::Tilde;
	case GDK_exclam:
		return Keyboard::Keycode::Exclamation;
	case GDK_at:
		return Keyboard::Keycode::At;
	case GDK_numbersign:
		return Keyboard::Keycode::Pound;
	case GDK_dollar:
		return Keyboard::Keycode::Dollar;
	case GDK_percent:
		return Keyboard::Keycode::Percent;
	case GDK_asciicircum:
		return Keyboard::Keycode::Power;
	case GDK_ampersand:
		return Keyboard::Keycode::Ampersand;
	case GDK_asterisk:
		return Keyboard::Keycode::Asterisk;
	case GDK_parenleft:
		return Keyboard::Keycode::ParenthesisLeft;
	case GDK_parenright:
		return Keyboard::Keycode::ParenthesisRight;
	case GDK_underscore:
		return Keyboard::Keycode::Underscore;
	case GDK_plus:
		return Keyboard::Keycode::Plus;

	case GDK_bracketleft:
		return Keyboard::Keycode::BracketLeft;
	case GDK_bracketright:
		return Keyboard::Keycode::BracketRight;
	case GDK_backslash:
		return Keyboard::Keycode::Backslash;
	case GDK_semicolon:
		return Keyboard::Keycode::Semicolon;
	case GDK_apostrophe:
		return Keyboard::Keycode::Apostrophe;
	case GDK_comma:
		return Keyboard::Keycode::Comma;
	case GDK_period:
		return Keyboard::Keycode::Period;
	case GDK_slash:
		return Keyboard::Keycode::Slash;

	case GDK_braceleft:
		return Keyboard::Keycode::BraceLeft;
	case GDK_braceright:
		return Keyboard::Keycode::BraceRight;
	case GDK_bar:
		return Keyboard::Keycode::Pipe;
	case GDK_colon:
		return Keyboard::Keycode::Colon;
	case GDK_quotedbl:
		return Keyboard::Keycode::Quote;
	case GDK_less:
		return Keyboard::Keycode::CaretLeft;
	case GDK_greater:
		return Keyboard::Keycode::CaretRight;
	case GDK_question:
		return Keyboard::Keycode::Question;

	case GDK_Tab:
		return Keyboard::Keycode::Tab;
	case GDK_Caps_Lock:
		return Keyboard::Keycode::CapsLock;
	case GDK_Return:
		return Keyboard::Keycode::Return;
	case GDK_Shift_L:
		return Keyboard::Keycode::ShiftLeft;
	case GDK_Shift_R:
		return Keyboard::Keycode::ShiftRight;
	case GDK_Control_L:
		return Keyboard::Keycode::ControlLeft;
	case GDK_Control_R:
		return Keyboard::Keycode::ControlRight;
	case GDK_Super_L:
		return Keyboard::Keycode::SuperLeft;
	case GDK_Super_R:
		return Keyboard::Keycode::SuperRight;
	case GDK_Alt_L:
		return Keyboard::Keycode::AltLeft;
	case GDK_Alt_R:
		return Keyboard::Keycode::AltRight;
	case GDK_space:
		return Keyboard::Keycode::Space;
	case GDK_Menu:
		return Keyboard::Keycode::Menu;

	case GDK_A:
		return Keyboard::Keycode::A;
	case GDK_B:
		return Keyboard::Keycode::B;
	case GDK_C:
		return Keyboard::Keycode::C;
	case GDK_D:
		return Keyboard::Keycode::D;
	case GDK_E:
		return Keyboard::Keycode::E;
	case GDK_F:
		return Keyboard::Keycode::F;
	case GDK_G:
		return Keyboard::Keycode::G;
	case GDK_H:
		return Keyboard::Keycode::H;
	case GDK_I:
		return Keyboard::Keycode::I;
	case GDK_J:
		return Keyboard::Keycode::J;
	case GDK_K:
		return Keyboard::Keycode::K;
	case GDK_L:
		return Keyboard::Keycode::L;
	case GDK_M:
		return Keyboard::Keycode::M;
	case GDK_N:
		return Keyboard::Keycode::N;
	case GDK_O:
		return Keyboard::Keycode::O;
	case GDK_P:
		return Keyboard::Keycode::P;
	case GDK_Q:
		return Keyboard::Keycode::Q;
	case GDK_R:
		return Keyboard::Keycode::R;
	case GDK_S:
		return Keyboard::Keycode::S;
	case GDK_T:
		return Keyboard::Keycode::T;
	case GDK_U:
		return Keyboard::Keycode::U;
	case GDK_V:
		return Keyboard::Keycode::V;
	case GDK_W:
		return Keyboard::Keycode::W;
	case GDK_X:
		return Keyboard::Keycode::X;
	case GDK_Y:
		return Keyboard::Keycode::Y;
	case GDK_Z:
		return Keyboard::Keycode::Z;

	case GDK_a:
		return Keyboard::Keycode::a;
	case GDK_b:
		return Keyboard::Keycode::b;
	case GDK_c:
		return Keyboard::Keycode::c;
	case GDK_d:
		return Keyboard::Keycode::d;
	case GDK_e:
		return Keyboard::Keycode::e;
	case GDK_f:
		return Keyboard::Keycode::f;
	case GDK_g:
		return Keyboard::Keycode::g;
	case GDK_h:
		return Keyboard::Keycode::h;
	case GDK_i:
		return Keyboard::Keycode::i;
	case GDK_j:
		return Keyboard::Keycode::j;
	case GDK_k:
		return Keyboard::Keycode::k;
	case GDK_l:
		return Keyboard::Keycode::l;
	case GDK_m:
		return Keyboard::Keycode::m;
	case GDK_n:
		return Keyboard::Keycode::n;
	case GDK_o:
		return Keyboard::Keycode::o;
	case GDK_p:
		return Keyboard::Keycode::p;
	case GDK_q:
		return Keyboard::Keycode::q;
	case GDK_r:
		return Keyboard::Keycode::r;
	case GDK_s:
		return Keyboard::Keycode::s;
	case GDK_t:
		return Keyboard::Keycode::t;
	case GDK_u:
		return Keyboard::Keycode::u;
	case GDK_v:
		return Keyboard::Keycode::v;
	case GDK_w:
		return Keyboard::Keycode::w;
	case GDK_x:
		return Keyboard::Keycode::x;
	case GDK_y:
		return Keyboard::Keycode::y;
	case GDK_z:
		return Keyboard::Keycode::z;

	case GDK_Num_Lock:
		return Keyboard::Keycode::NumLock;
	case GDK_KP_Divide:
		return Keyboard::Keycode::Divide;
	case GDK_KP_Multiply:
		return Keyboard::Keycode::Multiply;
	case GDK_KP_Subtract:
		return Keyboard::Keycode::Subtract;
	case GDK_KP_Add:
		return Keyboard::Keycode::Add;
	case GDK_KP_Enter:
		return Keyboard::Keycode::Enter;
	case GDK_KP_Decimal:
		return Keyboard::Keycode::Point;

	case GDK_KP_1:
		return Keyboard::Keycode::Keypad1;
	case GDK_KP_2:
		return Keyboard::Keycode::Keypad2;
	case GDK_KP_3:
		return Keyboard::Keycode::Keypad3;
	case GDK_KP_4:
		return Keyboard::Keycode::Keypad4;
	case GDK_KP_5:
		return Keyboard::Keycode::Keypad5;
	case GDK_KP_6:
		return Keyboard::Keycode::Keypad6;
	case GDK_KP_7:
		return Keyboard::Keycode::Keypad7;
	case GDK_KP_8:
		return Keyboard::Keycode::Keypad8;
	case GDK_KP_9:
		return Keyboard::Keycode::Keypad9;
	case GDK_KP_0:
		return Keyboard::Keycode::Keypad0;

	case GDK_KP_Home:
		return Keyboard::Keycode::KeypadHome;
	case GDK_KP_End:
		return Keyboard::Keycode::KeypadEnd;
	case GDK_KP_Page_Up:
		return Keyboard::Keycode::KeypadPageUp;
	case GDK_KP_Page_Down:
		return Keyboard::Keycode::KeypadPageDown;
	case GDK_KP_Up:
		return Keyboard::Keycode::KeypadUp;
	case GDK_KP_Down:
		return Keyboard::Keycode::KeypadDown;
	case GDK_KP_Left:
		return Keyboard::Keycode::KeypadLeft;
	case GDK_KP_Right:
		return Keyboard::Keycode::KeypadRight;
	case GDK_KP_Begin:
		return Keyboard::Keycode::KeypadCenter;
	case GDK_KP_Insert:
		return Keyboard::Keycode::KeypadInsert;
	case GDK_KP_Delete:
		return Keyboard::Keycode::KeypadDelete;
	}

	return Keyboard::Keycode::None;
}

} /* namespace phoenix */

#endif
