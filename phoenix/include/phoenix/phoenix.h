#ifndef PHOENIX_PHOENIX_H
#define PHOENIX_PHOENIX_H

#include <nall/config.hpp>
#include <nall/utility.hpp>
#include <nall/function.hpp>
#include <nall/image.hpp>
#include <nall/map.hpp>
#include <nall/string.hpp>

namespace phoenix {

class Font;
class Window;
class Menu;
class Sizable;
class Layout;
class Widget;

struct pFont;
struct pObject;
struct pOS;
struct pTimer;
struct pWindow;
struct pAction;
struct pMenu;
struct pSeparator;
struct pItem;
struct pCheckItem;
struct pRadioItem;
struct pSizable;
struct pLayout;
struct pWidget;
struct pButton;
struct pCanvas;
struct pCheckBox;
struct pComboBox;
struct pHexEdit;
struct pHorizontalScrollBar;
struct pHorizontalSlider;
struct pLabel;
struct pLineEdit;
struct pListView;
struct pProgressBar;
struct pRadioBox;
struct pTextEdit;
struct pVerticalScrollBar;
struct pVerticalSlider;
struct pViewport;

enum : unsigned {
	MaximumSize = ~0u,
	MinimumSize =  0u
};

class Color {
public:
	uint8_t red, green, blue, alpha;
	uint32_t rgb(void) const;
	uint32_t rgba(void) const;
	inline Color(void) : red(0), green(0), blue(0), alpha(255) { }
	inline Color(uint8_t red, uint8_t green, uint8_t blue,
	    uint8_t alpha = 255) : red(red), green(green), blue(blue),
	    alpha(alpha) { }
};

class Position {
public:
	signed x, y;
	inline Position(void) : x(0), y(0) { }
	template<typename X, typename Y>
	inline Position(X x, Y y) : x(x), y(y) { }
};

class Size {
public:
	unsigned width, height;
	inline Size(void) : width(0), height(0) { }
	template<typename W, typename H>
	inline Size(W width, H height) : width(width), height(height) { }
};

class Geometry {
public:
	signed x, y;
	unsigned width, height;
	Position position(void) const;
	Size size(void) const;
	nall::string text(void) const;
	inline Geometry(void) : x(0), y(0), width(0), height(0) { }
	inline Geometry(const Position& position, const Size& size) :
	    x(position.x), y(position.y),
	    width(size.width), height(size.height) { }
	template<typename X, typename Y, typename W, typename H>
	inline Geometry(X x, Y y, W width, H height) :
	    x(x), y(y), width(width), height(height) { }
	Geometry(const nall::string &text);
	virtual ~Geometry(void) { }
};

enum class Orientation : unsigned {
	Horizontal,
	Vertical
};

class Font {
public:
	nall::string description;
	Geometry geometry(const nall::string &text);
	Font(const nall::string &description = "");
	virtual ~Font(void) { }
};

class Desktop {
public:
	static Size size(void);
	static Geometry workspace(void);
	Desktop(void) = delete;
};

class Keyboard {
public:
	/*
	* Each code referes to a physical key.  Names are taken assuming:
	* NumLock on, CapsLock off, Shift off.  Layout uses US-104 keyboard.
	*/
	enum class Scancode : unsigned {
		None, Escape, F1, F2, F3, F4, F5, F6, F7, F8, F9,
		F10, F11, F12, PrintScreen, ScrollLock, Pause,
		Insert, Delete, Home, End, PageUp, PageDown,
		Up, Down, Left, Right,

		Grave, Number1, Number2, Number3, Number4, Number5, Number6,
		Number7, Number8, Number9, Number0, Minus, Equal, Backspace,
		BracketLeft, BracketRight, Backslash, Semicolon, Apostrophe,
		Comma, Period, Slash, Tab, CapsLock, Return, ShiftLeft,
		ShiftRight, ControlLeft, ControlRight, SuperLeft, SuperRight,
		AltLeft, AltRight, Space, Menu,

		A, B, C, D, E, F, G, H, I, J, K, L, M,
		N, O, P, Q, R, S, T, U, V, W, X, Y, Z,

		NumLock, Divide, Multiply, Subtract, Add, Enter, Point,
		Keypad1, Keypad2, Keypad3, Keypad4, Keypad5, Keypad6,
		Keypad7, Keypad8, Keypad9, Keypad0,

		Limit,
	};

	/*
	* Each enum refers to a translated scancode (eg Shift+1 = !)
	*/
	enum class Keycode : unsigned {
		None, Escape, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10,
		F11, F12, PrintScreen, SysRq, ScrollLock, Pause, Break,
		Insert, Delete, Home, End, PageUp, PageDown,
		Up, Down, Left, Right,

		Grave, Number1, Number2, Number3, Number4, Number5, Number6,
		Number7, Number8, Number9, Number0, Minus, Equal, Backspace,
		Tilde, Exclamation, At, Pound, Dollar, Percent, Power,
		Ampersand, Asterisk, ParenthesisLeft, ParenthesisRight,
		Underscore, Plus, BracketLeft, BracketRight, Backslash,
		Semicolon, Apostrophe, Comma, Period, Slash, BraceLeft,
		BraceRight, Pipe, Colon, Quote, CaretLeft, CaretRight,
		Question, Tab, CapsLock, Return, ShiftLeft, ShiftRight,
		ControlLeft, ControlRight, SuperLeft, SuperRight, AltLeft,
		AltRight, Space, Menu,

		A, B, C, D, E, F, G, H, I, J, K, L, M,
		N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
		a, b, c, d, e, f, g, h, i, j, k, l, m,
		n, o, p, q, r, s, t, u, v, w, x, y, z,

		NumLock, Divide, Multiply, Subtract, Add, Enter, Point,
		Keypad1, Keypad2, Keypad3, Keypad4, Keypad5, Keypad6,
		Keypad7, Keypad8, Keypad9, Keypad0, KeypadInsert,
		KeypadDelete, KeypadHome, KeypadEnd, KeypadPageUp,
		KeypadPageDown, KeypadUp, KeypadDown, KeypadLeft,
		KeypadRight, KeypadCenter, Limit,
	};

	static bool pressed(Scancode scancode);
	static bool released(Scancode scancode);
	static nall::array<bool> state();
	Keyboard(void) = delete;
};

class Mouse {
public:
	enum class Button : unsigned {
		Left,
		Middle,
		Right
	};

	static Position position(void);
	static bool pressed(Button);
	static bool released(Button);
	Mouse(void) = delete;
};

class DialogWindow {
public:
	template<typename... Args>
	static nall::string
	fileOpen(Window &parent, const nall::string &path, const Args&... args)
	{
		return fileOpen_(parent, path, { args... });
	}

	template<typename... Args>
	static nall::string
	fileSave(Window &parent, const nall::string &path, const Args&... args)
	{
		return fileSave_(parent, path, { args... });
	}

	static nall::string
	folderSelect(Window &parent, const nall::string &path);

	DialogWindow(void) = delete;
	virtual ~DialogWindow(void) { }
private:
	static nall::string fileOpen_(Window &parent, const nall::string &path,
	    const nall::lstring& filter);
	static nall::string fileSave_(Window &parent, const nall::string &path,
	    const nall::lstring& filter);
};

class MessageWindow {
public:
	enum class Buttons : unsigned {
		Ok,
		OkCancel,
		YesNo,
	};

	enum class Response : unsigned {
		Ok,
		Cancel,
		Yes,
		No,
	};

	static Response information(Window &parent, const nall::string &text,
	    Buttons = Buttons::Ok);
	static Response question(Window &parent, const nall::string &text,
	    Buttons = Buttons::YesNo);
	static Response warning(Window &parent, const nall::string &text,
	    Buttons = Buttons::Ok);
	static Response critical(Window &parent, const nall::string &text,
	    Buttons = Buttons::Ok);

	MessageWindow(void) = delete;
};

class Object {
public:
	Object(pObject &p);
	Object& operator=(const Object&) = delete;
	Object(const Object&) = delete;
	virtual ~Object(void);
	pObject &p;
};

class OS : public Object {
public:
	static void main(void);
	static bool pendingEvents(void);
	static void processEvents(void);
	static void quit(void);

	OS(void);
	static void initialize(void);
};

class Timer : private nall::base_from_member<pTimer&>, public Object {
public:
	nall::function<void ()> onTimeout;

	void setEnabled(bool enabled = true);
	void setInterval(unsigned milliseconds);

	Timer(void);
	~Timer(void);

	/* Timer::State is declared in phoenix.cpp for the time being. */
	struct State;
	State &state;

	pTimer &p;
};

class Window : private nall::base_from_member<pWindow&>, public Object {
public:
	static Window None;
	nall::function<void ()> onClose;
	nall::function<void (Keyboard::Keycode)> onKeyPress;
	nall::function<void (Keyboard::Keycode)> onKeyRelease;
	nall::function<void ()> onMove;
	nall::function<void ()> onSize;

	inline void append(void) { }
	inline void remove(void) { }
	template<typename T, typename... Args>
	void append(T &arg, Args&... args) { append_(arg); append(args...); }
	template<typename T, typename... Args>
	void remove(T &arg, Args&... args) { remove_(arg); remove(args...); }

	void append_(Layout &layout);
	void append_(Menu &menu);
	void append_(Widget &widget);
	Color backgroundColor(void);
	Geometry frameGeometry(void);
	Geometry frameMargin(void);
	bool focused(void);
	bool fullScreen(void);
	Geometry geometry(void);
	void ignore(void);
	void remove_(Layout &layout);
	void remove_(Menu &menu);
	void remove_(Widget &widget);
	void setBackgroundColor(const Color &color);
	void setFrameGeometry(const Geometry &geometry);
	void setFocused(void);
	void setFullScreen(bool fullscreen = true);
	void setGeometry(const Geometry &geometry);
	void setMenuFont(const nall::string &font);
	void setMenuVisible(bool visible = true);
	void setResizable(bool resizable = true);
	void setStatusFont(const nall::string &font);
	void setStatusText(const nall::string &text);
	void setStatusVisible(bool visible = true);
	void setTitle(const nall::string &text);
	void setVisible(bool visible = true);
	void setWidgetFont(const nall::string &font);
	nall::string statusText(void);
	void synchronizeLayout(void);
	bool visible(void);

	Window(void);
	~Window(void);
	struct State;
	State &state;
	pWindow &p;
};

struct Settings : public nall::configuration {
	nall::bidirectional_map<Keyboard::Scancode, unsigned> keymap;

	unsigned frameGeometryX;
	unsigned frameGeometryY;
	unsigned frameGeometryWidth;
	unsigned frameGeometryHeight;
	unsigned menuGeometryHeight;
	unsigned statusGeometryHeight;
	unsigned windowBackgroundColor;

	void load(void);
	void save(void);
	Settings(void);
	virtual ~Settings(void) { }
};

class Action : public Object {
public:
	bool enabled(void);
	void setEnabled(bool enabled = true);
	void setVisible(bool visible = true);
	bool visible(void);

	Action(pAction &p);
	~Action(void);

	struct State;
	State &state;
	pAction &p;
};

class Menu : private nall::base_from_member<pMenu&>, public Action {
public:
	template<typename... Args>
	void append(Args&... args) { append({args... }); }
	template<typename... Args>
	void remove(Args&... args) { remove({args... }); }

	void append(const nall::array<Action&> &list);
	void remove(const nall::array<Action&> &list);
	void setImage(const nall::image &image);
	void setText(const nall::string &text);

	Menu(void);
	~Menu(void);
	struct State;
	State &state;
	pMenu &p;
};

class Separator : private nall::base_from_member<pSeparator&>, public Action {
public:
	Separator(void);
	~Separator(void);
	pSeparator &p;
};

class Item : private nall::base_from_member<pItem&>, public Action {
public:
	nall::function<void ()> onActivate;

	void setImage(const nall::image &image);
	void setText(const nall::string &text);

	Item(void);
	~Item(void);

	struct State;
	State &state;

	pItem &p;
};

class CheckItem : private nall::base_from_member<pCheckItem&>, public Action {
public:
	nall::function<void ()> onToggle;

	bool checked(void);
	void setChecked(bool checked = true);
	void setText(const nall::string &text);

	CheckItem(void);
	~CheckItem(void);
	struct State;
	State &state;

	pCheckItem &p;
};

class RadioItem : private nall::base_from_member<pRadioItem&>, public Action {
public:
	template<typename... Args>
	static void group(Args&... args) { group({ args... }); }
	static void group(const nall::array<RadioItem&> &list);

	nall::function<void ()> onActivate;

	bool checked(void);
	void setChecked(void);
	void setText(const nall::string &text);

	RadioItem(void);
	~RadioItem(void);
	struct State;
	State &state;
	pRadioItem &p;
};

class Sizable : public Object {
public:
	virtual bool enabled(void) = 0;
	Layout* layout(void);
	virtual Geometry minimumGeometry(void) = 0;
	virtual void setEnabled(bool enabled = true) = 0;
	virtual void setGeometry(const Geometry &geometry) = 0;
	virtual void setVisible(bool visible = true) = 0;
	virtual bool visible(void) = 0;
	Window* window(void);

	Sizable(pSizable &p);
	~Sizable(void);
	struct State;
	State &state;
	pSizable &p;
};

class Layout : private nall::base_from_member<pLayout&>, public Sizable {
public:
	virtual void append(Sizable &sizable);
	virtual void remove(Sizable &sizable);
	virtual void reset(void) { }
	virtual void synchronizeLayout(void) = 0;

	Layout(void);
	Layout(pLayout &p);
	~Layout(void);
	struct State;
	State &state;
	pLayout &p;
};

class Widget : private nall::base_from_member<pWidget&>, public Sizable {
public:
	bool enabled(void);
	nall::string font(void);
	Geometry geometry(void);
	Geometry minimumGeometry(void);
	void setEnabled(bool enabled = true);
	void setFocused(void);
	void setFont(const nall::string &font);
	void setGeometry(const Geometry &geometry);
	void setVisible(bool visible = true);
	bool visible(void);

	Widget(void);
	Widget(pWidget &p);
	~Widget(void);
	struct State;
	State &state;
	pWidget &p;
};

class Button : private nall::base_from_member<pButton&>, public Widget {
public:
	nall::function<void ()> onActivate;
	void setImage(const nall::image &image,
	    Orientation = Orientation::Horizontal);
	void setText(const nall::string &text);

	Button(void);
	~Button(void);
	struct State;
	State &state;
	pButton &p;
};

class Canvas : private nall::base_from_member<pCanvas&>, public Widget {
public:
	nall::function<void ()> onMouseLeave;
	nall::function<void (Position)> onMouseMove;
	nall::function<void (Mouse::Button)> onMousePress;
	nall::function<void (Mouse::Button)> onMouseRelease;

	uint32_t* data(void);
	bool setImage(const nall::image &image);
	void setSize(const Size &size);
	Size size(void);
	void update(void);

	Canvas(void);
	~Canvas(void);
	struct State;
	State &state;
	pCanvas &p;
};

class CheckBox : private nall::base_from_member<pCheckBox&>, public Widget {
public:
	nall::function<void ()> onToggle;
	bool checked(void);
	void setChecked(bool checked = true);
	void setText(const nall::string &text);

	CheckBox(void);
	~CheckBox(void);
	struct State;
	State &state;
	pCheckBox &p;
};

class ComboBox : private nall::base_from_member<pComboBox&>, public Widget {
public:
	nall::function<void ()> onChange;
	template<typename... Args>
	void append(const Args&... args) { append_({ args... }); }

	void append_(const nall::lstring &list);
	void reset(void);
	unsigned selection(void);
	void setSelection(unsigned row);

	ComboBox(void);
	~ComboBox(void);
	struct State;
	State &state;
	pComboBox &p;
};

class HexEdit : private nall::base_from_member<pHexEdit&>, public Widget {
public:
	nall::function<uint8_t (unsigned)> onRead;
	nall::function<void (unsigned, uint8_t)> onWrite;

	void setColumns(unsigned columns);
	void setLength(unsigned length);
	void setOffset(unsigned offset);
	void setRows(unsigned rows);
	void update(void);

	HexEdit(void);
	~HexEdit(void);
	struct State;
	State &state;
	pHexEdit &p;
};

class HorizontalScrollBar :
    private nall::base_from_member<pHorizontalScrollBar&>,
    public Widget {
public:
	nall::function<void ()> onChange;

	unsigned length(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	HorizontalScrollBar(void);
	~HorizontalScrollBar(void);
	struct State;
	State &state;
	pHorizontalScrollBar &p;
};

class HorizontalSlider : private nall::base_from_member<pHorizontalSlider&>,
    public Widget {
public:
	nall::function<void ()> onChange;

	unsigned length(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	HorizontalSlider(void);
	~HorizontalSlider(void);
	struct State;
	State &state;
	pHorizontalSlider &p;
};

class Label : private nall::base_from_member<pLabel&>, public Widget {
public:
	void setText(const nall::string &text);
	Label(void);
	~Label(void);
	struct State;
	State &state;
	pLabel &p;
};

class LineEdit : private nall::base_from_member<pLineEdit&>, public Widget {
public:
	nall::function<void ()> onActivate;
	nall::function<void ()> onChange;

	void setEditable(bool editable = true);
	void setText(const nall::string &text);
	nall::string text(void);

	LineEdit(void);
	~LineEdit(void);
	struct State;
	State &state;
	pLineEdit &p;
};

class ListView : private nall::base_from_member<pListView&>, public Widget {
public:
	nall::function<void ()> onActivate;
	nall::function<void ()> onChange;
	nall::function<void (unsigned)> onToggle;

	template<typename... Args>
	void append(const Args&... args) { append_({ args... }); }
	template<typename... Args>
	void modify(unsigned row, const Args&... args)
	    { modify_(row, { args... }); }

	template<typename... Args>
	void setHeaderText(const Args&... args)
	    { setHeaderText_({ args... }); }

	void append_(const nall::lstring &list);
	void autoSizeColumns(void);
	bool checked(unsigned row);
	void modify_(unsigned row, const nall::lstring &list);
	void reset(void);
	bool selected(void);
	unsigned selection(void);
	void setCheckable(bool checkable = true);
	void setChecked(unsigned row, bool checked = true);
	void setHeaderText_(const nall::lstring &list);
	void setHeaderVisible(bool visible = true);
	void setSelected(bool selected = true);
	void setSelection(unsigned row);

	ListView(void);
	~ListView(void);
	struct State;
	State &state;
	pListView &p;
};

class ProgressBar : private nall::base_from_member<pProgressBar&>,
    public Widget {
public:
	void setPosition(unsigned position);

	ProgressBar(void);
	~ProgressBar(void);
	struct State;
	State &state;
	pProgressBar &p;
};

class RadioBox : private nall::base_from_member<pRadioBox&>, public Widget {
public:
	template<typename... Args>
	static void group(Args&... args) { group({ args... }); }
	static void group(const nall::array<RadioBox&> &list);

	nall::function<void ()> onActivate;

	bool checked(void);
	void setChecked(void);
	void setText(const nall::string &text);

	RadioBox(void);
	~RadioBox(void);

	struct State;
	State &state;
	pRadioBox &p;
};

class TextEdit : private nall::base_from_member<pTextEdit&>, public Widget {
public:
	nall::function<void ()> onChange;

	void setCursorPosition(unsigned position);
	void setEditable(bool editable = true);
	void setText(const nall::string &text);
	void setWordWrap(bool wordWrap = true);
	nall::string text(void);

	TextEdit(void);
	~TextEdit(void);
	struct State;
	State &state;
	pTextEdit &p;
};

class VerticalScrollBar :
    private nall::base_from_member<pVerticalScrollBar&>,
    public Widget {
public:
	nall::function<void ()> onChange;

	unsigned length(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	VerticalScrollBar(void);
	~VerticalScrollBar(void);
	struct State;
	State &state;
	pVerticalScrollBar &p;
};

class VerticalSlider : private nall::base_from_member<pVerticalSlider&>,
    public Widget {
public:
	nall::function<void ()> onChange;

	unsigned length(void);
	unsigned position(void);
	void setLength(unsigned length);
	void setPosition(unsigned position);

	VerticalSlider(void);
	~VerticalSlider(void);
	struct State;
	State &state;
	pVerticalSlider &p;
};

class Viewport : private nall::base_from_member<pViewport&>, public Widget {
public:
	nall::function<void ()> onMouseLeave;
	nall::function<void (Position)> onMouseMove;
	nall::function<void (Mouse::Button)> onMousePress;
	nall::function<void (Mouse::Button)> onMouseRelease;

	uintptr_t handle(void);

	Viewport(void);
	~Viewport(void);
	pViewport &p;
};

class FixedLayout : public Layout {
public:
	void append(Sizable &sizable, const Geometry &geometry);
	void append(Sizable &sizable);
	bool enabled(void);
	Geometry minimumGeometry(void);
	void remove(Sizable &sizable);
	void reset(void);
	void setEnabled(bool enabled = true);
	void setGeometry(const Geometry &geometry);
	void setVisible(bool visible = true);
	void synchronizeLayout(void);
	bool visible(void);
	FixedLayout(void);
	~FixedLayout(void);

/* TODO: Maybe private eventually? */
	struct State {
		bool enabled;
		bool visible;
	} state;

	struct Children {
		Sizable *sizable;
		Geometry geometry;
	};

	nall::vector<Children> children;
};

class HorizontalLayout : public Layout {
public:
	void append(Sizable &sizable, const Size &size,
	    unsigned spacing = 0);
	void append(Sizable &sizable);
	bool enabled(void);
	Geometry minimumGeometry(void);
	void remove(Sizable &sizable);
	void reset(void);
	void setAlignment(double alignment);
	void setEnabled(bool enabled = true);
	void setGeometry(const Geometry &geometry);
	void setMargin(unsigned margin);
	void setVisible(bool visible = true);
	void synchronizeLayout(void);
	bool visible(void);
	HorizontalLayout(void);
	~HorizontalLayout(void);

/* TODO: Maybe private eventually? */
	struct State {
		double alignment;
		bool enabled;
		unsigned margin;
		bool visible;
	} state;

	struct Children {
		Sizable *sizable;
		unsigned width, height, spacing;
	};

	nall::vector<Children> children;
};

class VerticalLayout : public Layout {
public:
	void append(Sizable &sizable, const Size &size,
	    unsigned spacing = 0);
	void append(Sizable &sizable);
	bool enabled(void);
	Geometry minimumGeometry(void);
	void remove(Sizable &sizable);
	void reset(void);
	void setAlignment(double alignment);
	void setEnabled(bool enabled = true);
	void setGeometry(const Geometry &geometry);
	void setMargin(unsigned margin);
	void setVisible(bool visible = true);
	void synchronizeLayout(void);
	bool visible(void);
	VerticalLayout(void);
	~VerticalLayout(void);

/* TODO: Maybe private eventually? */
	struct State {
		double alignment;
		bool enabled;
		unsigned margin;
		bool visible;
	} state;

	struct Children {
		Sizable *sizable;
		unsigned width, height, spacing;
	};

	nall::vector<Children> children;
};

/* (X)::State is transplanted from phoenix-qt4.cpp */
struct Timer::State {
	bool enabled;
	unsigned milliseconds;

	State(void)
	{
		enabled = false;
		milliseconds = 0;
	}
};

struct Window::State {
	bool backgroundColorOverride;
	Color backgroundColor;
	bool fullScreen;
	Geometry geometry;
	bool ignore;
	nall::array<Layout&> layout;
	nall::array<Menu&> menu;
	nall::string menuFont;
	bool menuVisible;
	bool resizable;
	nall::string statusFont;
	nall::string statusText;
	bool statusVisible;
	nall::string title;
	bool visible;
	nall::array<Widget&> widget;
	nall::string widgetFont;

	State(void)
	{
		backgroundColorOverride = false;
		backgroundColor = { 0, 0, 0, 255 };
		fullScreen = false;
		geometry = { 128, 128, 256, 256 };
		ignore = false;
		menuVisible = false;
		resizable = true;
		statusVisible = false;
		visible = false;
	}
};

struct Action::State {
	bool enabled;
	Menu *menu;
	bool visible;
	Window *window;

	State(void)
	{
		enabled = true;
		menu = 0;
		visible = true;
		window = 0;
	}
};

struct Menu::State {
	nall::array<Action&> action;
	nall::image image;
	nall::string text;

	State(void) : image(0, 32, 255u << 24, 255u << 16,
	    255u << 8, 255u << 0) { }
};

struct Item::State {
	nall::image image;
	nall::string text;

	State(void) : image(0, 32, 255u << 24, 255u << 16,
	    255u << 8, 255u << 0) { }
};

struct CheckItem::State {
	bool checked;
	nall::string text;

	State(void) { checked = false; }
};

struct RadioItem::State {
	bool checked;
	nall::array<RadioItem&> group;
	nall::string text;

	State(void) { checked = true; }
};

struct Sizable::State {
	Layout *layout;
	Window *window;

	State(void)
	{
		layout = 0;
		window = 0;
	}
};

struct Layout::State {
	State(void) { }
};

struct Widget::State {
	bool abstract;
	bool enabled;
	nall::string font;
	Geometry geometry;
	bool visible;

	State(void)
	{
		abstract = false;
		enabled = true;
		geometry = { 0, 0, 0, 0 };
		visible = true;
	}
};

struct Button::State {
	nall::image image;
	Orientation orientation;
	nall::string text;

	State(void) : image(0, 32, 255u << 24, 255u << 16,
		255u << 8, 255u << 0) { }
};

struct Canvas::State {
	uint32_t *data;
	unsigned width;
	unsigned height;

	State(void)
	{
		data = nullptr;
		width = 256;
		height = 256;
	}
};

struct CheckBox::State {
	bool checked;
	nall::string text;

	State(void) { checked = false; }
};

struct ComboBox::State {
	unsigned selection;
	nall::vector<nall::string> text;

	State(void) { selection = 0; }
};

struct HexEdit::State {
	unsigned columns;
	unsigned length;
	unsigned offset;
	unsigned rows;

	State(void)
	{
		columns = 16;
		length = 0;
		offset = 0;
		rows = 16;
	}
};

struct HorizontalScrollBar::State {
	unsigned length;
	unsigned position;

	State(void)
	{
		length = 101;
		position = 0;
	}
};

struct HorizontalSlider::State {
	unsigned length;
	unsigned position;

	State(void)
	{
		length = 101;
		position = 0;
	}
};

struct Label::State {
	nall::string text;
};

struct LineEdit::State {
	bool editable;
	nall::string text;

	State(void) { editable = true; }
};

struct ListView::State {
	bool checkable;
	nall::array<bool> checked;
	nall::lstring headerText;
	bool headerVisible;
	bool selected;
	unsigned selection;
	nall::vector<nall::lstring> text;

	State(void)
	{
		checkable = false;
		headerVisible = false;
		selected = false;
		selection = 0;
	}
};

struct ProgressBar::State {
	unsigned position;

	State(void) { position = 0; }
};

struct RadioBox::State {
	bool checked;
	nall::array<RadioBox&> group;
	nall::string text;

	State(void) { checked = true; }
};

struct TextEdit::State {
	unsigned cursorPosition;
	bool editable;
	nall::string text;
	bool wordWrap;

	State(void)
	{
		cursorPosition = 0;
		editable = true;
		wordWrap = true;
	}
};

struct VerticalScrollBar::State {
	unsigned length;
	unsigned position;

	State(void)
	{
		length = 101;
		position = 0;
	}
};

struct VerticalSlider::State {
	unsigned length;
	unsigned position;

	State(void)
	{
		length = 101;
		position = 0;
	}
};

} /* namespace phoenix */

#endif
