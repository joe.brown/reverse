Reverse
=======
Welcome to 'reverse'!

Building
--------
To build, follow the typical CMake paradigm:
    $ git clone https://gitlab.com/joe.brown/reverse.git
    $ cd reverse
    $ make build
    $ cd build
    $ cmake ..
    $ make -j4
    $ make install

For the default options, you'll need to add the $HOME/.local/bin to your
system path.  Due to my lack of knowledge on how to script CMake, I don't
know how to install things with reduced permissions when 'sudo make install'
is ran.

To build, you'll need the following headers and libraries:
    - Qt4
    - SDL1.2
    - OpenGL
    - PulseAudio

Contributing
------------
Contributions are welcome.  Your code is your code.  You do not assign
copyright to the project.

TODO
----
These are long-term goals.  Currently the binary works and you can play
games, which is ultimately what we want.  But I'd prefer that the whole
codebase be easier to maintain going forward.

- Port to Qt5
- Add SDL2 support
- Modern OpenGL Support (3.3+)
- Vulkan Support
