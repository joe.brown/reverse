find_program(XDG-DESKTOP-ICON_EXECUTABLE xdg-desktop-icon)
find_program(XDG-DESKTOP-MENU_EXECUTABLE xdg-desktop-menu)
find_program(XDG-ICON-RESOURCE_EXECUTABLE xdg-icon-resource)

execute_process(COMMAND ${XDG-DESKTOP-MENU_EXECUTABLE}
    install --novendor "emulator/reverse.desktop")
execute_process(COMMAND ${XDG-ICON-RESOURCE_EXECUTABLE}
    install --novendor --size 48 "emulator/reverse.png")
